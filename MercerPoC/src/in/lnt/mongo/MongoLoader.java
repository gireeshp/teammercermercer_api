package in.lnt.mongo;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import in.lnt.connector.MongoConnector;
import in.lnt.utility.general.Cache;

public class MongoLoader {
	public void dumpDataToMongo(JSONArray data) {
		int i = 0;
		DBCollection collection = MongoConnector.getMongoDBConnection()
				.getCollection(Cache.getProperty(in.lnt.utility.constants.CacheConstats.MONGO_COLLECTION_NAME));
		DBObject dbObject;
		System.out.println(
				"Collection Name : " + Cache.getProperty(in.lnt.utility.constants.CacheConstats.MONGO_COLLECTION_NAME));
		for (int j = 0; j < data.length(); j++) {
			// convert JSON to DBObject directly
			dbObject = (DBObject) JSON.parse(data.get(j).toString());
			collection.insert(dbObject);
			// System.out.println(j);
			if (i++ == 100) {
				try {
					MongoConnector.m = null;
					MongoConnector.connectToDB();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			i++;
		}

	}
}
