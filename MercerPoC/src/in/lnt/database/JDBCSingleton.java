package in.lnt.database;

import java.sql.Connection;  
import java.sql.DriverManager;  
import java.sql.PreparedStatement;  
import java.sql.ResultSet;  
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.DataUtility;  

/**
 * @author Nikhil Kshirsagar
 * @since 25-09-2017
 * @version 1.0 
 */
public class JDBCSingleton {
	
	private static final Logger logger = LoggerFactory.getLogger(JDBCSingleton.class);
    // Create a JDBCSingleton class.  
	// Static member holds only one instance of the JDBCSingleton class.         
	private static JDBCSingleton jdbc;  

	private static String jdbcDriver = Cache.getProperty(CacheConstats.MERCER_DB_DRIVER);	// "com.mysql.jdbc.Driver";
	private static String dbAddress = Cache.getProperty(CacheConstats.MERCER_DB_ADDRESS);	//"jdbc:mysql://localhost:3306/";
	private static String dbName = Cache.getProperty(CacheConstats.MERCER_DB_NAME);			// "mercerdb";
	private static String userName = Cache.getProperty(CacheConstats.MERCER_DB_USERNAME);	// "username";
	private static String password = Cache.getProperty(CacheConstats.MERCER_DB_PASSWORD);	// "password";
	
	// JDBCSingleton prevents the instantiation from any other class.  
	private JDBCSingleton() {  }  

	// Providing global point of access.  
	public static JDBCSingleton getInstance() {    
		if (jdbc==null) {  
			jdbc=new  JDBCSingleton();  
		}  
		return jdbc;  
	}
	
	// Get connection from methods like insert, view etc.   
	public static Connection getConnection()throws ClassNotFoundException, SQLException {  
		Connection con=null;  
		Class.forName(jdbcDriver);  
		con= DriverManager.getConnection(dbAddress+dbName, userName, password);  
		return con;  
	}  

	// Get connection from methods like insert, view etc.   
	public  static Connection getConnection(String dbAddress, String dbName, String userName, String password)throws ClassNotFoundException, SQLException {  
		Connection con=null;  
		Class.forName("com.mysql.jdbc.Driver");  
		con= DriverManager.getConnection(dbAddress+dbName, userName, password);  
		return con;  
	}  

	
	// Insert the record into the database   
	public int insert(String query, Object[] args) throws SQLException {  
		Connection c=null;  
		PreparedStatement ps=null;  
		int recordCounter=0;  
		try {  
			c = JDBCSingleton.getConnection();  

			ps=c.prepareStatement(query);  
			for(int i = 0; i<args.length; i++){
				ps.setObject(i+1, args[i]);
			}
			
			recordCounter=ps.executeUpdate();  
		
		} catch (Exception e) { 
			e.printStackTrace(); 
		} finally{  
			if (ps!=null){  
				ps.close();  
			}if(c!=null){  
				c.close();  
			}   
		}  
		return recordCounter;  
	}  

	//to view the data from the database        
	public ResultSet get(Connection conn, String query, Object[] args) throws SQLException {  

		PreparedStatement pStmt = null;  
		ResultSet rs = null;  

		try {  
			
			pStmt = conn.prepareStatement(query);  
			if(args != null) {
				for(int i = 0; i<args.length; i++){
					pStmt.setObject(i+1, args[i]);
				}
			}
			
			rs = pStmt.executeQuery();
		} catch (Exception ex) { 
			logger.error(ex.getMessage() + " Reason is : "+ ex.getCause() );
		}  
		return rs;
	}  
  
	// to update the password for the given username  
	public int update(String name, String password) throws SQLException  {  
		Connection c=null;  
		PreparedStatement ps=null;  

		int recordCounter=0;  
		try {  
			c=JDBCSingleton.getConnection();  
			ps=c.prepareStatement(" update userdata set upassword=? where uname='"+name+"' ");  
			ps.setString(1, password);  
			recordCounter=ps.executeUpdate();  
		} catch (Exception e) {  e.printStackTrace(); } finally{  

			if (ps!=null){  
				ps.close();  
			}if(c!=null){  
				c.close();  
			}   
		}  
		return recordCounter;  
	}  

	//to delete the data from the database   
	public int delete(int userid) throws SQLException{  
		Connection c=null;  
		PreparedStatement ps=null;  
		int recordCounter=0;  
		try {  
			c=JDBCSingleton.getConnection();  
			ps=c.prepareStatement(" delete from userdata where uid='"+userid+"' ");  
			recordCounter=ps.executeUpdate();  
		} catch (Exception e) { e.printStackTrace(); }   
		finally{  
			if (ps!=null){  
				ps.close();  
			}if(c!=null){  
				c.close();  
			}   
		}  
		return recordCounter;  
	}

	public static void main(String[] args) throws SQLException {
		JDBCSingleton jdbcInstance = JDBCSingleton.getInstance();
		Object[] args2 = new Object[]{};
				
		String query = "insert into fileStorageInfo(srNo, requestId, time_stamp, fileName, requestLocale, requestAgent, requestIp) "
				+ "values(?,?,?,?,?,?,?)";
		args2 = new Object[]{"018c6fe4-f95e-4362-b25d-dbcaf5b29123", "temp", DataUtility.currentTimestamp(),
				"temp.csv", "fr", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
				"0:0:0:0:0:0:1:1"};
		jdbcInstance.insert(query, args2);
	}

}
