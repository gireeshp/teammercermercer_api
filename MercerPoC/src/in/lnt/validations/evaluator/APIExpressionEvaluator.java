package in.lnt.validations.evaluator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.maxiq.ExpressionEvaluator.ExpressionEvaluator;
import com.augmentiq.maxiq.ExpressionEvaluator.exceptions.ExpressionEvaluatorException;
import com.augmentiq.maxiq.ExpressionEvaluator.exceptions.FieldNotValidException;
import com.augmentiq.maxiq.ExpressionEvaluator.utils.ExpUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;

import in.lnt.constants.Constants;
import in.lnt.enums.DataTypes;
import in.lnt.enums.ValidationTypes;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.service.db.DBUtils;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.DataUtility;
import in.lnt.utility.general.JsonUtils;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class APIExpressionEvaluator {
	private static final Logger logger = LoggerFactory.getLogger(APIExpressionEvaluator.class);
	public ExpressionEvaluator expressionEvaluator = new ExpressionEvaluator();
	private static Pattern pattern = null;
	ObjectMapper mapper = new ObjectMapper();
	public Map<String,String> columnDataTypeMapping = null;
	public static DecimalFormat df = new DecimalFormat("#");

	@SuppressWarnings("deprecation")
	public ObjectNode expressionEvaluator(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, JsonNode dataNode, boolean isAggregateRequest,
			HashMap<String, String> columnMappingMap, ArrayList<JsonNode> columnNodeList) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_EXPRESSIONEVALUATOR));
		String expr = "";
		String result = "";
		ObjectNode obj = null;
		boolean checkBox = false;
		String errorType = "";
		JsonNode tempJsonNode = null;
		if (columnNode != null && columnNode.get(Constants.QUESTION_TYPE) != null
				&& columnNode.get(Constants.QUESTION_TYPE).asText().equalsIgnoreCase("checkboxes")) {
			checkBox = true;
			pattern = Pattern.compile(Constants.EXP_PATTERN_CHECKBOX);
		} else {
			pattern = Pattern.compile(Constants.EXP_PATTERN);
		}

		Map<String, String> paramValues = new HashMap<String, String>();
		if (validationOBJ != null && validationOBJ.get(Constants.EXPRESSION) != null) {

			//logger.info(String.format("Machine learning flag value is %s", mlFlag));
			paramValues = getParamValues(dataMap, validationOBJ, checkBox, columnMappingMap, columnNodeList);
				
			expr = paramValues.get(Constants.EXPRESSION_STRING).replace("null", "\'null\'");

			try {
				
				if(columnNode!=null && columnNode.get(Constants.CODE) !=null && columnNode.get(Constants.CODE).asText().equalsIgnoreCase("YOUR_EEID"))
				{
				
					if(dataMap!=null && (dataMap.get("YOUR_EEID")==null || dataMap.get("YOUR_EEID").asText().equals("")))
					{
					
						return null;
					}
				}
				if(columnNode!=null && columnNode.get(Constants.MAPPED_COLUMN_NAME) !=null 
						&& columnNode.get(Constants.MAPPED_COLUMN_NAME).asText().equalsIgnoreCase("YOUR_EEID"))
				{
				
					if(dataMap!=null && (dataMap.get("YOUR_EEID")==null || dataMap.get("YOUR_EEID").asText().equals("")))
					{
					
						return null;
					}
				}
				result = expressionEvaluator.expressionEvaluator(paramValues, expr, false);

			} catch (FieldNotValidException e) {
				logger.error("Exception occured in expressionEvaluator .."+e.getMessage());
				result = e.getMessage();
				result = getMessageForAnErrorType(validationOBJ, ValidationTypes.expressionError, expr, result);
				obj = prepareOutputJson(validationOBJ, columnNode, expr + result,
						ValidationTypes.expressionError, columnNode.get(Constants.CODE).asText(), expr, null, null);
				// throw new JSONException(e.getMessage()+"error in
				// exp..."+expr);
				return obj;
			} catch (ExpressionEvaluatorException e) {
				logger.error("Exception occured in expressionEvaluator .."+e.getMessage());
				result = e.getMessage();
				result = getMessageForAnErrorType(validationOBJ, ValidationTypes.expressionError, expr, result);
				obj = prepareOutputJson(validationOBJ, columnNode, expr + result,
						ValidationTypes.expressionError, columnNode.get(Constants.CODE).asText(), expr, null, null);
				// throw new JSONException(e.getMessage()+"error in
				// exp..."+expr);
				return obj;
			} catch (Exception e) {
				logger.error("Exception occured in expressionEvaluator .."+e.getMessage());
				result = e.getMessage();
				obj = prepareOutputJson(validationOBJ, columnNode, expr + " " + "ATTENTION: " + result,
						ValidationTypes.expressionError, columnNode.get(Constants.CODE).asText(), expr, null, null);
				// throw new Exception(e.getMessage()+"error in exp..."+expr);
				return obj;

			}
				//logger.debug(String.format("result..%s", result));
				
				if (isAggregateRequest) {
					obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.expression, null, null,
							null, null);
					if(StringUtils.isBlank(result) || result.length() < 1) {
						result = "[]";
						//logger.info("Result found blank. So, replacing it with [].");
					}
					obj.put("data", mapper.readTree(ExpUtils.cleanSingleQuoteStartAndEnd(result)));
					return obj;
				}
				
				if (validationOBJ.get(Constants.ERROR_TYPE) != null)
					errorType = (String) validationOBJ.get(Constants.ERROR_TYPE).asText();

				if ((errorType.compareToIgnoreCase(Constants.ERROR) == 0)
						|| (errorType.compareToIgnoreCase(Constants.ALERT) == 0)
						|| (errorType.compareToIgnoreCase(Constants.ENHANCEMENT) == 0)) {
					
					if (result != null && result.equals("true")) {
						obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.expression, null, null,
								null, null); // PE-3711
					} else if (result != null && result.equals("false")) {
					} 
				} else if (errorType.compareToIgnoreCase("AUTOCORRECT") == 0) {
					if (columnNode.has(Constants.CODE) && null != dataMap.get(columnNode.get(Constants.CODE).asText())
							&& !dataMap.get(columnNode.get(Constants.CODE).asText()).asText().equals(result)) {
						((ObjectNode) validationOBJ).set("OriginalValue",
								dataMap.get(columnNode.get(Constants.CODE).asText()));
						((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), result);
						//Start PE-7071 Following code added for the new requirement 
						if( !JsonUtils.isNullOrBlankOrNullNode(dataNode.get(columnNode.get(Constants.CODE).asText()))){
							dataMap.put(columnNode.get(Constants.CODE).asText(),
									dataNode.get(columnNode.get(Constants.CODE).asText()));
						}
						//End  PE-7071
						obj = prepareOutputJson(validationOBJ, columnNode, result, ValidationTypes.expression, null,
								null, null, null);
					}
				}
		}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_EXPRESSIONEVALUATOR));
		if (null == obj && !result.equals("false") && !errorType.toUpperCase().equals("AUTOCORRECT")) {
			result = getMessageForAnErrorType(validationOBJ, ValidationTypes.expressionError, expr, result);
			obj = prepareOutputJson(validationOBJ, columnNode, result,
					ValidationTypes.expressionError, columnNode.get(Constants.CODE).asText(), expr, null, null);
		}
		return obj;
	}

	public ObjectNode checkMandatoryFields(HashMap<String, JsonNode> dataMap, String key, String dataType,
			String mappedColumn) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_CHECKMANDATORYFIELDS));
			if ((dataMap.get(key) == null || dataMap.get(key).asText().isEmpty())
					&& (dataMap.get(mappedColumn) == null || dataMap.get(mappedColumn).asText().isEmpty())) {
				return prepareOutputJson(null, null, null, ValidationTypes.required, key, null, null, null);
			}
			//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_CHECKMANDATORYFIELDS));
		return checkDataType(dataMap, key, dataType, mappedColumn);
	}

	/*
	 * For CSC
	 */
	public ObjectNode expressionEvaluator(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, JsonNode dataNode,  Map<String, Integer> cscDuplicateChkMap, ArrayList<JsonNode> columnNodeList)
					throws CustomStatusException, Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_EXPRESSIONEVALUATOR));
		ArrayList<String> paramValueList=null;
		String expr = "";
		String result = "";
		ObjectNode obj = null;
		boolean checkBox = false;
		String fieldArr[] = null;
		String prefix = "this.";
		String patternString = "";
		ArrayList<String> dimList=null;
		JsonNode dimNode = null;
		Pattern pattern = Pattern.compile(Constants.EXP_PATTERN);
		if (columnNode != null && columnNode.get(Constants.QUESTION_TYPE) != null
				&& columnNode.get(Constants.QUESTION_TYPE).asText().equalsIgnoreCase("checkboxes")) {
			checkBox = true;
			pattern = Pattern.compile(Constants.EXP_PATTERN_CHECKBOX);
		} else {
			pattern = Pattern.compile(Constants.EXP_PATTERN);
		}

		Map<String, String> paramValues = new HashMap<String, String>();
		if (validationOBJ != null && validationOBJ.get(Constants.EXPRESSION) != null && !validationOBJ.get(Constants.EXPRESSION).asText().equals(Constants.BLANK)) {//PE7044 Blank expression clause is added
			expr = validationOBJ.get(Constants.EXPRESSION).asText().replaceAll(Constants.CURLY_BRACES, "");
			patternString = expr;
			if (expr != null && expr.contains("bool_in_lookup")) {
				patternString = expr.replaceAll(Constants.REPLACE_FROM, Constants.REPLACE_TO);
			}
			Matcher matcher = pattern.matcher(patternString);
			List<String> fields = new ArrayList<String>();
			while (matcher.find()) {
				fields.add(matcher.group().replace("this.", "").trim());
			}

			//logger.debug(String.format("fields..%s", fields));
			for (String field : fields) {
				if (field != null && field.trim().contains("contextData")) {
					prefix = prefix.replace("this.", "");
				}
				if (checkBox) {
					fieldArr = extractFields(field.trim());
					if (fieldArr != null) {	
						paramValues.put(prefix + field.trim(), (dataMap.get(fieldArr[0]) == null)
								? (dataMap.get("otherSectionsData." + fieldArr[0].trim()) == null ? "null"
										: findInArray(dataMap.get("otherSectionsData." + fieldArr[0]), fieldArr[1]))
								: findInArray(dataMap.get(fieldArr[0]), fieldArr[1]));
					} else {
						paramValues.put(prefix + field.trim(), (dataMap.get(field.trim()) == null)
								? (dataMap.get("otherSectionsData." + field.trim()) == null ? "null"
										: getContextDataValues(dataMap.get("otherSectionsData." + field.trim()).asText().replace(",", "")))
								:  getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(field.trim(), "dataType"),
										dataMap.get(field.trim()).asText().replace(",", "")));
					}
				} else {
					String tmp = null;
					if (dataMap.get(field.trim()) == null) {
						if (dataMap.get("otherSectionsData." + field.trim()) == null) {
							if (cscDuplicateChkMap != null && cscDuplicateChkMap.containsKey(field.trim())) {
								/*throw new JSONException(
										String.format(Constants.ERROR_N_VALUES_FOUND_FOR_VARIABLE_IN_EXPR,
												cscDuplicateChkMap.get(field.trim()), field.trim(), expr)); // Cannot find variable {1} in expression {2}
								*/
								
								paramValueList =  new ArrayList<String>();
								paramValueList.add(""+cscDuplicateChkMap.get(field.trim()));
								paramValueList.add(""+field.trim());
								paramValueList.add(""+expr);
								throw new CustomStatusException(Constants.HTTPSTATUS_400,
										in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_048),
										in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_048+Constants.PARAMS),paramValueList);
							} else {
								/*throw new JSONException(String.format(Constants.ERROR_CANNOT_FIND_VARIABLE_IN_EXPR,
										field.trim(), expr)); // Cannot find
																// variable {1}
																// in expression
																// {2}
*/								
								paramValueList =  new ArrayList<String>();
								paramValueList.add(field.trim());
								paramValueList.add(expr);
								throw new CustomStatusException(Constants.HTTPSTATUS_400,
										in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_007),
										in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_007+Constants.PARAMS),paramValueList);
							}
						} else {
							tmp = getContextDataValues(dataMap.get("otherSectionsData." + field.trim()).asText().replace(",", ""));
						}
					} else {
						tmp = getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(field.trim(), "dataType"),
								dataMap.get(field.trim()).asText().replace(",", ""));
					}
					paramValues.put(prefix + field.trim(), tmp); 
				}
				prefix = "this.";
			}
			expr = expr.replace("null", "\'null\'");
			try {
				result = expressionEvaluator.expressionEvaluator(paramValues, expr, false);

			} catch (Exception e) {
				logger.error("Excepion occured in expressionEvaluator.."+e.getMessage());
				paramValueList =  new ArrayList<String>();
				paramValueList.add(e.getMessage());
				throw new CustomStatusException(Constants.HTTPSTATUS_400,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_040),
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_040+Constants.PARAMS),paramValueList);
			}
				//logger.debug(String.format("result..%s", result));
				String errorType = "";
				if (validationOBJ.get(Constants.ERROR_TYPE) != null)
					errorType = (String) validationOBJ.get(Constants.ERROR_TYPE).asText();
				if ((errorType.compareToIgnoreCase(Constants.ERROR) == 0)
						|| (errorType.compareToIgnoreCase(Constants.ALERT) == 0)
						|| (errorType.compareToIgnoreCase(Constants.ENHANCEMENT) == 0)) {
					if (result != null && result.equals("true")) {

						// Implementation start PE : 5414

						if (validationOBJ.get(Constants.MDA) != null
								&& !validationOBJ.get(Constants.MDA).asText().equals("")) {
							if (validationOBJ.get(Constants.MDA).asText().equalsIgnoreCase(Constants.EXECLUE_ROW)) {
								//logger.info(String.format("MDA value in request..%s", Constants.EXECLUE_ROW));
								((ObjectNode) (dataNode)).put(Constants.EXCLUDE_FLAG, "Y");
								return obj;
							} else if (validationOBJ.get(Constants.MDA).asText()
									.equalsIgnoreCase(Constants.CLEAR_VALUE)) {
								//logger.info(String.format("MDA value in request..%s", Constants.CLEAR_VALUE));
								((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), "");
								return obj;
							} else {//7044 condition added
								//7044 condition added
								String responseData= expressionEvaluatorForMDA(dataMap, validationOBJ,columnNode,cscDuplicateChkMap);
								 
								if(columnNode.get(Constants.DIMENSIONS)!=null)
								{
									dimList=APIExpressionEvaluator.extractDimensions(columnNode.get(Constants.DIMENSIONS));
									dimNode = mapper.createObjectNode();
									for(String dim:dimList)
									{
									((ObjectNode)dimNode).put(dim,responseData);
							}
									((ObjectNode) (dataNode)).set(columnNode.get(Constants.CODE).asText(), dimNode);
						}

								else{
											((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), responseData);
									}
							
								return obj;
							}
						}

						// Implementation end PE : 5414

						obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.expression, null, null,
								null, null); // PE-3711
					} else if (result != null && result.equals("false")) {
					} else {
						throw new CustomStatusException(Constants.HTTPSTATUS_400,in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_005));
					}
				} else if (errorType.compareToIgnoreCase("AUTOCORRECT") == 0) {
					if (columnNode.has(Constants.CODE) && null != dataMap.get(columnNode.get(Constants.CODE).asText())
							&& !dataMap.get(columnNode.get(Constants.CODE).asText()).equals(result)) {
						((ObjectNode) validationOBJ).set("OriginalValue",
								dataMap.get(columnNode.get(Constants.CODE).asText()));
						((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), result);
						obj = prepareOutputJson(validationOBJ, columnNode, result, ValidationTypes.expression, null,
								null, null, null);
					}
				}

		}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_EXPRESSIONEVALUATOR));
		return obj;
	}

	public ObjectNode checkMandatoryFields(HashMap<String, JsonNode> dataMap, String key, String dataType)
			throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_CHECKMANDATORYFIELDS));
			if (dataMap.get(key) == null || dataMap.get(key).asText().isEmpty()) {
				return prepareOutputJson(null, null, null, ValidationTypes.required, key, null, null, null);
			}
			//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_CHECKMANDATORYFIELDS));
		return checkDataType(dataMap, key, dataType);
	}

	public ObjectNode checkDataType(HashMap<String, JsonNode> dataMap, String key, String dataType) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_CHECKDATATYPE));
		Pattern INT_PATTERN = Pattern.compile(Constants.INT_PATTERN);
		Pattern DOUBLE_PATTERN = Pattern.compile(Constants.DOUBLE_PATTERN);
		Pattern LONG_PATTERN = Pattern.compile(Constants.LONG_PATTERN);
		ObjectNode obj = null;
			if (EnumUtils.isValidEnum(DataTypes.class, String.valueOf(dataType).toUpperCase())) {
				String value = String.valueOf(dataMap.get(key).asText()).replace(",", "");
				switch (DataTypes.valueOf(String.valueOf(dataType.toUpperCase()))) {
				case STRING:
					break;
				case INTEGER:
				case INT:
					if (((StringUtils.isEmpty(value)) || (value == "null")) || (INT_PATTERN.matcher(value).matches())) {
					} else {
						obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
					}
					break;
				case DOUBLE:
					if (((StringUtils.isEmpty(value)) || (value == "null"))
							|| (DOUBLE_PATTERN.matcher(value).matches())) {
					} else {
						obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
					}
					break;
				case DATE:
					break;
				case BOOLEAN:
					break;
				case LONG:
					if (((StringUtils.isEmpty(value)) || (value == "null"))
							|| (LONG_PATTERN.matcher(value).matches())) {
					} else {
						obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
					}
					break;
				}
			}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_CHECKDATATYPE));
		return obj;
	}

	public ObjectNode checkValidationTypeOneOf(HashMap<String, JsonNode> dataMap, String key, JsonNode node,
			JsonNode validationNode) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_CHECKVALIDATIONTYPEONEOF));
		ObjectNode obj = null;
		Pattern INT_PATTERN = Pattern.compile("^([+-]?[0-9]\\d*|0)$");
		Pattern DOUBLE_PATTERN = Pattern.compile("\\d+\\.\\d+");
		Pattern LONG_PATTERN = Pattern.compile("^-?\\d{1,19}$");
			String value = dataMap.get((node.get("code").asText())).asText();
			JsonNode valuesNode = validationNode.get("values");
			ArrayList<Object> valueList = new ArrayList();
			String values = "";

			for (int i = 0; i < valuesNode.size(); i++) {
				if (valuesNode != null) {
					valueList.add(valuesNode.get(i));
					if (i == 0)
						values = values + valuesNode.get(i).asText();
					else
						values = values + "," + valuesNode.get(i).asText();
				}

			}
			String modifiedValue = String.valueOf(value).replace(",", "");
			switch (validationNode.get("values").get(0).getNodeType()) {
			case NUMBER:
				if (DOUBLE_PATTERN.matcher(modifiedValue).matches()) {
					if (!valueList.contains(Double.valueOf(value))) {
						obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
					}
				} else if (INT_PATTERN.matcher(modifiedValue).matches()) {
					if (!valueList.contains(Integer.valueOf(value))) {
						obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
					}
				} else if (LONG_PATTERN.matcher(modifiedValue).matches()) {
					if (!valueList.contains(Integer.valueOf(value))) {
						obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
					}

				} else {

					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}

				break;
			case STRING:
				if (!StringUtils.isBlank(value)) {
					if (!valueList.contains(String.valueOf(value))) {
						obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
					}
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}
				break;

			}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_CHECKVALIDATIONTYPEONEOF));
		return obj;
	}
	/*
	 * For CSC end
	 */

	public ObjectNode checkDataType(HashMap<String, JsonNode> dataMap, String key, String dataType, String mappedColumn)
			throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_CHECKDATATYPE));
		Pattern INT_PATTERN = Pattern.compile(Constants.INT_PATTERN);
		Pattern DOUBLE_PATTERN = Pattern.compile(Constants.DOUBLE_PATTERN);
		Pattern LONG_PATTERN = Pattern.compile(Constants.LONG_PATTERN);
		ObjectNode obj = null;
		String value = "";
			if (EnumUtils.isValidEnum(DataTypes.class, String.valueOf(dataType).toUpperCase())) {
				if (key != null && !key.equals("")) {
					value = String.valueOf(dataMap.get(key).asText()).replace(",", "");
				} else {
					value = String.valueOf(dataMap.get(mappedColumn).asText()).replace(",", "");
				}
				switch (DataTypes.valueOf(String.valueOf(dataType.toUpperCase()))) {
				case STRING:
					break;
				case INTEGER:
				case INT:
					if (((StringUtils.isEmpty(value)) || (value == "null")) || (INT_PATTERN.matcher(value).matches())) {
					} else {
						obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
					}
					break;
				case DOUBLE:
					if (((StringUtils.isEmpty(value)) || (value == "null"))
							|| (DOUBLE_PATTERN.matcher(value).matches())) {
					} else {
						obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
					}
					break;
				case DATE:
					break;
				case BOOLEAN:
					break;
				case LONG:
					if (((StringUtils.isEmpty(value)) || (value == "null"))
							|| (LONG_PATTERN.matcher(value).matches())) {
					} else {
						obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
					}
					break;
				}
			}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_CHECKDATATYPE));
		return obj;
	}

	public ObjectNode checkValidationTypeOneOf(HashMap<String, JsonNode> dataMap, String key, JsonNode node,
			JsonNode validationNode, String mappedColumn) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_CHECKVALIDATIONTYPEONEOF));
		ObjectNode obj = null;
		Pattern INT_PATTERN = Pattern.compile("^([+-]?[0-9]\\d*|0)$");
		Pattern DOUBLE_PATTERN = Pattern.compile("\\d+\\.\\d+");
		Pattern LONG_PATTERN = Pattern.compile("^-?\\d{1,19}$");
			String value = dataMap.get((node.get("code").asText())).asText();
			JsonNode valuesNode = validationNode.get("values");
			ArrayList<Object> valueList = new ArrayList();
			String values = "";

			for (int i = 0; i < valuesNode.size(); i++) {
				if (valuesNode != null) {
					valueList.add(valuesNode.get(i));
					if (i == 0)
						values = values + valuesNode.get(i).asText();
					else
						values = values + "," + valuesNode.get(i).asText();
				}

			}
			String modifiedValue = String.valueOf(value).replace(",", "");
			switch (validationNode.get("values").get(0).getNodeType()) {
			case NUMBER:
				if (DOUBLE_PATTERN.matcher(modifiedValue).matches()) {
					if (!valueList.contains(Double.valueOf(value))) {
						obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
					}
				} else if (INT_PATTERN.matcher(modifiedValue).matches()) {
					if (!valueList.contains(Integer.valueOf(value))) {
						obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
					}
				} else if (LONG_PATTERN.matcher(modifiedValue).matches()) {
					if (!valueList.contains(Integer.valueOf(value))) {
						obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
					}

				} else {

					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}

				break;
			case STRING:
				if (!StringUtils.isBlank(value)) {
					if (!valueList.contains(String.valueOf(value))) {
						obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
					}
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}
				break;

			}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_CHECKVALIDATIONTYPEONEOF));
		return obj;
	}

	private String[] extractFields(String field) throws Exception{
		String arr[] = null;
		if (field != null && field.length() > 0 && StringUtils.contains(field, "['")
				&& StringUtils.contains(field, "']")) {
			// ArrayUtils
			arr = StringUtils.split(field, "['");

		}
		return arr;
	}

	private String findInArray(JsonNode node, String value) throws Exception{

		//logger.debug(String.format("value...%s", value));
		boolean returnValue = false;
		if (node != null && node.getNodeType() == JsonNodeType.ARRAY) {
			Iterator<JsonNode> it = node.iterator();
			while (it.hasNext()) {
				returnValue = it.next().asText().equalsIgnoreCase(value);
				if (returnValue)
					break;
			}
		} else {
			String arr[] = node.asText().split(",");
			if (arr != null && arr.length > 0) {
				for (int i = 0; i < arr.length; i++) {
					if (arr[i].equalsIgnoreCase(value))
						returnValue = true;
				}
			}
		}
		//logger.debug(String.format("returnValue...%s", returnValue));
		return returnValue + "";
 	}

	/*
	 *  NOTE : Use this method to :
	 *  Generate message(or result) for the prepareOutputJson() method depending upon the "errorType"
	 */
	public static String getMessageForAnErrorType(JsonNode validationOBJ, ValidationTypes errorType, String expr, String result) throws Exception{
		String message = null;
		if (errorType != null) {
			switch (errorType) {
			case expressionError:
				// PE-6660 : If "message" is provided by user in validationOBJ, use it.
				// 			Else display default message.
				if(!JsonUtils.isNullOrBlankOrNullNode(validationOBJ.get("message")))
				{
				if(StringUtils.isBlank(validationOBJ.get("message").asText())) {
					message =   result;
				} else {
					message = validationOBJ.get("message").asText();
				}
				}
				break;
				
			case  eeIdAutoCorrect :
				message = "Missing employee Id geneated:"+result;
				break;
				
			default:
				message = expr + " " + "ATTENTION: " + result;
				break;
					
			}
		}else {
			message = expr + " " + "ATTENTION: " + result;			
		}
		return message;
	}

	public ObjectNode prepareOutputJson(JsonNode validationOBJ, JsonNode columnNode, String result,
			ValidationTypes errorType, String ky, String value, String datatype, String values) throws Exception {
		
		/*
		 *  NOTE : In order to create result, use getMessageForAnErrorType() written above this method.
		 */
		
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_PREPAREOUTPUTJSON));
		ObjectNode validation = mapper.createObjectNode();
		JsonNode paramNode = mapper.createObjectNode();
		String message = null;
			if (errorType != null) {
				switch (errorType) {
				case DataType:
					validation.put(Constants.FIELD, ky);
					/*validation.put(Constants.MESSAGE_KEY,
							ky + " with value " + value + " not matched with data type " + datatype);*/
					validation.put(Constants.MESSAGE_KEY, in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_045));
					((ObjectNode)paramNode).put("key",ky);
					((ObjectNode)paramNode).put("value",value);
					((ObjectNode)paramNode).put("datatype",datatype);
					validation.set(Constants.MESSAGE_PARAMS,paramNode);
					break;
				case required:
					validation.put(Constants.FIELD, ky);
					//validation.put(Constants.MESSAGE_KEY, ky + " is required.");
					validation.put(Constants.MESSAGE_KEY, in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_041));
					((ObjectNode)paramNode).put("key",ky);
					validation.set(Constants.MESSAGE_PARAMS,paramNode);
					break;
				case oneOf:
					validation.put(Constants.FIELD, ky);
					validation.put(Constants.MESSAGE,
							value + " is not a valid value for this field.  Please choose from " + values);
					validation.put(Constants.MESSAGE_KEY, in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_042));
					((ObjectNode)paramNode).put("value",value);
					((ObjectNode)paramNode).put("values",values);
					validation.set(Constants.MESSAGE_PARAMS,paramNode);
					break;
				case expression:
					Iterator<String> it = validationOBJ.fieldNames();
					String key = "";
					while (it.hasNext()) {
						key = it.next();
						validation.put(key, validationOBJ.get(key).asText());
					}
					// PE-7071 : Blank CODE will be accepted.
					String field = JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.CODE))
							? (columnNode.get(Constants.CODE)==null?null:"")
							: columnNode.get(Constants.CODE).asText();
					validation.put(Constants.FIELD, field);
					break;

				case dataTypeError:
					//logger.info(String.format("Check for the dataTypeError %s ", validationOBJ));
					if (null != validationOBJ) {
						Iterator<String> itr = validationOBJ.fieldNames();
						//logger.info(String.format("Check for the dataTypeError %s ", itr.hasNext()));
						String keyName = "";
						while (itr.hasNext()) {
							key = itr.next();
							validation.put(keyName, validationOBJ.get(keyName).asText());
						}
					}
					validation.put(Constants.FIELD, columnNode.get(Constants.CODE).asText());
					validation.put(Constants.VALIDATION_TYPE, "dataTypeCheck");
					validation.put(Constants.ERROR_TYPE, Constants.ERROR);
					//validation.put(Constants.MESSAGE, "Incorrect " + value + " format");
					validation.put(Constants.ID, "dataType" + StringUtils.capitalize(datatype));
					validation.put(Constants.MESSAGE_KEY, in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_043));
					((ObjectNode)paramNode).put("value",value);
					validation.set(Constants.MESSAGE_PARAMS,paramNode);
					//logger.info(String.format("validation object is..%s", validation));
					break;

				case expressionError:
					validation.put(Constants.FIELD, ky);
					validation.put(Constants.VALIDATION_TYPE, Constants.EXPRESSION);
					validation.put(Constants.ERROR_TYPE, Constants.ERROR);
					if(!JsonUtils.isNullOrBlankOrNullNode(validationOBJ.get("message")) &&
							StringUtils.isBlank(validationOBJ.get("message").asText())) {
						validation.put(Constants.MESSAGE_KEY, in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_044));
						((ObjectNode)paramNode).put("result",result);
						validation.set(Constants.MESSAGE_PARAMS,paramNode);
					} else {
					validation.put(Constants.MESSAGE, result);
					}
					validation.put("category", "input_data");
				    break;
			case  eeIdAutoCorrect :
				validation.put(Constants.FIELD,ky);
				validation.put(Constants.VALIDATION_TYPE, Constants.EXPRESSION);
				validation.put(Constants.ERROR_TYPE, Constants.AUTOCORRECT);
				//validation.put(Constants.MESSAGE, result);
				validation.put(Constants.MESSAGE_KEY, in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_039));
				((ObjectNode)paramNode).put("eeid",result);
				validation.set(Constants.MESSAGE_PARAMS,paramNode);
				validation.put(Constants.ORIGINAL_VALUE,Constants.BLANK);
			    break;
			   
			case  range :
				Iterator<String> it2 = validationOBJ.fieldNames();
				while (it2.hasNext()) {
					key = it2.next();
					if(validationOBJ.get(key).getNodeType() == JsonNodeType.ARRAY)
					validation.set(key, validationOBJ.get(key));
					else
						validation.put(key, validationOBJ.get(key).asText());	
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.VALIDATION_TYPE, Constants.VALIDATION_RANGE);
				validation.put(Constants.ERROR_TYPE, value);
				validation.put(Constants.MESSAGE_KEY, result );
				validation.put("category", "input_data");
				if(!values.equals(Constants.BLANK))
				validation.put(Constants.DIMENSION, values);
			    break; 
			case  rangeValidationRefTable :
				
				Iterator<String> it1 = validationOBJ.fieldNames();
					while (it1.hasNext()) {
					key = it1.next();
					if(validationOBJ.get(key).getNodeType() == JsonNodeType.ARRAY)
					validation.set(key, validationOBJ.get(key));
					else
						validation.put(key, validationOBJ.get(key).asText());	
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.ERROR_TYPE, value);
				validation.put(Constants.MESSAGE_KEY, result );
				if(!values.equals(Constants.BLANK))
				validation.put(Constants.DIMENSION, values);
			    break;
			case eligibility:
				validation = (ObjectNode) validationOBJ;
				break;
				
			case mlautocorrect :
				Iterator<String> mlitr = validationOBJ.fieldNames();
				while (mlitr.hasNext()) {
					key = mlitr.next();
					if(validationOBJ.get(key).getNodeType() == JsonNodeType.ARRAY)
						validation.set(key, validationOBJ.get(key));
					else
						validation.put(key, validationOBJ.get(key).asText());	
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.VALIDATION_TYPE, Constants.EXPRESSION);
				validation.put(Constants.ERROR_TYPE, Constants.AUTOCORRECT);
				validation.put(Constants.MESSAGE, result );
				validation.put("category", "input_data");
			    break;
			    
			case dropDown :
					validation.put(Constants.FIELD, ky);
					validation.put(Constants.ERROR_TYPE, Constants.ERROR);
					validation.put(Constants.MESSAGE_KEY, in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_053));
					((ObjectNode)paramNode).put("values",values);
					validation.set(Constants.MESSAGE_PARAMS,paramNode);
					break;

			}
			} else {
				if (result != null && result.length() > 0) {
					validation.put(Constants.MESSAGE, result);
				}
			}
		
		//logger.debug(String.format("validation..%s", validation));
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_PREPAREOUTPUTJSON));
		return validation;
	}

	public Map<String, String> getParamValues(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			boolean checkBox, HashMap<String, String> columnMappingMap, ArrayList<JsonNode> columnNodeList) throws Exception{
		String patternString = "";
		String expr = "";
		String prefix = "this.";
		String fieldArr[] = null;

		boolean isStringDataType = false;
		Map<String, String> paramValues = new HashMap<String, String>();
		expr = validationOBJ.get(Constants.EXPRESSION).asText();//.replaceAll(Constants.CURLY_BRACES, "");

		patternString = expr;
		if (expr != null && expr.contains("bool_in_lookup")) {
			patternString = expr.replaceAll(Constants.REPLACE_FROM, Constants.REPLACE_TO);
		}
		Matcher matcher = pattern.matcher(patternString);
		List<String> fields = new ArrayList<String>();
		while (matcher.find()) {
			fields.add(matcher.group().replace("this.", "").trim());
		}
		//logger.debug(String.format("fields..%s", fields));
		for (String field : fields) {
			if (field != null && field.trim().contains("contextData")) {
				prefix = prefix.replace("this.", "");
			}
			JsonNode dataMap_field_node = dataMap.get(field.trim());
			if (checkBox) {
				//logger.debug(String.format("checkBox..%s", fields));
				fieldArr = extractFields(field.trim());
				if (fieldArr != null) {
					paramValues.put(prefix + field.trim(),
							(dataMap.get(fieldArr[0]) == null)
									? (dataMap.get(columnMappingMap.get(fieldArr[0].trim())) == null
											? (dataMap.get("otherSectionsData." + fieldArr[0].trim()) == null ? "null"
													: findInArray(dataMap.get("otherSectionsData." + fieldArr[0]),
															fieldArr[1]))
											: "null")
									: findInArray((dataMap.get(fieldArr[0].trim())), fieldArr[1]));
				} else {
					paramValues
							.put(prefix + field.trim(),
									(JsonUtils.isNullOrBlankOrNullNode(dataMap_field_node)) 
											? (dataMap.get(columnMappingMap.get(field.trim())) == null
													? (dataMap.get("otherSectionsData." + field.trim()) == null ? "null" : // To  make  it  data  type  dependent
																	getContextDataValues( getContextDataValues(dataMap.get("otherSectionsData." + field.trim()).asText().replace(",", ""))))
													: getDataTypeBasedValue(
															columnDataTypeMapping.getOrDefault(
																	columnMappingMap.get(field.trim()), "dataType"),
															dataMap.get(columnMappingMap.get(field.trim())).asText()))
											: getDataTypeBasedValue(
													columnDataTypeMapping.getOrDefault(field.trim(), "dataType"),
													dataMap_field_node.asText().replace(",", "")) 

					);
				}
			} else {
				paramValues.put(prefix + field.trim(),
						(JsonUtils.isNullOrBlankOrNullNode(dataMap_field_node)) ? 
								(dataMap.get(columnMappingMap.get(field.trim())) == null
								? (dataMap.get("otherSectionsData." + field.trim()) == null 
										? "null"
										: dataMap.get("otherSectionsData." + field.trim()).asText().replace(",", ""))
								:  getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(columnMappingMap.get(field.trim()), "dataType"),
										dataMap.get(columnMappingMap.get(field.trim())).asText()))
						: getDataTypeBasedValue(
										columnDataTypeMapping.getOrDefault(field.trim(), "dataType"),
										dataMap_field_node.asText().replace(",", ""))); 
			}
			prefix = "this.";
		}
		// Add expression
		paramValues.put(Constants.EXPRESSION_STRING, expr);
		return paramValues;
	}
	
	public String getDataTypeBasedValue(String type, String value) {
		try {
			if (!StringUtils.isBlank(value)) {
				switch (type) {
				case "int":
					return value;
				case "double":
					return df.format(Double.valueOf(value));
				case "text":
				case "string":
					return "\"" + value + "\"";
				case "boolean":
					return value;
				case "date":
					return "\"" + value + "\""; 
				case "dataType":
					return getContextDataValues(value);
				}
			}
			else{
				return "null";
			}
		} catch (Exception e) {
			return "null";
		}
		return value; 
	} 
 	 
	public String getContextDataValues(String value) {
		try {
			if (!StringUtils.isBlank(value)) {
				if (NumberUtils.isNumber(value)) {
					try {
						Double d = Double.valueOf(value);
						if ((d % 1) == 0) {
							return value;
						} else {
							return df.format(Double.valueOf(value));
						}
					} catch (Exception e) {

					}
				} else {
					value = "\"" + value + "\""; // Modified for PE: 7254
			      return  value;
				}
			} else {
				return "null";
			}
		} catch (Exception e) {
			return "null";
		}
		return value;
	} 

	
	public Map<String, String> getParamValuesForML(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			boolean checkBox, HashMap<String, String> columnMappingMap, ArrayList<JsonNode> columnNodeList) throws Exception{
		
		//logger.info("Entering inside getParamValuesForML method ");
		String patternString = "";
		String expr = "";
		String prefix = "this.";
		boolean isStringDataType = false;

		Map<String, String> paramValues = new HashMap<String, String>();

		expr = validationOBJ.get(Constants.EXPRESSION).asText();//.replaceAll(Constants.CURLY_BRACES, "");
		patternString = expr;
		if (expr != null && expr.contains("bool_in_lookup")) {
			patternString = expr.replaceAll(Constants.REPLACE_FROM, Constants.REPLACE_TO);
		}
		Matcher matcher = pattern.matcher(patternString);
		List<String> fields = new ArrayList<String>();
		while (matcher.find()) {
			fields.add(matcher.group().replace("this.", "").trim());
		}
		//logger.debug(String.format("fields..%s", fields));
		for (String field : fields) {
			for (JsonNode columnNode : columnNodeList) {
				if((null != columnNode.get(Constants.CODE)) && (null != columnNode.get(Constants.CODE)) && columnNode.get(Constants.CODE).asText().equals(field)){
 					isStringDataType = columnNode.get(Constants.DATA_TYPE).asText().equals("string") || columnNode.get(Constants.DATA_TYPE).asText().equals("text");
				}
			}
			
			
			if (field != null && field.trim().contains("contextData")) {
				prefix = prefix.replace("this.", "");
			}

				if (dataMap.get(field.trim()) == null) {
					//logger.info("dataMap.get(field.trim()) is null");
				} else {
					if (dataMap.get(field + "_ACflag".trim()).asText().equalsIgnoreCase("N")
							|| dataMap.get(field + "_ACflag".trim()).asText().equalsIgnoreCase("A")) {
						paramValues.put(prefix + field.trim(), dataMap.get(field + "_Replace".trim()).asText());
						dataMap.get(field.trim()).asText().replace(",", "");
					}else {
						paramValues.put(prefix + field.trim(),
								(dataMap.get(field.trim()) == null) ? (dataMap.get(columnMappingMap.get(field.trim())) == null
										? (dataMap.get("otherSectionsData." + field.trim()) == null ? "null"
												: dataMap.get("otherSectionsData." + field.trim()).asText().replace(",", ""))
										: (dataMap.get(columnMappingMap.get(field.trim())).asText().replace(",", "")))
										: (isStringDataType ? "\"" + dataMap.get(field.trim()).asText().replace(",", "") + "\""
												: dataMap.get(field.trim()).asText().replace(",", "")));
					}
				}

			prefix = "this.";
		}
		// Add expression
		paramValues.put(Constants.EXPRESSION_STRING, expr);
		//logger.info(String.format("paramValues from ML are...%s", paramValues));
		return paramValues;
	}
	

	@SuppressWarnings("unlikely-arg-type")
	public ArrayList<ObjectNode> prepareRangeObject(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ, JsonNode columnNode,
			JsonNode dataNode)
			throws Exception {

		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_EXPRESSIONEVALUATOR));
		ObjectNode obj = null;
		String paramValue = "";
		String warning = null;
		String warningMsg = null;
		ArrayList<String> dimList=null;
		JsonNode code=null;
		StringBuffer key=null;
		ArrayList<ObjectNode> objList= new ArrayList<ObjectNode>();
		String value="";
		HashMap<String,String> paramValueMap=null;
		//logger.info("Getting min,max for Alert,Error..");
		String minError = validationOBJ.has(Constants.MINIMUM_ERROR)
								?validationOBJ.get(Constants.MINIMUM_ERROR).asText():null;
		String maxError = validationOBJ.has(Constants.MAXIMUM_ERROR)
								?validationOBJ.get(Constants.MAXIMUM_ERROR).asText():null;
		String minAlert = validationOBJ.has(Constants.MINIMIUM_ALERT)
								?validationOBJ.get(Constants.MINIMIUM_ALERT).asText():null;
		String maxAlert = validationOBJ.has(Constants.MAXIMUM_ALERT)
								?validationOBJ.get(Constants.MAXIMUM_ALERT).asText():null;
		code=columnNode.get(Constants.CODE);
			
		if (code != null && columnNode.get(Constants.DIMENSIONS) !=null) 
			{
				paramValueMap = prepareParamValuesMap(code,columnNode.get(Constants.DIMENSIONS),dataMap);
			}
			else if (code != null ) {
				paramValueMap = new HashMap<String,String>();
			if (null != dataMap && !JsonUtils.isNullOrBlankOrNullNode(code) 
					&& !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(code.asText()))) {
				paramValue = dataMap.get(code.asText()).asText();
				paramValueMap.put(Constants.BLANK, paramValue);
			} else if (!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.MAPPED_COLUMN_NAME)) 
						&& !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(columnNode.get(Constants.MAPPED_COLUMN_NAME).asText()))) {
					paramValue = dataMap.get(columnNode.get(Constants.MAPPED_COLUMN_NAME).asText()).asText();
					paramValueMap.put(Constants.BLANK, paramValue);
				
			}
		}
			
		
		if(paramValueMap!=null && paramValueMap.size()>0)
		{
			for (Map.Entry<String, String> entry : paramValueMap.entrySet()) {
				warningMsg=null;
			    value= entry.getValue();
				if(DataUtility.isStringNotNullAndBlank(minError) &&  !StringUtils.isEmpty(value) 
						&& Double.parseDouble(value) <
						Double.parseDouble(minError)) {
					warning = Constants.ERROR;
					//warningMsg = Constants.ERROR_MIN_MSG;		//"minError";
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_024);		//"minError";
				}
				if(null==warningMsg && DataUtility.isStringNotNullAndBlank(maxError) && Double.parseDouble(value) > 
						Double.parseDouble(maxError)) {
					warning = Constants.ERROR;
					//warningMsg = Constants.ERROR_MAX_MSG;	// "maxError";
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_027);		//"minError";
					
				}
				if(null==warningMsg && DataUtility.isStringNotNullAndBlank(minAlert) && Double.parseDouble(value) <
						Double.parseDouble(minAlert)) {
					warning = Constants.ALERT;
					//warningMsg = Constants.ALERT_MIN_MSG;		// "minAlert";
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_025);		//"minError";
				}
				if(null==warningMsg && DataUtility.isStringNotNullAndBlank(maxAlert) && Double.parseDouble(value) > 
						Double.parseDouble(maxAlert)) {
					warning = Constants.ALERT;
					//warningMsg = Constants.ALERT_MAX_MSG;	// "maxAlert";
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_026);		//"minError";
				}
				//logger.info(String.format("warning :: %s warningMsg ::  %s", warning,warningMsg));
				if(warning!=null) {
					obj = prepareOutputJson(validationOBJ, columnNode, warningMsg,
						ValidationTypes.range, code.asText(), warning, null, entry.getKey()); 
					objList.add(obj);
				}
				
			}
		}
		return objList;
	}


	// PE - 5568
	public ArrayNode checkValidity(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ, JsonNode columnNode,
			JsonNode dataNode, boolean isMDARequest) throws Exception {
		ArrayNode arr = mapper.createArrayNode();
		ObjectNode validation = mapper.createObjectNode();
		JsonNode paramNode = mapper.createObjectNode();
		if (validationOBJ != null && validationOBJ.has(Constants.DEPENDENT_COLUMNS)
				&& validationOBJ.get(Constants.DEPENDENT_COLUMNS) != null) {
			JsonNode depedentColumns = validationOBJ.get(Constants.DEPENDENT_COLUMNS);
			boolean performMDAAction = false;
			if ((dataNode.get(columnNode.get(Constants.CODE).asText()).asText()).equals(Constants.ANSWER_YES)) {
				for (JsonNode depedentColumn : depedentColumns) {
					if ( null!=dataNode.get(depedentColumn.asText()) && 
							(StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), "null")
							|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), ""))) {
						if (!isMDARequest) {
							ObjectNode errorObject = mapper.createObjectNode();
							errorObject.put(Constants.VALIDATION_TYPE,validationOBJ.get(Constants.VALIDATION_TYPE).asText());
							errorObject.put(Constants.ERROR_TYPE, Constants.ALERT);
							errorObject.put(Constants.FIELD, depedentColumn.asText());
							/*errorObject.put(Constants.MESSAGE_KEY,
									"This incubent is not eligible for " + columnNode.get(Constants.CODE).asText()
											+ "'s  "+ columnNode.get(Constants.DISPLAYLABEL).asText() +". Please indicate eligibility.");*/
							errorObject.put(Constants.MESSAGE_KEY,in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_049));
							((ObjectNode)paramNode).put("code",columnNode.get(Constants.CODE).asText());
						//	((ObjectNode)paramNode).put("displayLabel",columnNode.get(Constants.DISPLAYLABEL).asText());
							((ObjectNode)paramNode).put("displayLabel",JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL))? "":columnNode.get(Constants.DISPLAYLABEL).asText());
							errorObject.set(Constants.MESSAGE_PARAMS,paramNode);
							errorObject = prepareOutputJson(errorObject, null, null, ValidationTypes.eligibility,
									null, null, null, null);
							arr.add(errorObject);
						}
						// MDA ACTION
						performMDAAction = true;
					} else {
						continue;
					}
				}
				if (performMDAAction) {
					if (isMDARequest) {
						// Do Nothing
					}
				}
				
			} else if ((dataNode.get(columnNode.get(Constants.CODE).asText()).asText())
					.equals(Constants.ANSWER_NO)) {
				// Hierarchy : No Answer, 0, Some Answer

				for (JsonNode depedentColumn : depedentColumns) {
					if (null!=dataNode.get(depedentColumn.asText()) &&
							(StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), "null")
							|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), ""))) {
						continue;
					} else if (null!=dataNode.get(depedentColumn.asText()) && dataNode.get(depedentColumn.asText()).asText().equals("0")) {
						((ObjectNode) (dataNode)).put(depedentColumn.asText(), "");
					} else {
						if (!isMDARequest) {
							ObjectNode errorObject = mapper.createObjectNode();

							errorObject.put(Constants.ERROR_TYPE, Constants.ERROR);
							errorObject.put(Constants.VALIDATION_TYPE,validationOBJ.get(Constants.VALIDATION_TYPE).asText());
							errorObject.put(Constants.FIELD, depedentColumn.asText());
							/*errorObject.put(Constants.MESSAGE,
									"This incubent is not eligible for " + columnNode.get(Constants.CODE).asText()
											+ "'s  "+ columnNode.get(Constants.DISPLAYLABEL).asText() +". Please indicate eligibility.");*/
							errorObject.put(Constants.MESSAGE_KEY,in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_049));
							((ObjectNode)paramNode).put("code",columnNode.get(Constants.CODE).asText());
							((ObjectNode)paramNode).put("displayLabel",JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL))? "":columnNode.get(Constants.DISPLAYLABEL).asText());
							errorObject.set(Constants.MESSAGE_PARAMS,paramNode);
							errorObject = prepareOutputJson(errorObject, null, null, ValidationTypes.eligibility,
									null, null, null, null);
							arr.add(errorObject);
						}
						// MDA ACTION
						performMDAAction = true;
						
					}
				}
				if (performMDAAction) {
					if (isMDARequest) {
							((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(),
									Constants.ANSWER_YES);
					}
				}

			} else {
				boolean throwError = false;
				boolean throwAutocorrection = false;
				for (JsonNode depedentColumn : depedentColumns) {
					if (null!=dataNode.get(depedentColumn.asText()) && 
							(StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), "null")
							|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), ""))) {
						throwError = true; 
						// MDA ACTION
						performMDAAction = true;

					} else {
						((ObjectNode) validationOBJ).set("OriginalValue",
								dataMap.get(columnNode.get(Constants.CODE).asText()));
						((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(),
								Constants.ANSWER_YES);
						throwAutocorrection = true;
					}
				}
				if (!isMDARequest && throwError && !throwAutocorrection) {
					ObjectNode errorObject = mapper.createObjectNode();
					errorObject.put(Constants.ERROR_TYPE, Constants.ALERT);
					errorObject.put(Constants.VALIDATION_TYPE,validationOBJ.get(Constants.VALIDATION_TYPE).asText());
					errorObject.put(Constants.FIELD, columnNode.get(Constants.CODE).asText());
					errorObject.put(Constants.MESSAGE_KEY,in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_050));
					((ObjectNode)paramNode).put("code",columnNode.get(Constants.CODE).asText());
					((ObjectNode)paramNode).put("displayLabel",JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL))? "":columnNode.get(Constants.DISPLAYLABEL).asText());
					((ObjectNode)paramNode).put("code1",columnNode.get(Constants.CODE).asText());
					((ObjectNode)paramNode).put("displayLabel",JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL))? "":columnNode.get(Constants.DISPLAYLABEL).asText());
					errorObject.set(Constants.MESSAGE_PARAMS,paramNode);
					
					errorObject = prepareOutputJson(errorObject, null, null, ValidationTypes.eligibility, null,
							null, null, null);
					arr.add(errorObject);
				}
				
				/*if (!isMDARequest && throwAutocorrection) {
					arr.add(prepareOutputJson(validationOBJ, columnNode, Constants.ANSWER_YES,
							ValidationTypes.expression, null, null, null, null));
				}*/
				
 				if (performMDAAction) {
					if (isMDARequest) {
						((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(),
								Constants.ANSWER_NO);
					}
				}
			}

		}
		return arr;
	}

	public ArrayList<ObjectNode> prepareRefernceRangeObject(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, JsonNode dataNode) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_PREPAREREFENCERANGEOBJECT));	
		StringBuffer query=new StringBuffer();
		List<String> columnList = null;
		String lookUpColumn="";
		long startTime=0;
		HashMap<String,String> rangeMap=null;
		String paramValue = "";
		JsonNode code=null;
		JsonNode mappedColName=null;
		HashMap<String,String> paramValueMap=null;
		String tableName = null;
		ArrayList<String> paramValueList=null;
		code=columnNode.get(Constants.CODE);
		mappedColName=columnNode.get(Constants.MAPPED_COLUMN_NAME);
		ArrayList<ObjectNode> objList= new ArrayList<ObjectNode>();
		
		//******************Start Cache Logic For Testing then Development Purpose********************************
		/*CacheManager cm = CacheManager.getInstance();
		Cache cache = cm.getCache("cacheStore");
		
		 Element element = cache.get("allTableList");
		 */
		//******************End Cache Logic For Testing then Development Purpose********************************
		if ( ! StringUtils.isEmpty(validationOBJ.get(Constants.REFERENCE_TABLE_NAME).asText())) {
			tableName =validationOBJ.get(Constants.REFERENCE_TABLE_NAME).asText();
		}else {
			throw new CustomStatusException(Constants.HTTPSTATUS_400, in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_051));
		}
		
		if (code != null && columnNode.get(Constants.DIMENSIONS) !=null) 
		{
			paramValueMap = prepareParamValuesMap(code,columnNode.get(Constants.DIMENSIONS),dataMap);
		}
		else if (code != null ) {
		paramValueMap=new HashMap<String,String>();
		paramValue = getParamValuesRange(code,dataMap,mappedColName);
		paramValueMap.put(Constants.BLANK, paramValue);
		}
		
		query.append("select ERROR_MIN,ERROR_MAX,ALERT_MIN,ALERT_MAX from "+tableName+" where ");
			startTime  = System.currentTimeMillis();
			columnList = DBUtils.getRefTableMetaData(tableName);
			//logger.debug(String.format("Time taken for getRefTableMetaData() in prepareRefernceRangeObject..%s", (System.currentTimeMillis() - startTime)));
			for (String item : columnList) {
			  
			    if(!(item.equalsIgnoreCase("CTX_CTRY_CODE") || item.equalsIgnoreCase("CTX_SUPER_SECTOR") || item.equalsIgnoreCase("CTX_SECTOR")
			    		|| item.equalsIgnoreCase("CTX_SUBSECTOR") || item.equalsIgnoreCase("ERROR_MIN") || item.equalsIgnoreCase("ALERT_MIN")
			    		|| item.equalsIgnoreCase("ALERT_MAX") || item.equalsIgnoreCase("ERROR_MAX")))
			    	lookUpColumn = item;
			   
			}
		if(dataMap.get(lookUpColumn)!=null)
		{
			query.append(lookUpColumn+"='"+dataMap.get(lookUpColumn).asText()+"'");
		}else {
			/*throw new CustomStatusException(Constants.HTTPSTATUS_400, 
					String.format(Constants.ERROR_LOOKUP_COLUMN_MISSING_IN_DB,tableName));*/
			paramValueList =  new ArrayList<String>();
			paramValueList.add(tableName);
			throw new CustomStatusException(Constants.HTTPSTATUS_400, in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_052),
					in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_052+Constants.PARAMS),paramValueList);
		}
		int j=0;
		for(int i=0; i < columnList.size();i++)
		{
		if(j==0)
		{
			 if(columnList.get(i).equalsIgnoreCase("CTX_CTRY_CODE"))
			 {
			j++;
				 query.append(" and "+columnList.get(i)+"='"+(dataMap.get(Constants.CONTEXTDATAKEY+".ctryCode")).asText()+"'");
			 }
			 
			 else if (columnList.get(i).equalsIgnoreCase("CTX_SUPER_SECTOR"))
			 {
				 j++;
				 query.append(" and "+columnList.get(i)+"='"+(dataMap.get(Constants.CONTEXTDATAKEY+".industry.superSector")).asText()+"'");
			 }
			 else if(columnList.get(i).equalsIgnoreCase("CTX_SECTOR"))
			 {
				 j++;
				 query.append(" and "+columnList.get(i)+"='"+(dataMap.get(Constants.CONTEXTDATAKEY+".industry.sector")).asText()+"'");
			 }
			 else if(columnList.get(i).equalsIgnoreCase("CTX_SUBSECTOR"))
			 {
				 j++;
				 query.append(" and "+columnList.get(i)+"='"+(dataMap.get(Constants.CONTEXTDATAKEY+".industry.subSector")).asText()+"'");
			 }
		}
		else
		{
			if(columnList.get(i).equalsIgnoreCase("CTX_CTRY_CODE"))
				query.append(" or "+columnList.get(i)+"='"+(dataMap.get(Constants.CONTEXTDATAKEY+".ctryCode")).asText()+"'");
				 else if (columnList.get(i).equalsIgnoreCase("CTX_SUPER_SECTOR"))
					 query.append(" or "+columnList.get(i)+"='"+(dataMap.get(Constants.CONTEXTDATAKEY+".industry.superSector")).asText()+"'");
				 else if(columnList.get(i).equalsIgnoreCase("CTX_SECTOR"))
					 query.append(" or "+columnList.get(i)+"='"+(dataMap.get(Constants.CONTEXTDATAKEY+".industry.sector")).asText()+"'");
				 else if(columnList.get(i).equalsIgnoreCase("CTX_SUBSECTOR"))
					 query.append(" or "+columnList.get(i)+"='"+(dataMap.get(Constants.CONTEXTDATAKEY+".industry.subSector")).asText()+"'");
		}
		}
		//logger.info(String.format("query..%s", query));
		startTime  = System.currentTimeMillis();
		rangeMap = DBUtils.getRangeDetails(query.toString());
		//logger.debug(String.format("Time taken for getRangeDetails() in prepareRefernceRangeObject..%s", (System.currentTimeMillis() - startTime)));
		if(rangeMap!=null && rangeMap.get("SQLException")!=null)
		{
			return null;
		}
		
		objList= getRangeResult(paramValueMap,validationOBJ,columnNode,paramValue,rangeMap.get("ERROR_MIN"),rangeMap.get("ALERT_MIN"),
				rangeMap.get("ERROR_MAX"),rangeMap.get("ALERT_MAX"));
		//logger.info(String.format("rangeResult...%s", rangeResult));
			
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_PREPAREREFENCERANGEOBJECT));
		return objList;
		
	}
	// Reference table range logic check PE : 6738, PE 6027 start
	public static String getParamValuesRange(JsonNode code,HashMap<String, JsonNode> dataMap,JsonNode mappedColName)throws Exception
	{
		String paramValue = "";
		if(code!=null && !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(code.asText())))
		{
			if(dataMap.get(code.asText()).asText()!=null && !dataMap.get(code.asText()).asText().equals(Constants.BLANK))
			{
				paramValue = dataMap.get(code.asText()).asText();
			}
			else if (mappedColName!=null && mappedColName.asText()!=null)
			{
				if(dataMap.get(mappedColName.asText()).asText()!=null && !dataMap.get(mappedColName.asText()).asText().equals(Constants.BLANK))
				{
					paramValue = dataMap.get(mappedColName.asText()).asText();
				}
			}
		}
		return paramValue;
	}
	// Reference table range logic check PE : 6738
	
	public static ArrayList<ObjectNode> getRangeResult(HashMap<String,String> paramValueMap,JsonNode validationOBJ,JsonNode columnNode,
			String paramValue,String minError,String minAlert,String maxError,String maxAlert)throws Exception
	{
	
		String warning = "";
		String warningMsg = null;
		String value="";
		ObjectNode obj = null;
		ArrayList<ObjectNode> objList= new ArrayList<ObjectNode>();
	
	if(paramValueMap!=null && paramValueMap.size()>0)
	{
		for (Map.Entry<String, String> entry : paramValueMap.entrySet()) {
			warningMsg=null;
		    value= entry.getValue();
		   if(JsonUtils.isStringExist(value))
		   {
			if(DataUtility.isStringNotNullAndBlank(minError) &&  !StringUtils.isEmpty(value) 
					&& Double.parseDouble(value) <
					Double.parseDouble(minError)) {
				warning = Constants.ERROR;
				//warningMsg = Constants.ERROR_MIN_MSG;		//"minError";
				warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_024);	
			}
			if(null==warningMsg && DataUtility.isStringNotNullAndBlank(maxError) && Double.parseDouble(value) > 
					Double.parseDouble(maxError)) {
				warning = Constants.ERROR;
				//warningMsg = Constants.ERROR_MAX_MSG;	// "maxError";
				warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_027);
			}
			if(null==warningMsg && DataUtility.isStringNotNullAndBlank(minAlert) && Double.parseDouble(value) <
					Double.parseDouble(minAlert)) {
				warning = Constants.ALERT;
				//warningMsg = Constants.ALERT_MIN_MSG;		// "minAlert";
				warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_025);
			}
			if(null==warningMsg && DataUtility.isStringNotNullAndBlank(maxAlert) && Double.parseDouble(value) > 
					Double.parseDouble(maxAlert)) {
				warning = Constants.ALERT;
				//warningMsg = Constants.ALERT_MAX_MSG;	// "maxAlert";
				warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_026);
			}
		    }
			//logger.info(String.format("warning :: %s warningMsg ::  %s", warning,warningMsg));
			if (JsonUtils.isStringExist(warningMsg) && JsonUtils.isStringExist(warning)) {	
				if(warning.equals(Constants.ERROR))
					obj = new APIExpressionEvaluator().prepareOutputJson(validationOBJ, columnNode, warningMsg,
							ValidationTypes.rangeValidationRefTable, columnNode.get(Constants.CODE).asText(),"ERROR",
							null, entry.getKey()); // PE-3711
					else if(warning.equals(Constants.ALERT))
					obj = new APIExpressionEvaluator().prepareOutputJson(validationOBJ, columnNode, warningMsg,
							ValidationTypes.rangeValidationRefTable, columnNode.get(Constants.CODE).asText(),"ALERT",null, entry.getKey());
				objList.add(obj);
				}
			
		}
	}
	return objList;
}
//	Reference table range logic check PE : 6738, PE 6027 end
	
	public static HashMap<String,String> prepareParamValuesMap(JsonNode code,JsonNode dimension,HashMap<String, JsonNode> dataMap) throws Exception{
		
		ArrayList<String> dimList=null;
		StringBuffer key=null;
		key=new StringBuffer();
		HashMap<String,String> paramValueMap=new HashMap<String,String>();
		dimList = extractDimensions(dimension);
		
		for(int i=0;i<dimList.size();i++)
		{
			key.delete(0,key.length());
			key.append(code.asText()).append(".").append(dimList.get(i));
			
			if(dataMap.get(key.toString())!=null)
			{
			paramValueMap.put(dimList.get(i), dataMap.get(key.toString()).asText());
			}
		}
		return paramValueMap;
		
	}
	public static ArrayList<String> extractDimensions(JsonNode node) throws Exception
	{
	ArrayList<String> list = new ArrayList<String>();
	if (node != null && node.getNodeType() == JsonNodeType.ARRAY) {
		Iterator<JsonNode> it = node.iterator();
		while (it.hasNext()) {
			list.add(it.next().asText());
		}
	}
	return list;
	}
	
	//PE7044 This method will execute the MDA Set Default expression and return the result of the expression
	public String expressionEvaluatorForMDA(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, Map<String, Integer> cscDuplicateChkMap) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_EXPRESSIONEVALUATOR));
		String expr = "";
		String result = "";
		boolean checkBox = false;
		String fieldArr[] = null;
		String prefix = "this.";
		String patternString = "";
		ArrayList<String> paramValueList=null;
		Pattern pattern = Pattern.compile(Constants.EXP_PATTERN);
		if (columnNode != null && columnNode.get(Constants.QUESTION_TYPE) != null
				&& columnNode.get(Constants.QUESTION_TYPE).asText().equalsIgnoreCase("checkboxes")) {
			checkBox = true;
			pattern = Pattern.compile(Constants.EXP_PATTERN_CHECKBOX);
		} else {
			pattern = Pattern.compile(Constants.EXP_PATTERN);
		}

		Map<String, String> paramValues = new HashMap<String, String>();
		if (validationOBJ != null && validationOBJ.get(Constants.MDA) != null) {
			expr = validationOBJ.get(Constants.MDA).asText().replaceAll(Constants.CURLY_BRACES, "");
			patternString = expr;
			if (expr != null && expr.contains("bool_in_lookup")) {
				patternString = expr.replaceAll(Constants.REPLACE_FROM, Constants.REPLACE_TO);
			}
			Matcher matcher = pattern.matcher(patternString);
			List<String> fields = new ArrayList<String>();
			while (matcher.find()) {
				fields.add(matcher.group().replace("this.", "").trim());
			}

			for (String field : fields) {
				if (field != null && field.trim().contains("contextData")) {
					prefix = prefix.replace("this.", "");
				}
				if (checkBox) {
					fieldArr = extractFields(field.trim());
					if (fieldArr != null) {	
						paramValues.put(prefix + field.trim(), (dataMap.get(fieldArr[0]) == null)
								? (dataMap.get("otherSectionsData." + fieldArr[0].trim()) == null ? "null"
										: findInArray(dataMap.get("otherSectionsData." + fieldArr[0]), fieldArr[1]))
								: findInArray(dataMap.get(fieldArr[0]), fieldArr[1]));
					} else {
						paramValues.put(prefix + field.trim(), (dataMap.get(field.trim()) == null)
								? (dataMap.get("otherSectionsData." + field.trim()) == null ? "null"
										: getContextDataValues(dataMap.get("otherSectionsData." + field.trim()).asText().replace(",", "")))
								:  getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(field.trim(), "dataType"),
										dataMap.get(field.trim()).asText().replace(",", "")));
					}
				} else {
					String tmp = null;
					if (dataMap.get(field.trim()) == null) {
						if (dataMap.get("otherSectionsData." + field.trim()) == null) {
							if (cscDuplicateChkMap != null && cscDuplicateChkMap.containsKey(field.trim())) {
								paramValueList =  new ArrayList<String>();
								paramValueList.add(""+cscDuplicateChkMap.get(field.trim()));
								paramValueList.add(""+field.trim());
								paramValueList.add(""+expr);
								throw new CustomStatusException(Constants.HTTPSTATUS_400,
										in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_048),
										in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_048+Constants.PARAMS),paramValueList);// Cannot find variable {1} in expression {2}
							} else {
								throw new JSONException(String.format(Constants.ERROR_CANNOT_FIND_VARIABLE_IN_EXPR,
										field.trim(), expr)); 
							}
						} else {
							tmp = getContextDataValues(dataMap.get("otherSectionsData." + field.trim()).asText().replace(",", ""));
						}
					} else {
						tmp = getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(field.trim(), "dataType"),
								dataMap.get(field.trim()).asText().replace(",", ""));
					}
					paramValues.put(prefix + field.trim(), tmp); 
				}
				prefix = "this.";
			}
			expr = expr.replace("null", "\'null\'");
			try {
				result = expressionEvaluator.expressionEvaluator(paramValues, expr, false);

			}  catch (Exception e) {
				logger.error("Excepion occured in expressionEvaluator.."+e.getMessage());
				paramValueList =  new ArrayList<String>();
				paramValueList.add(e.getMessage());
				throw new CustomStatusException(Constants.HTTPSTATUS_400,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_040),
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_040+Constants.PARAMS),paramValueList);
			}
		}
		return result;
	}
}
