package in.lnt.validations;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import in.lnt.enums.DataTypes;
import in.lnt.enums.TransformationTypes;
import in.lnt.enums.ValidationTypes;
import in.lnt.mongo.MongoLoader;
import in.lnt.parser.CSVParser;
import in.lnt.parser.JsonAsMap;
import in.lnt.parser.JsonParser;

public class ValidationGenerator extends Thread {
	
	private static final Logger logger = LoggerFactory.getLogger(ValidationGenerator.class);
	
	long startTime = 0;
	// static Map<String, JSONObject> mapping = new HashMap<>();
	List<HashMap<String, String>> data = null;
	JSONArray dataValidated = new JSONArray();
	String validationObjectString = "";
	ObjectMapper mapper = new ObjectMapper();

	public ValidationGenerator(JSONArray dataValidated) {
		this.dataValidated = dataValidated;
	}

	public ValidationGenerator(long startTime) {
		 this.startTime = startTime;
	}

	Map<String, JSONObject> parseData(InputStream inputStreaJSON, InputStream inputStreamData, String fileType) {
		Map<String, JSONObject> mapping = new HashMap<>();
		try {
			mapping = JsonParser.parseJSON(inputStreaJSON);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			/*Throwable t= new Throwable("Internal Server Error");
			throw new CustomAPIException(HttpStatus.INTERNAL_SERVER_ERROR,t,e.getMessage());*/
		}
		logger.info("FILETYPE : " + fileType);
		if (StringUtils.containsIgnoreCase(fileType, "csv")) {
			data = (List<HashMap<String, String>>) CSVParser.parseFile(inputStreamData);
		} else if (StringUtils.containsIgnoreCase(fileType, "json")) {
			data = JsonAsMap.getJsonAsMap(inputStreamData);
			// data.remove(0);
		}

		// checking if mapping objects and the number of fields in data are same. 
		logger.info((null != data) + " ============== " + !data.isEmpty());
		if (null != data && !data.isEmpty()) {
			 
			logger.info( "\n" + data.get(0).keySet().size() + " "
					+ mapping.size() + " " + data.size());
			if (data.get(0).keySet().size() != mapping.size()) {
				mapping.clear();
				mapping = null;
				return null;
			}
			/*
			 * else if (data.size() <= 3) { return null; }
			 */
		} else {
			return null;
		}
		logger.info("Mapping file objects -> " + mapping.size() + " & Data file records -> " + data.size()
				+ " and No of fields in data : " + data.get(0).keySet().size());
		return mapping;
	}

	public JSONArray ValidationDriver(InputStream inputStreaJSON, InputStream inputStreamCSV, String fileType) {
		Map<String, JSONObject> mappingNew=null;
		try
		{
		mappingNew = parseData(inputStreaJSON, inputStreamCSV, fileType);
		
		if (null == mappingNew) {
			return null;
		}
	}
	catch(Exception  e)
		{
		e.printStackTrace();
		/*Throwable t= new Throwable("Internal Server Error");
		throw new CustomAPIException(HttpStatus.INTERNAL_SERVER_ERROR,t,e.getMessage());*/
		}
		return iterateOverDataForValidation(mappingNew);
	}

	JSONArray iterateOverDataForValidation(Map<String, JSONObject> mapping) {
		JSONArray dataArray = new JSONArray();
		List<String> mappingKeys = null;
		try{
		mappingKeys = new ArrayList<String>(mapping.keySet());
		// List<Integer> dataSize = divideDataForThreads(data.size());
		for (int i = 0; i < data.size(); i++) {
			MandatoryValidations mandatoryValidationsObject = new MandatoryValidations();

			// Putting validations error in map
			JSONObject jsonObj = new JSONObject();

			for (int j = 0; j < mappingKeys.size(); j++) {
				JSONObject mappingObject = mapping.get(mappingKeys.get(j));
				if (null != mappingObject) {

					// Iterating Over Validations
					JSONArray validationArray = (JSONArray) mappingObject.get("validations");
					for (int k = 0; k < validationArray.length(); k++) {
						List<LinkedHashMap> validationObjectList = (List<LinkedHashMap>) validationArray.get(k);

						if (validationObjectList.size() == 0) {
							mandatoryValidationsObject.checkDataType(data.get(i), mappingKeys.get(j),
									(JSONArray) mappingObject.get("dataType"));
						}

						for (LinkedHashMap validationObjectMap : validationObjectList) {
							if (EnumUtils.isValidEnum(ValidationTypes.class,
									(String) validationObjectMap.get("validationType"))) {
								switch (ValidationTypes.valueOf((String) validationObjectMap.get("validationType"))) {
								case required:
									mandatoryValidationsObject.checkMandatoryFields(data.get(i), mappingKeys.get(j),
											(JSONArray) mappingObject.get("dataType"));
									break;
								case oneOf:
									mandatoryValidationsObject.checkValidationTypeOneOf(
											data.get(i).get(mappingKeys.get(j)), mappingKeys.get(j),
											(List<?>) validationObjectMap.get("values"),
											(JSONArray) mappingObject.get("dataType"));
									break;
								case expression:
									mandatoryValidationsObject.expressionEvaluator(data.get(i),
											validationObjectMap, mappingKeys.get(j));
									break;
								default:
									break;
								}
							}
						}
					}
					// Iterating Over Transformations
					if (null != data.get(i).get(mappingKeys.get(j))) {

						if (mappingObject.has("transformations")) {

							JSONArray transformationsArray = (JSONArray) mappingObject.get("transformations");
							for (int k = 0; k < transformationsArray.length(); k++) {
								List<LinkedHashMap> transformationObjectList = (List<LinkedHashMap>) transformationsArray
										.get(k);
								for (LinkedHashMap transformationObjectMap : transformationObjectList) {
									if (null != transformationObjectMap.get("tranformationType")) {
										if (EnumUtils.isValidEnum(TransformationTypes.class,
												(String) transformationObjectMap.get("tranformationType"))) {
											TransformationCreator.transformationsCreator(mappingKeys.get(j),
													(String) transformationObjectMap.get("tranformationType"),
													data.get(i));
										}
									}

								}
							}
						}
					}

					// Getting field value
					JSONArray dataType = (JSONArray) mappingObject.get("dataType");
					// logger.info(String.valueOf(dataType.get(0)).toUpperCase());

					Pattern INT_PATTERN = Pattern.compile("^([+-]?[0-9]\\d*|0)$");
					Pattern DOUBLE_PATTERN = Pattern.compile("\\d+\\.\\d+");
					Pattern LONG_PATTERN = Pattern.compile("^-?\\d{1,19}$");

					switch (DataTypes.valueOf(String.valueOf(dataType.get(0)).toUpperCase())) {
					case STRING:
						jsonObj.put(mappingKeys.get(j), (null != data.get(i).get(mappingKeys.get(j)))
								? data.get(i).get(mappingKeys.get(j)) : JSONObject.NULL);
						break;
					case INTEGER:
					case INT:						
						/*jsonObj.put(mappingKeys.get(j),
								(null != data.get(i).get(mappingKeys.get(j))) ? (INT_PATTERN
										.matcher(data.get(i).get(mappingKeys.get(j)).replace(",", "")).matches())
												? Integer.valueOf(data.get(i).get(mappingKeys.get(j)).replace(",", ""))
												: Integer.valueOf(data.get(i).get(mappingKeys.get(j)).replace(",", ""))
										: JSONObject.NULL);*/
						jsonObj.put(mappingKeys.get(j),
								(null != data.get(i).get(mappingKeys.get(j))) 
								? (INT_PATTERN.matcher(data.get(i).get(mappingKeys.get(j)).replace(",", "")).matches())
												? Integer.valueOf(data.get(i).get(mappingKeys.get(j)).replace(",", ""))
												: StringUtils.isNumeric(data.get(i).get(mappingKeys.get(j)).replace(",", "")) 
													? Integer.valueOf(data.get(i).get(mappingKeys.get(j)).replace(",", ""))
													: data.get(i).get(mappingKeys.get(j)).replace(",", "")
								: JSONObject.NULL);
						
						break;
					case DOUBLE:
						jsonObj.put(mappingKeys.get(j),
								(null != data.get(i).get(mappingKeys.get(j)))
									? (DOUBLE_PATTERN .matcher(data.get(i).get(mappingKeys.get(j)).replace(",", "")).matches())
												? (Double.valueOf(data.get(i).get(mappingKeys.get(j)).replace(",", "")))
												: StringUtils.isNumeric(data.get(i).get(mappingKeys.get(j)).replace(",", "")) 
													? Double.valueOf(data.get(i).get(mappingKeys.get(j)).replace(",", ""))
													: data.get(i).get(mappingKeys.get(j)).replace(",", "")
										: JSONObject.NULL);
						break;
					case LONG:
						jsonObj.put(mappingKeys.get(j),
								(null != data.get(i).get(mappingKeys.get(j))) ? (LONG_PATTERN
										.matcher(data.get(i).get(mappingKeys.get(j)).replace(",", "")).matches())
												? Long.valueOf(data.get(i).get(mappingKeys.get(j)).replace(",", ""))
												: StringUtils.isNumeric(data.get(i).get(mappingKeys.get(j)).replace(",", "")) 
													? Long.valueOf(data.get(i).get(mappingKeys.get(j)).replace(",", ""))
													: data.get(i).get(mappingKeys.get(j)).replace(",", "")
										: JSONObject.NULL);
						break;
					case DATE:
						jsonObj.put(mappingKeys.get(j), (null != data.get(i).get(mappingKeys.get(j)))
								? data.get(i).get(mappingKeys.get(j)) : JSONObject.NULL);
						break;
					}
				}
			}
			jsonObj.put("validationErrors", mandatoryValidationsObject.validationResultArray);
			dataArray.put(jsonObj);
		}
		mapping = null;
		long estimatedTime = System.currentTimeMillis() - startTime;
		//logger.info( " startTime : " +startTime + " estimatedTime" +estimatedTime);
		logger.info("End time for Process : " + estimatedTime + " ms");
		/*ValidationGenerator validationGenerator = new ValidationGenerator(dataArray);
		validationGenerator.start();*/
		}
		catch(Exception e)
		{
			e.printStackTrace();
			/*Throwable t= new Throwable("Internal Server Error");
			throw new CustomAPIException(HttpStatus.INTERNAL_SERVER_ERROR,t,e.getMessage());*/
		}
		return dataArray;
	}

	// Run a thread for Mongo Dump
	public void run() {
		logger.info("thread for mongodump is running..." + dataValidated.length());
		MongoLoader mongoLoader = new MongoLoader();
		mongoLoader.dumpDataToMongo(dataValidated);
	}

	private List<Integer> divideDataForThreads(int size) {
		// TODO Auto-generated method stub
		List<Integer> sizes = new ArrayList<Integer>();
		int n = 4;
		while (size > 0 && n > 0) {
			int a = (int) (Math.floor(size / n / 50) * 50);
			size -= a;
			n--;
			sizes.add(a);
		}
		return sizes;
	}

}
