package in.lnt.validations;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;

import in.lnt.constants.Constants;
import in.lnt.enums.ValidationTypes;
import in.lnt.parser.CSVParser;
import in.lnt.service.db.DBUtils;
import in.lnt.utility.general.DataUtility;
import in.lnt.utility.general.JsonUtils;
import in.lnt.validations.evaluator.APIExpressionEvaluator;

public class FormValidator_CSC {
	private static final Logger logger = LoggerFactory.getLogger(FormValidator_CSC.class);
	ObjectMapper mapper = new ObjectMapper();
	List<String> uuidList = new ArrayList<String>();

	public JsonNode parseAndValidate_CrossSectionCheck(String entitiesJson, boolean isMDARequest) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_PARSEANDVALIDATE));
		JsonNode resultNode = null;
		ArrayNode entityArr = null;
		ArrayNode arr = null;
		JsonNode resultEntiryNode=null;
//			JsonNode sectionStructureNode = null;
			JsonNode dataNode=null;
//			JsonNode otherSecDataNode=null;
			JsonNode contextDataNode=null;
			JsonNode entityNode=null; 
			resultNode=mapper.createObjectNode();
			resultEntiryNode=null;
			entityArr= mapper.createArrayNode();
			
			Map.Entry<String, JsonNode> entry = null;
			Map.Entry<String, JsonNode> entry1 = null;
			ArrayNode entitiesNode = mapper.readValue(entitiesJson, ArrayNode.class);				// "Entities" node
			JsonNode sectionsNode = null;															// "sections"
			
			
			//logger.info(String.format("nodetype...%d", entitiesNode.size()));
			
			/************ Iterating entities *****************/
			for(int i=0;i<entitiesNode.size();i++) {
				entityNode = entitiesNode.get(i);
			
				//logger.debug("Iteration no.."+i);
				resultEntiryNode = mapper.createObjectNode(); 
				dataNode = mapper.createArrayNode(); //entityNode.get(Constants.DATA);
				sectionsNode = entityNode.get(Constants.SECTIONS);
				contextDataNode = entityNode.get(Constants.CONTEXTDATAKEY);

				HashMap<String, JsonNode> dataMap = new HashMap<String, JsonNode>();
				Iterator<Map.Entry<String, JsonNode>> it =null;
				
				/************ dataMap_ContextData *****************/
				/*HashMap<String, JsonNode> dataMap_ContextData = new HashMap<String, JsonNode>();
				if(contextDataNode !=null && !contextDataNode.toString().equals("")) {
//					uniqueIdColumnCode=contextDataNode.get("uniqueIdColumnCode").asText();
					Iterator<Map.Entry<String, JsonNode>> contextIt = contextDataNode.fields();
					while (contextIt.hasNext()) {
						entry = contextIt.next();
						if (entry.getValue().getNodeType() != JsonNodeType.OBJECT && entry.getValue().getNodeType() != JsonNodeType.ARRAY) {
							dataMap_ContextData.put(Constants.CONTEXTDATAKEY+"."+entry.getKey().trim(), entry.getValue());
						} else if(entry.getValue().getNodeType() == JsonNodeType.ARRAY)
						{
							Iterator<Map.Entry<String, JsonNode>> arrIt = contextDataNode.fields();
							arrIt=entry.getValue().get(0).fields();
								while(arrIt.hasNext())
								{
									entry1=	arrIt.next();
									dataMap_ContextData.put(Constants.CONTEXTDATAKEY+".industry."+entry1.getKey().trim(),entry1.getValue());
								}
						}
						else{	
							dataMap_ContextData = new FormValidator().parseJsonObjectMap(entry.getKey(), entry.getValue(), dataMap_ContextData,Constants.CONTEXTDATAKEY);
						}
					}
					entry = null;
				}*/
				
				if (contextDataNode != null && !contextDataNode.toString().equals("")) {
					Iterator<Map.Entry<String, JsonNode>> contextIt = contextDataNode.fields();
					while (contextIt.hasNext()) {
						entry = contextIt.next();
						if (entry.getValue().getNodeType() != JsonNodeType.OBJECT
								&& entry.getValue().getNodeType() != JsonNodeType.ARRAY) {
							dataMap.put(Constants.CONTEXTDATAKEY + "." + entry.getKey().trim(), entry.getValue());
						} else if (entry.getValue().getNodeType() == JsonNodeType.ARRAY) {
							Iterator<Map.Entry<String, JsonNode>> arrIt = contextDataNode.fields();
							arrIt = entry.getValue().get(0).fields();
							while (arrIt.hasNext()) {
								entry1 = arrIt.next();
								dataMap.put(Constants.CONTEXTDATAKEY + ".industry." + entry1.getKey().trim(),
										entry1.getValue());
							}
						} else {
							dataMap = new FormValidator().parseJsonObjectMap(entry.getKey(), entry.getValue(), dataMap,
									Constants.CONTEXTDATAKEY);
						}
					}
					entry = null;
				}

				/*********** Iterating sections ******************/
				List<ArrayList<JsonNode>> columnNodeList2 = new ArrayList<ArrayList<JsonNode>>();
				List<ArrayList<HashMap<String, JsonNode>>> dataMap2_Sections = new ArrayList<ArrayList<HashMap<String, JsonNode>>>();
				APIExpressionEvaluator apiExpressionEvaluator = new APIExpressionEvaluator();
				//Set Column - data type mapping - PE
				apiExpressionEvaluator.columnDataTypeMapping = new HashMap<String,String>();
				APIExpressionEvaluator.df.setMaximumFractionDigits(16);
				APIExpressionEvaluator.df.setMinimumFractionDigits(1);
				

				for (int sctnCtr=0; sctnCtr<sectionsNode.size(); sctnCtr++){
					ArrayList<HashMap<String, JsonNode>> dataMap2 = new ArrayList<HashMap<String, JsonNode>>();
					
					/*********** Iterating columns : Generate columnNodeList for all sections ******************/
					ArrayList<JsonNode> columnNodeListTmp = new ArrayList<JsonNode>();
					for(int clmCtr = 0; clmCtr<sectionsNode.get(sctnCtr).get(Constants.SECTION_STRUCTURE).get(Constants.COLUMNS).size(); clmCtr++){
						columnNodeListTmp.add(sectionsNode.get(sctnCtr).get(Constants.SECTION_STRUCTURE).get(Constants.COLUMNS).get(clmCtr));
					}
					columnNodeList2.add(columnNodeListTmp);
					
					for (ArrayList<JsonNode> columnNodeList : columnNodeList2) {
						for (JsonNode columnNode : columnNodeList) {
							if ((null != columnNode.get(Constants.CODE)
									&& !StringUtils.isBlank(columnNode.get(Constants.CODE).asText()))
									&& (null != columnNode.get(Constants.DATA_TYPE))) {
								apiExpressionEvaluator.columnDataTypeMapping.put(
										columnNode.get(Constants.CODE).asText(),
										columnNode.get(Constants.DATA_TYPE).asText());
							}
						}
					}
					
					/*********** Iterating Data : Generate DataMap for all sections ******************/
					dataNode = mapper.createArrayNode();
					for(int dataCtr = 0; dataCtr<sectionsNode.get(sctnCtr).get(Constants.DATA).size(); dataCtr++){
						HashMap<String, JsonNode> dataMapTmp = new HashMap<String, JsonNode>();
						JsonNode dNode = sectionsNode.get(sctnCtr).get(Constants.DATA).get(dataCtr);	// sectionsNode.get(0).get(Constants.DATA).get(0)	
//						for(int k=0;k<dNode.size();k++) {
							
							it = dNode.fields();
							while (it.hasNext()) {
								entry = it.next();
								if (entry.getValue().getNodeType() != JsonNodeType.OBJECT) {
									dataMapTmp.put(entry.getKey().trim(), entry.getValue());
								} else {
									dataMapTmp = new FormValidator() .parseJsonObjectMap(entry.getKey(), entry.getValue(), dataMapTmp,null);
								}  
							}
							((ArrayNode)dataNode).add(dNode);
//						}
						dataMap2.add(dataMapTmp);


					}
					//Making csv out of JSON single level
					if(null != dataNode) {

						//logger.info(dataNode.toString());
						JSONArray myArr = new JSONArray(dataNode.toString());
						Set<String> keyList = new HashSet();
						for (int j = 0; j < myArr.length(); j++) {
							JSONObject json = myArr.getJSONObject(j);
							Iterator<String> keys = json.keys();
							while (keys.hasNext()) {
								keyList.add(keys.next());
							}
						}
						CSVParser.parseFile(new ByteArrayInputStream(
								(JsonUtils.rowToString(new JSONArray(keyList)) + JsonUtils.toString(new JSONArray(keyList), myArr))
										.toString().getBytes("UTF-8")));
					
					}
					uuidList.add(apiExpressionEvaluator.expressionEvaluator.setCsvData(CSVParser.dataForAggregation, null));
					
					dataMap2_Sections.add(dataMap2);
					
				}
			
				/*********** Iterating sections ******************/
				JsonNode sectionNodeNum = null;
				ArrayNode sectionArray = mapper.createArrayNode();
				for (int sctnCtr=0; sctnCtr<sectionsNode.size(); sctnCtr++){
					sectionNodeNum = mapper.createObjectNode();
					arr = mapper.createArrayNode();
					/*********** Generate DataMap for the section ******************/
					int c=0;
					Map<String, Integer> duplicateChkMap = new HashMap<String, Integer>();
					for(ArrayList<HashMap<String, JsonNode>> dataMap2: dataMap2_Sections){
						if(sctnCtr != c){
							for(HashMap<String, JsonNode> hm : dataMap2 ){
								for(Map.Entry<String,JsonNode> e : hm.entrySet()){
									dataMap.put(e.getKey(), e.getValue());			// Put key in dataMap
									if(duplicateChkMap.containsKey(e.getKey())){
										int val= Integer.parseInt(duplicateChkMap.get(e.getKey()).toString());
										duplicateChkMap.put(e.getKey(), val+1);
									}else{
										duplicateChkMap.put(e.getKey(), 1);
									}
								}
							}
						}
							
						c++;
					}
					for(Map.Entry<String, Integer> e : duplicateChkMap.entrySet()){
						if(e.getValue()!=1){
							dataMap.remove(e.getKey());
						}
					}
					
					/*********** Iterating Data ******************/
					for(int dataCtr = 0; dataCtr<sectionsNode.get(sctnCtr).get(Constants.DATA).size(); dataCtr++){
						
						dataMap.putAll(dataMap2_Sections.get(sctnCtr).get(dataCtr));
						//logger.debug(String.format("dataMap..%s" , dataMap));
						dataNode = sectionsNode.get(sctnCtr).get(Constants.DATA);
						resultNode = validate(dataMap, columnNodeList2.get(sctnCtr), dataNode.get(dataCtr), apiExpressionEvaluator, duplicateChkMap,isMDARequest);
						arr.add(resultNode);
						
					}		// SECTION END
					((ObjectNode) (sectionNodeNum)).set(Constants.SECTIONCODE, sectionsNode.get(sctnCtr).get(Constants.SECTIONCODE));			// TODO Constants.SECTIONCODE
					((ObjectNode) (sectionNodeNum)).set(Constants.DATA, arr);
					((ArrayNode) sectionArray).add(sectionNodeNum);
					
				}
				((ObjectNode) resultEntiryNode).set(Constants.SECTIONS, sectionArray);					// Add sections
				if(null != contextDataNode)
					((ObjectNode) (resultEntiryNode)).set(Constants.CONTEXTDATAKEY, contextDataNode);	// Add ContextData
				entityArr.add(resultEntiryNode);
				
				for (String uuid : uuidList) {
					apiExpressionEvaluator.expressionEvaluator.removeCsvData(uuid);
				}
			}
			resultNode=mapper.createObjectNode().set("entities", entityArr);	  
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_PARSEANDVALIDATE));
		return resultNode;
	}

	public JsonNode validate(HashMap<String, JsonNode> dataMap, ArrayList<JsonNode> columnNodeList,
			JsonNode dataNode, APIExpressionEvaluator apiExpressionEvaluator, Map<String, Integer> cscDuplicateChkMap, boolean isMDARequest) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_VALIDATE));
		JsonNode validationNode = null;
		ObjectNode obj = null;
		String code = "";
		ArrayNode arr = null;
		JsonNode nodeDTObject = null;
		JsonNode dnCodeObject = null;
		Boolean flag = null;
		String result = null;
		JsonNode validationObjNode = null;
		ArrayList<ObjectNode> rangeObjList = null;
		ArrayList<String> dimList=null;
		//PE-5643 by default true because for true type do not fire validations.
		boolean isOptionType = true;
		JsonNode validationOBJ = null;
		JsonNode dimNode = null;
			if (columnNodeList != null && columnNodeList.size() > 0) {
				arr = mapper.createArrayNode();
				for (JsonNode node : columnNodeList) {
					code = node.get(Constants.CODE).asText();
					if (dataNode != null && dataNode.get(code) != null) {
						flag = false;
						
						 // Validate the data type object  block starts here.
						if (node.get(Constants.DATA_TYPE) != null) {
							//logger.info("Check if  dataType is not empty");
							nodeDTObject = node.get(Constants.DATA_TYPE); 
							dnCodeObject = dataNode.get(code);
							validationObjNode = node.get(Constants.VALIDATIONS);
                         // If data node  is not empty then do data type validation
                            if (dnCodeObject !=null && 
                           		 ! StringUtils.isEmpty(dnCodeObject.asText())) {    
                           	//Check for integer,boolean,double ,date and string data type object.
   							if ( DataUtility.isNumeric(dnCodeObject.asText())
   									&& "int".equalsIgnoreCase(nodeDTObject.asText())) {
   								flag = true;
   							} else if ( Boolean.parseBoolean(dnCodeObject.asText())
   									&& "boolean".equalsIgnoreCase(nodeDTObject.asText())) {
   								flag = true;
   							} else if ( DataUtility.isValidDate((dnCodeObject.asText()))
   									&& "date".equalsIgnoreCase(nodeDTObject.asText())) {
   								flag = true;
   							} else if ( DataUtility.isDecimal(dnCodeObject.asText()) 
   									&& "double".equalsIgnoreCase(nodeDTObject.asText())) { 
   								flag = true;
   							}else if ( "string".equalsIgnoreCase(nodeDTObject.asText())) {
   									flag = true;
   								}
   							} else {
   								flag = true;
   							}
   						} else { // if data node is empty then skip data type validations.
   									flag = true;
   							}
   							//logger.info(String.format("Flag value is %s", flag));
   							
   							//logger.info(String.format("VALIDATIONS %s", node.get(Constants.VALIDATIONS)));

							if(null!=node.get(Constants.VALIDATIONS)) {
									for(int valCnt=0; valCnt<node.get(Constants.VALIDATIONS).size(); valCnt++) {
										if(null!=node.get(Constants.VALIDATIONS) && null!=node.get(Constants.VALIDATIONS).get(valCnt)) { 
											validationOBJ = node.get(Constants.VALIDATIONS).get(valCnt);
	
											// Check if MDA action available
											if (null!=validationOBJ && null != validationOBJ.get(Constants.MDA)
													&& !validationOBJ.get(Constants.MDA).asText().equals("")
													&& !validationOBJ.get(Constants.VALIDATION_TYPE).asText().equals("expression"))
													 {
												
												// MDA = exclude_row
												if (validationOBJ.get(Constants.MDA).asText().equalsIgnoreCase(Constants.EXECLUE_ROW)) {
													((ObjectNode) (dataNode)).put(Constants.EXCLUDE_FLAG, "Y");
												// MDA = clear_value
												} else if (validationOBJ.get(Constants.MDA).asText().equalsIgnoreCase(Constants.CLEAR_VALUE)) {
													((ObjectNode) (dataNode)).put(code, "");
												} else {//7044 condition added
													String responseData= apiExpressionEvaluator.expressionEvaluatorForMDA(dataMap, validationOBJ,node,cscDuplicateChkMap);
													 
													if(node.get(Constants.DIMENSIONS)!=null)
													{
														dimList=APIExpressionEvaluator.extractDimensions(node.get(Constants.DIMENSIONS));
														dimNode = mapper.createObjectNode();
														for(String dim:dimList)
														{
														((ObjectNode)dimNode).put(dim,responseData);
														}
														((ObjectNode) (dataNode)).set(code, dimNode);
													}
													
													else{
																((ObjectNode) (dataNode)).put(code, responseData);
														}
												}
											}
										}
	
									}
								}
   						
   							//PE -5216 and PE-5643 Check for drop down options exist in metadata or in database.
   							if (node.get("questionType") != null
   									&& ( node.get("questionType").asText().equals("dropdown") || 
   											node.get("questionType").asText().equals("radiobutton") )) {
   								//logger.info(String.format("Question type is %s", node.get("questionType").asText()));
   								isOptionType =  checkAnswerExists(node, dataNode, code); 
   								
   								// Implementation start PE : 5643	
   								if(!isOptionType) {
   									
   									// Clear the value anyways if checkAnswerExists==false
   									((ObjectNode) (dataNode)).put(code, "");	
   								}
   								// Implementation end PE : 5643
   								
   								if ( ! isOptionType ) {
   									obj = apiExpressionEvaluator.prepareOutputJson(null, null, null, ValidationTypes.dropDown,
   											code, null, null,node.get("displayLabel").asText() );
   								}
   								if (obj != null) {
   									arr.add(obj);
   									obj = null;
   								}
   							} 

						if (node.get(Constants.VALIDATIONS) != null && node.get(Constants.VALIDATIONS).size() > 0
								&& flag) {

							for (int i = 0; i < node.get(Constants.VALIDATIONS).size(); i++) {

								validationNode = node.get(Constants.VALIDATIONS).get(i);
								if (validationNode.get(Constants.VALIDATION_TYPE) != null
										&& EnumUtils.isValidEnum(ValidationTypes.class,
												validationNode.get(Constants.VALIDATION_TYPE).asText())) {
									switch (ValidationTypes
											.valueOf(validationNode.get(Constants.VALIDATION_TYPE).asText())) {
									case required:
										obj = apiExpressionEvaluator.checkMandatoryFields(dataMap, code,
												node.get(Constants.DATA_TYPE).asText());
										break;
									case oneOf:
										obj = apiExpressionEvaluator.checkValidationTypeOneOf(dataMap, code, node,
												validationNode);
										break;
									case range:
										rangeObjList = apiExpressionEvaluator.prepareRangeObject(dataMap, validationNode, node,dataNode);
										break;
									case rangeValidationRefTable:
										rangeObjList = apiExpressionEvaluator.prepareRefernceRangeObject(dataMap, validationNode,node, dataNode);
										break;
									case expression:
										obj = apiExpressionEvaluator.expressionEvaluator(dataMap, validationNode, node,
												dataNode, cscDuplicateChkMap,columnNodeList);
										break;
									case eligibility:
										arr.addAll(apiExpressionEvaluator.checkValidity(dataMap, validationNode, node,
												dataNode, isMDARequest));
										break;
									default:
									}
								} else {
									result = Constants.MANDATORY_VALIDATIONTYPE;
									obj = apiExpressionEvaluator.prepareOutputJson(null, null, result, null, null, null,
											null, null);
								}
								if (obj != null) {
									arr.add(obj);
									obj = null;
								}
								if(rangeObjList!=null)
								{
									for(int j=0;j<rangeObjList.size();j++)
									{
										arr.add(rangeObjList.get(j));
									}
									rangeObjList=null;
								}
							}
						} else if (flag == false) {
							//logger.info("Check if flag is false");
							result = Constants.INVALID_DATATYPE_FOR_ERROR;
							if (null != nodeDTObject && null != dnCodeObject) {
								obj = apiExpressionEvaluator.prepareOutputJson(validationObjNode, node, result,
										ValidationTypes.dataTypeError, dnCodeObject.asText(), nodeDTObject.asText(),
										nodeDTObject.asText(), null);
								//logger.info(String.format("Value of object is %s", obj));
							}
							if (obj != null) {
								arr.add(obj);
								obj = null;
							}
						}

						if (node.get("questionType") != null
								&& node.get("questionType").asText().equals("checkboxes")) {

							buildCheckboxArray(dataNode,code);
						}
					}
				}

				if ((arr != null && arr.size() > 0)) {
					((ObjectNode) (dataNode)).set(code, dataNode.get(code));
					((ObjectNode) (dataNode)).set(Constants.VALIDATION_RESULTS, arr);
					arr = null;
				}
			}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_VALIDATE));
		return dataNode;
	}

	public void buildCheckboxArray(JsonNode dataNode,String code) throws Exception
	{
		if(dataNode.get(code)!=null && dataNode.get(code).getNodeType() != JsonNodeType.OBJECT)
        {
            ArrayNode arrNode = mapper.createArrayNode();
            String arr1[]=dataNode.get(code).asText().split(",");
            if(arr1!=null && arr1.length >0)
            {
                for(int i=0;i<arr1.length;i++)
                {
                    arrNode.add(arr1[i]);
                }
            }
            ((ObjectNode) (dataNode)).set(code, arrNode);
        }
		else
		{
			Iterator<Map.Entry<String, JsonNode>> it = dataNode.get(code).fields();
			Map.Entry<String, JsonNode> entry = null;
			while(it!=null && it.hasNext())
			{
				entry =it.next();
				 ArrayNode arrNode = mapper.createArrayNode();
                    String arr1[]=entry.getValue().asText().split(",");
                    if(arr1!=null && arr1.length >0)
                    {
                        for(int i=0;i<arr1.length;i++)
                        {
                            arrNode.add(arr1[i]);
                        }
                    }
                    ((ObjectNode)(dataNode.get(code))).set(entry.getKey(), arrNode);
			}
			
		}
        
	}
	public String checkUniqueNessForEEID(String enetityJson) throws Exception
	{
		String isUnique="true";
		JsonNode entityNode=null; 
		JsonNode sectionsNode = null;	
		JsonNode dataNode=null;
		HashSet<String> eeidSet=null;
			ArrayNode entitiesNode = mapper.readValue(enetityJson, ArrayNode.class);
			for(int i=0;i<entitiesNode.size();i++) {
				entityNode = entitiesNode.get(i);
				sectionsNode = entityNode.get(Constants.SECTIONS);
				for (int sctnCtr=0; sctnCtr<sectionsNode.size(); sctnCtr++){
					eeidSet = new HashSet<String>();
						dataNode = sectionsNode.get(sctnCtr).get(Constants.DATA);
						if(dataNode!=null)
						{
						for (int k = 0; k < dataNode.size(); k++) {
							if(dataNode.get(k).get(Constants.EMPLOYEE_EEID)!=null && !dataNode.get(k).get(Constants.EMPLOYEE_EEID).asText().equals(""))
							{
							String eeId = dataNode.get(k).get(Constants.EMPLOYEE_EEID).asText();
							if(!eeidSet.add(eeId))
							{
								eeidSet=null;
								return Constants.DUPLICATE;
							}	
							}
							else
							{
								return Constants.MISSING;
							}
						}
						}
				}
			}

		
		return isUnique;
	}
	
	/**
	 * 
	 * @param dropDownOptions
	 * @param dataNode
	 * @param code
	 * @param key
	 * @return boolean value
	 */
	public static boolean checkAnswerExists(JsonNode dropDownOptions, JsonNode dataNode,  String code) throws Exception{

		//logger.info("Entering checkAnswerExists method "+dataNode+" code "+code+ " key is "+ key);
		ArrayNode optionsArrNode = null;
		JsonNode dbNode = null;
		Map<String, String> optionsDictionary = null;
		Map<String,String> dbData = null;
		boolean found = false;	
		boolean fromDB = false;
		boolean fromJson = false;
		
			if(JsonUtils.isNullOrBlankOrNullNode(dataNode.get(code))){
				return true;
			}
			dbData =  new HashMap<>();
			optionsDictionary = new HashMap<>();
		if ( null != dropDownOptions && null !=  dropDownOptions.get(Constants.OPTIONS)) {
			
			if ( null != dropDownOptions.get(Constants.OPTIONS).get("items")  ){
				optionsArrNode = (ArrayNode) dropDownOptions.get(Constants.OPTIONS).get("items");
				if ( optionsArrNode.size() > 0)
					fromJson = true;
			}
			if (null != dropDownOptions.get(Constants.OPTIONS).get(Constants.REFERENCETABLE) ) {
				dbNode =  dropDownOptions.get(Constants.OPTIONS).get(Constants.REFERENCETABLE);
				fromDB = true;
				dbData = DBUtils.getRefTableData(dbNode.get(Constants.CODE).asText());
			}
			
		}
			if (fromJson) {
				for (int i = 0; i < optionsArrNode.size(); i++) {
					optionsDictionary.put(optionsArrNode.get(i).get("value").asText(),
							optionsArrNode.get(i).get("displayName").asText());
				}

				if (optionsDictionary.containsKey(dataNode.get(code).asText())) {
					found = true;
				}

			} else if (fromDB && dbData.containsKey(dataNode.get(code).asText())) {
					found = true;
					
			}
			//logger.info("Existing checkAnswerExists method ");

		return found;
	}

}
