package in.lnt.validations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.maxiq.ExpressionEvaluator.ExpressionEvaluator;

import in.lnt.enums.DataTypes;
import in.lnt.enums.ValidationTypes;

public class MandatoryValidations {

	
	private static final Logger logger = LoggerFactory.getLogger(MandatoryValidations.class);
	// static List<String> mappingKeys = new ArrayList<String>();
	JSONArray validationResultArray = new JSONArray();
	ExpressionEvaluator ExpressionEvaluator = new ExpressionEvaluator();
	void checkDataType(HashMap<String, String> hashMap, String key, JSONArray dataType) {
		Pattern INT_PATTERN = Pattern.compile("^([+-]?[0-9]\\d*|0)$");
		Pattern DOUBLE_PATTERN = Pattern.compile("\\d+\\.\\d+");
		Pattern LONG_PATTERN = Pattern.compile("^-?\\d{1,19}$");

		for (int i = 0; i < dataType.length(); i++) {
			if (EnumUtils.isValidEnum(DataTypes.class, String.valueOf(dataType.get(i)).toUpperCase())) {

				String value = String.valueOf(hashMap.get(key)).replace(",", "");
				 logger.info(value + " " + key);
				switch (DataTypes.valueOf(String.valueOf(dataType.get(i)).toUpperCase())) {
				case STRING:
					break;
				case INTEGER:
				case INT:
					if (((StringUtils.isEmpty(value)) || (value == "null")) || (INT_PATTERN.matcher(value).matches())) {
					} else {

						putJsonObject(key, hashMap.get(key), dataType.get(i).toString(), null,null,
								ValidationTypes.DataType);
					}
					break;
				case DOUBLE:
					if (((StringUtils.isEmpty(value)) || (value == "null"))
							|| (DOUBLE_PATTERN.matcher(value).matches())) {
						// logger.info("in if");
					} else {
						// logger.info("in else");
						putJsonObject(key, hashMap.get(key), dataType.get(i).toString(), null,null,
								ValidationTypes.DataType);
					}
					break;
				case DATE:
					break;
				case BOOLEAN:
					break;
				case LONG:
					if (((StringUtils.isEmpty(value)) || (value == "null"))
							|| (LONG_PATTERN.matcher(value).matches())) {
					} else {
						putJsonObject(key, hashMap.get(key), dataType.get(i).toString(), null,null,
								ValidationTypes.DataType);
					}
					break;
				}
			}
		}
	}

	void checkMandatoryFields(HashMap<String, String> hashMap, String key, JSONArray dataType) {

		if (hashMap.get(key) == null || hashMap.get(key).isEmpty()) {
			putJsonObject(key, null, null, null,null, ValidationTypes.required);
		}
		checkDataType(hashMap, key, dataType);
		return;
	}

	void checkValidationTypeOneOf(String value, String key, List<?> values, JSONArray dataType) {

		Pattern INT_PATTERN = Pattern.compile("^([+-]?[0-9]\\d*|0)$");
		Pattern DOUBLE_PATTERN = Pattern.compile("\\d+\\.\\d+");
		Pattern LONG_PATTERN = Pattern.compile("^-?\\d{1,19}$");

		if (EnumUtils.isValidEnum(DataTypes.class, values.get(0).getClass().getSimpleName().toUpperCase())) {
			String modifiedValue = String.valueOf(value).replace(",", "");

			switch (DataTypes.valueOf(values.get(0).getClass().getSimpleName().toUpperCase())) {
			case DOUBLE:
				if (DOUBLE_PATTERN.matcher(modifiedValue).matches()) {
					if (!values.contains(Double.valueOf(value))) {
						putJsonObject(key, value, null, values.toString(),null, ValidationTypes.oneOf);
					}
				} else {
					// putJsonObject(key, value, dataType.get(0).toString(),
					// null, ValidationTypes.DataType);
					putJsonObject(key, value, null, values.toString(),null, ValidationTypes.oneOf);
				}
				break;
			case STRING:
				if (!StringUtils.isBlank(value)) {
					if (!values.contains(String.valueOf(value))) {
						putJsonObject(key, value, null, values.toString(),null, ValidationTypes.oneOf);
					}
				} else {
					// putJsonObject(key, null, null, null,
					// ValidationTypes.required);
					putJsonObject(key, value, null, values.toString(),null, ValidationTypes.oneOf);
				}
				break;
			case INTEGER:
			case INT:
				if (INT_PATTERN.matcher(modifiedValue).matches()) {
					if (!values.contains(Integer.valueOf(value))) {
						putJsonObject(key, value, null, values.toString(),null, ValidationTypes.oneOf);
					}
				} else {
					// putJsonObject(key, null, dataType.get(0).toString(),
					// null, ValidationTypes.DataType);
					putJsonObject(key, value, null, values.toString(),null, ValidationTypes.oneOf);
				}
				break;
			case DATE:
				break;
			case BOOLEAN:
				break;
			case LONG:
				if (LONG_PATTERN.matcher(modifiedValue).matches()) {
					if (!values.contains(Integer.valueOf(value))) {
						putJsonObject(key, value, null, values.toString(),null, ValidationTypes.oneOf);
					}
				} else {
					// putJsonObject(key, null, dataType.get(0).toString(),
					// null, ValidationTypes.DataType);
					putJsonObject(key, value, null, values.toString(),null, ValidationTypes.oneOf);
				}
				break;
			}
		}
	}

	void expressionEvaluator(HashMap<String, String> data, LinkedHashMap validationObject, String key) {
		// (String) validationObjectMap.get("expression")
		this.validationResultArray = new JSONArray();
		if (validationObject.containsKey("expression")) {
			
			String validationObjectMap = (String) validationObject.get("expression");
			
			//Remove the "{" or "}" curly braces by space.
			validationObjectMap =  validationObjectMap.replaceAll("\\{|\\}", "");
			
			//logger.info("================= \n" + "Expression : " + validationObjectMap);
			Map<String, String> paramValues = new HashMap<String, String>();
			Matcher m = Pattern.compile("this.[\\w\\s*]+\\.[\\w\\s*]+\\.[\\w\\s*]+|this.[\\w\\s*]+\\.[\\w\\s*]+|this.[\\w+\\s*]+").matcher(validationObjectMap);
			List<String> fields = new ArrayList<String>();
			while (m.find()) {
				fields.add(m.group().replace("this.", ""));
			}
			for (String field : fields) {
				paramValues.put("this." + field.trim(),
						(data.get(field.trim()) == null) ? "null" : data.get(field.trim()).replace(",", ""));
			}

			//logger.info("Fields from Expression : " + fields.toString() + "  " + "\nparamValues: "+ paramValues.toString() + "\nExpression :" + validationObjectMap);
			validationObjectMap = validationObjectMap.replace("null", "\'null\'");
			try {
				String result = ExpressionEvaluator.expressionEvaluator(paramValues, validationObjectMap, false);
				String errorType = "";
				if(validationObject.containsKey("errorType"))
					errorType = (String) validationObject.get("errorType");
				if ((errorType.compareToIgnoreCase("ERROR") == 0) || (errorType.compareToIgnoreCase("ALERT")== 0)) {
					if (result.equals("true")) {
					} else if (result.equals("false")) {
						putJsonObject(key, data.get(key), null, paramValues.toString(), validationObject,
								ValidationTypes.expression);
					} 
				}
				
				else if(errorType.compareToIgnoreCase("AUTOCORRECT") == 0){
					//logger.info("BEFORE : " + key + " " + data.get(key));
					//Check if Original Value is Same as result of expressionEvaluator
					if (null != data.get(key) && data.get(key).equals(result)) {
						return;
						
					} else {
						validationObject.put("OriginalValue",data.get(key));
						data.put(key, result);
						putJsonObject(key, data.get(key), null, paramValues.toString(), validationObject,
								ValidationTypes.expression);
					}
					//logger.info("AFTER : " + key + " " + data.get(key));
				}
				
				// logger.info(result);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
	
	
	
	
	
	
	/**
	 * 
	 * @param key
	 *            - field name
	 * @param value
	 *            - field value
	 * @param dataType
	 *            - data Type of field
	 * @param values
	 *            - possible values of field
	 * @param validationObject - MetadData Object for Error Message
	 * @param errorType
	 *            - ErrorType of field
	 */
	void putJsonObject(String key, String value, String dataType, String values, LinkedHashMap validationObject, ValidationTypes errorType) {
		JSONObject validations = new JSONObject();
		switch (errorType) {
		case DataType:
			// logger.info("field " + key + " value " + value + "
			// DATATYPE");
			validations.put("field", key);
			validations.put("message", key + " with value " + value + " not matched with data type " + dataType);
			validationResultArray.put(validations);
			break;
		case required:
			// logger.info("field " + key + " REQUIRED");
			validations.put("field", key);
			validations.put("message", key + " is required.");
			validationResultArray.put(validations);
			break;
		case oneOf:
			validations.put("field", key);
			validations.put("message", value + " is not a valid value for this field.  Please choose from " + values);
			validationResultArray.put(validations);
			break;
		case expression:

			// dataType taken as Expression
			Iterator iterator = validationObject.keySet().iterator();
			String keyFromMetaData = "";
			while (iterator.hasNext()) {
				keyFromMetaData = (String) iterator.next();
				validations.put(keyFromMetaData,(null != validationObject.get(keyFromMetaData)) ? validationObject.get(keyFromMetaData) : JSONObject.NULL );
			}
			validations.put("field", key);
			/*validations.put("field", key);
			validations.put("message", value + " is not a valid value of this field based on these values : " + values
					+ " and expression : " + dataType);*/
			validationResultArray.put(validations);
			break;
		}
	}
}
