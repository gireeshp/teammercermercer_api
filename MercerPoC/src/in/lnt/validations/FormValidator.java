package in.lnt.validations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lti.mosaic.function.def.InversionAndRangeCheckFunctions;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.constants.Constants;
import in.lnt.enums.ValidationTypes;
import in.lnt.parser.CSVParser;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.DataUtility;
import in.lnt.utility.general.JsonUtils;
import in.lnt.validations.evaluator.APIExpressionEvaluator;

public class FormValidator {
	private static final Logger logger = LoggerFactory.getLogger(FormValidator.class);
	ObjectMapper mapper = new ObjectMapper();
	ArrayList<String> paramValueList=null;
	public JsonNode parseAndValidate(String entitiesJson, String csvJsonString, boolean isAggregateRequest,
			HashMap<String, String> xslJsonMap, String FirstSheetName) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_PARSEANDVALIDATE));
		JsonNode resultNode = null;
		ArrayNode entityArr = null;
		ArrayNode arr = null;
		Set<String> xslJsonSheets = null;
		JsonNode resultEntiryNode = null;
		JsonNode sectionStructureNode = null;
		JsonNode dataNode = null;
		JsonNode otherSecDataNode = null;
		JsonNode contextDataNode = null;
		JsonNode csvNode = null;
		String uniqueIdColumnCode = "";
		JsonNode entityNode = null;
		resultNode = mapper.createObjectNode();
		resultEntiryNode = null;
		entityArr = mapper.createArrayNode();
		Map.Entry<String, JsonNode> entry = null;
		Map.Entry<String, JsonNode> entry1 = null;
		ArrayNode entitiesNode = mapper.readValue(entitiesJson, ArrayNode.class);
		HashMap<String, String> columnMappingMap = null;
		StringBuffer coName_CtryCode = new StringBuffer();
		String countryCode = null;
		//logger.info(String.format("nodetype...%d", entitiesNode.size()));

			// For .xlsx data
			if (xslJsonMap != null) {
				xslJsonSheets = new HashSet<String>(xslJsonMap.keySet());
			}

			for (int i = 0; i < entitiesNode.size(); i++) {
				countryCode = "";
				arr = mapper.createArrayNode();
				entityNode = entitiesNode.get(i);
				resultEntiryNode = mapper.createObjectNode();
				sectionStructureNode = entityNode.get(Constants.SECTION_STRUCTURE);
				dataNode = entityNode.get(Constants.DATA);
				otherSecDataNode = entityNode.get(Constants.OTHERSECTIONDATAKEY);
				contextDataNode = entityNode.get(Constants.CONTEXTDATAKEY);
				if (null != contextDataNode && contextDataNode.has("ctryCode")) {
					countryCode = contextDataNode.get("ctryCode").asText();
				}
				if (contextDataNode != null && contextDataNode.get("entityNameColumnCode") != null
						&& !contextDataNode.get("entityNameColumnCode").equals("")) {

				}
				if (csvJsonString != null && !csvJsonString.equals("")) {
					csvNode = mapper.readValue(csvJsonString, JsonNode.class);
				} else if (xslJsonMap != null && xslJsonMap.size() > 0) {
					/*String coName_CtryCode = contextDataNode.get("companyName").asText() + "_"
							+ contextDataNode.get("ctryCode").asText();*/
					coName_CtryCode.delete(0,coName_CtryCode.length());
					coName_CtryCode.append(contextDataNode.get("companyName").asText());
					coName_CtryCode.append("_");
					coName_CtryCode.append(contextDataNode.get("ctryCode").asText());

					// Default condition - Need to check usage of FirstSheetName !!
					if (FirstSheetName != null) {
						csvNode = mapper.readValue(xslJsonMap.get(FirstSheetName), JsonNode.class);

						// If sheet is found
					} else if (xslJsonMap.get(coName_CtryCode.toString()) != null) {

						// If sheet not previously used
						if (xslJsonSheets.contains(coName_CtryCode.toString())) {
							csvNode = mapper.readValue(xslJsonMap.get(coName_CtryCode.toString()), JsonNode.class);
							xslJsonSheets.remove(coName_CtryCode.toString());

							// If sheet previously used
						} else {
							/*throw new CustomStatusException(Constants.HTTPSTATUS_400,
									String.format(Constants.ERROR_REPEATED_ENTITY_FOR_COMPANY_COUNTRY,
											contextDataNode.get("companyName").asText(),
											contextDataNode.get("ctryCode").asText()));*/
							paramValueList =  new ArrayList<String>();
							paramValueList.add(""+contextDataNode.get("companyName").asText());
							paramValueList.add(""+contextDataNode.get("ctryCode").asText());
							throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_034),
									Cache.getPropertyFromError(ErrorCacheConstant.ERR_034+Constants.PARAMS),paramValueList);
						}

						// If sheet not found
					} else if (xslJsonMap.get(coName_CtryCode.toString()) == null) {
						/*throw new CustomStatusException(Constants.HTTPSTATUS_400,
								String.format(Constants.ERROR_SHEET_NOT_FOUND, coName_CtryCode.toString()));*/
						paramValueList =  new ArrayList<String>();
						paramValueList.add(""+coName_CtryCode.toString());
						throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_035),
								Cache.getPropertyFromError(ErrorCacheConstant.ERR_035+Constants.PARAMS),paramValueList);
					}
				}
				HashMap<String, JsonNode> dataMap = new HashMap<String, JsonNode>();
				Iterator<Map.Entry<String, JsonNode>> it = null;
				ArrayList<JsonNode> columnNodeList = new ArrayList<JsonNode>();
				columnMappingMap = new HashMap<String, String>();
				for (int j = 0; j < sectionStructureNode.get(Constants.COLUMNS).size(); j++) {
					columnNodeList.add(sectionStructureNode.get(Constants.COLUMNS).get(j));
					if (sectionStructureNode.get(Constants.COLUMNS).get(j).get("mappedColumnName") != null
							&& !sectionStructureNode.get(Constants.COLUMNS).get(j).get("mappedColumnName").equals(""))
						if (sectionStructureNode.get(Constants.COLUMNS).get(j).get("code") != null)
							columnMappingMap.put(
									sectionStructureNode.get(Constants.COLUMNS).get(j).get("code").asText(),
									sectionStructureNode.get(Constants.COLUMNS).get(j).get("mappedColumnName")
											.asText());
				}

				if (otherSecDataNode != null && !otherSecDataNode.toString().equals("")) {
					Iterator<Map.Entry<String, JsonNode>> osdIt = otherSecDataNode.fields();
					while (osdIt.hasNext()) {
						entry = osdIt.next();
						if (entry.getValue().getNodeType() != JsonNodeType.OBJECT) {
							dataMap.put(Constants.OTHERSECTIONDATAKEY + "." + entry.getKey().trim(), entry.getValue());
						} else {
							dataMap = parseJsonObjectMap(entry.getKey(), entry.getValue(), dataMap,
									Constants.OTHERSECTIONDATAKEY);
						}
					}
					entry = null;
				}

				// Populate dataMap with values from "contextDataNode"
				if (contextDataNode != null && !contextDataNode.toString().equals("")) {
					uniqueIdColumnCode = contextDataNode.get("uniqueIdColumnCode").asText();
					Iterator<Map.Entry<String, JsonNode>> contextIt = contextDataNode.fields();
					while (contextIt.hasNext()) {
						entry = contextIt.next();
						if (entry.getValue().getNodeType() != JsonNodeType.OBJECT
								&& entry.getValue().getNodeType() != JsonNodeType.ARRAY) {
							dataMap.put(Constants.CONTEXTDATAKEY + "." + entry.getKey().trim(), entry.getValue());
						} else if (entry.getValue().getNodeType() == JsonNodeType.ARRAY) {
							Iterator<Map.Entry<String, JsonNode>> arrIt = contextDataNode.fields();
							arrIt = entry.getValue().get(0).fields();
							while (arrIt.hasNext()) {
								entry1 = arrIt.next();
								dataMap.put(Constants.CONTEXTDATAKEY + ".industry." + entry1.getKey().trim(),
										entry1.getValue());
							}
						} else {
							dataMap = parseJsonObjectMap(entry.getKey(), entry.getValue(), dataMap,
									Constants.CONTEXTDATAKEY);
						}
					}
					entry = null;
				}

				// findAndMerge(dataNode.get(k).get(uniqueIdColumnCode).asText(),csvJsonString)
				if (csvNode != null) {
					dataNode = findAndMerge(dataNode, (ArrayNode) csvNode.get("data"), uniqueIdColumnCode);
				}

				// Making csv out of JSON single level
				if (null != dataNode) {
					JSONArray myArr = new JSONArray(dataNode.toString());
					Set<String> keyList = new HashSet();
					for (int j = 0; j < myArr.length(); j++) {
						JSONObject json = myArr.getJSONObject(j);
						Iterator<String> keys = json.keys();
						while (keys.hasNext()) {
							keyList.add(keys.next());
						}
					}
					
					CSVParser.parseFile(new ByteArrayInputStream(
							(JsonUtils.rowToString(new JSONArray(keyList)) + JsonUtils.toString(new JSONArray(keyList), myArr))
									.toString().getBytes("UTF-8")));
				}

				APIExpressionEvaluator apiExpressionEvaluator = new APIExpressionEvaluator();
				//Set Column - data type mapping - PE
				apiExpressionEvaluator.columnDataTypeMapping = new HashMap<String,String>();
				APIExpressionEvaluator.df.setMaximumFractionDigits(16);
				APIExpressionEvaluator.df.setMinimumFractionDigits(1);

				for (JsonNode columnNode : columnNodeList) {
					if ((null != columnNode.get(Constants.CODE)
							&& !StringUtils.isBlank(columnNode.get(Constants.CODE).asText()))
							&& (null != columnNode.get(Constants.DATA_TYPE))) {
						apiExpressionEvaluator.columnDataTypeMapping.put(columnNode.get(Constants.CODE).asText(),
								columnNode.get(Constants.DATA_TYPE).asText());
					}
				} 
				
				HashMap<String, Object> otherParams = null;
				if (countryCode != null || !countryCode.equals("")) {
					otherParams = new HashMap<String, Object>();
					otherParams.put("countryCode", countryCode);
				}
				String uuid = apiExpressionEvaluator.expressionEvaluator.setCsvData(CSVParser.dataForAggregation,
						otherParams);
				if (!isAggregateRequest) {
					for (int k = 0; k < dataNode.size(); k++) {

						it = dataNode.get(k).fields();
						while (it.hasNext()) {
							entry = it.next();
							if (entry.getValue().getNodeType() != JsonNodeType.OBJECT) {
								dataMap.put(entry.getKey().trim(), entry.getValue());
							} else {
								dataMap = parseJsonObjectMap(entry.getKey(), entry.getValue(), dataMap, null);
							}
						}
						//logger.debug(String.format("dataMap..%s", dataMap));
						resultNode = validate(dataMap, columnNodeList, dataNode.get(k),apiExpressionEvaluator, false,
								columnMappingMap);
						arr.add(resultNode);
						//logger.debug(String.format("resultNode..%s", resultNode));

					}
				} else {

					// PE : 2449 to update datamap with entity data which was missing in case of
					// aggregate
					it = dataNode.get(0).fields();
					while (it.hasNext()) {
						entry = it.next();
						if (entry.getValue().getNodeType() != JsonNodeType.OBJECT) {
							dataMap.put(entry.getKey().trim(), entry.getValue());
						} else {
							dataMap = parseJsonObjectMap(entry.getKey(), entry.getValue(), dataMap, null);
						}
					}
					resultNode = validate(dataMap, columnNodeList, dataNode.get(0),apiExpressionEvaluator,
							isAggregateRequest, columnMappingMap);
				}

				if (!isAggregateRequest) {
					//((ObjectNode) (resultEntiryNode)).set(Constants.DATA, resultNode);
					((ObjectNode) (resultEntiryNode)).set(Constants.DATA, arr);
				} else {
					((ObjectNode) (resultEntiryNode)).set(Constants.VALIDATION_RESULTS, resultNode);
				}
				if (null != contextDataNode)
					((ObjectNode) (resultEntiryNode)).set(Constants.CONTEXTDATAKEY, contextDataNode);
				entityArr.add(resultEntiryNode);

				// removing csv from expression evaluator
				// apiExpressionEvaluator.expressionEvaluator.removeCsvData(uuid);
			}
			resultNode = mapper.createObjectNode().set("entities", entityArr);
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_PARSEANDVALIDATE));
		return resultNode;
	}

	public HashMap<String, JsonNode> parseJsonObjectMap(String key, JsonNode value, HashMap<String, JsonNode> map,
			String datakey) throws Exception {
		Iterator<Map.Entry<String, JsonNode>> it = value.fields();
		Map.Entry<String, JsonNode> entry = null;
			while (it.hasNext()) {
				entry = it.next();
				if (entry.getValue().getNodeType() != JsonNodeType.OBJECT) {
					if (datakey != null) {
						map.put((datakey + "." + key + "." + entry.getKey()).trim(), entry.getValue());
					} else {
							map.put((key + "." + entry.getKey()).trim(), entry.getValue());
					}	
				} else {
					parseJsonObjectMap(key + "." + entry.getKey(), entry.getValue(), map, datakey);
				}
			}
		return map;
	}

	public JsonNode validate(HashMap<String, JsonNode> dataMap, ArrayList<JsonNode> columnNodeList, JsonNode dataNode,
			 APIExpressionEvaluator apiExpressionEvaluator, boolean isAggregateRequest,
			HashMap<String, String> columnMappingMap) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_VALIDATE));
		JsonNode validationNode = null;
		ObjectNode obj = null;
		ArrayList<ObjectNode> rangeObjList = null;
		String code = "";
		String mappedColumnName = null;
		ArrayNode arr = null;
		JsonNode nodeDTObject = null;
		JsonNode dnCodeObject = null;
		Boolean flag = null;
		String result = null;
		String skip = null;
		JsonNode validationObjNode = null;
		ObjectNode jobInversionObjNode = null;
		ArrayNode jobInversionArr = null;
		Iterator<Map.Entry<String, JsonNode>> it = null;
		Map.Entry<String, JsonNode> entry = null;
		//PE-5643 by default true because for true type do not fire validations.
		boolean isOptionType = true;
		
		//PE-5691
		boolean isEmtpyRecord = false;
		
		//PE 7154 Improvement in old story data type check.
		String dnObjetText = null;
		
			if (columnNodeList != null && columnNodeList.size() > 0) {
				arr = mapper.createArrayNode();

				// If request is AggregateRequest
				if (isAggregateRequest) {
					for (JsonNode node : columnNodeList) {
						if (node.get(Constants.VALIDATIONS) != null && node.get(Constants.VALIDATIONS).size() > 0) {

							for (int i = 0; i < node.get(Constants.VALIDATIONS).size(); i++) {

								validationNode = node.get(Constants.VALIDATIONS).get(i);

								// For Inversion
								obj = apiExpressionEvaluator.expressionEvaluator(dataMap, validationNode, node,
										dataNode,isAggregateRequest, columnMappingMap,columnNodeList);
								if (obj != null) {
									arr.add(obj);
									obj = null;
								}
							}
						}
					}

					// PE-4843 Append JobInversion Object
					logger.info("Creating JobInversion object...");
					jobInversionObjNode = mapper.createObjectNode();
					jobInversionObjNode.put(Constants.ERROR_GROUP, Constants.PERSONAL);
					jobInversionObjNode.put(Constants.ERROR_TYPE, Constants.ERROR);
					jobInversionObjNode.put(Constants.VALIDATION_TYPE, Constants.EXPRESSION);
					jobInversionObjNode.put(Constants.MESSAGE, Constants.JOB_INVERSION_ERRORS);
					jobInversionObjNode.put(Constants.AGGREGATE_DISPLAY_LABEL, Constants.MEDIAN_PAY);
					jobInversionObjNode.put(Constants.FIELD, Constants.ANNUAL_BASE);
					jobInversionObjNode.put(Constants.CATEGORY, Constants.JOB_INVERSION);
										
					logger.info("Before calling JobInversion.jobInversion()...");
					//Implemented as per code review comments..
					//logger.info(String.format("csvData :: %s csvData size :: %d", EvalVisitor.data.toString(),EvalVisitor.data.size()));
					
					//logger.info(String.format("otherDataMap :: %s otherDataMap size :: %d", EvalVisitor.otherData.toString(),EvalVisitor.otherData.size()));
					jobInversionArr = InversionAndRangeCheckFunctions.jobInversion();
					//logger.info(String.format("JobInversion Data :: %s", jobInversionArr));
					jobInversionObjNode.putArray(Constants.DATA).addAll(jobInversionArr);
					
					arr.add(jobInversionObjNode);
					
					JsonNode objectNode = mapper.convertValue(arr, JsonNode.class);
					//logger.info(String.format("JobInversion object :: %s", objectNode));
					return objectNode;
				}

				for (JsonNode node : columnNodeList) {
					isEmtpyRecord=false;
					if (node.get(Constants.CODE) != null)
						code = node.get(Constants.CODE).asText();
					if (node.get(Constants.SKIP) != null)
						skip = node.get(Constants.SKIP).asText();
					if (node.get(Constants.MAPPED_COLUMN_NAME) != null)
						mappedColumnName = node.get(Constants.MAPPED_COLUMN_NAME).asText();
					
					if ( dataNode != null || (dataNode!=null && dataNode.get(mappedColumnName) != null)) {
						
						//Following code written for Mercer JIRA Story PE-5691
						// If data node code value is not present then set to Blank and Data map also.
							if(skip==null  && code !=null && null == dataNode.get(code) ) {
								((ObjectNode)dataNode).put(code, JsonNodeFactory.instance.nullNode()) ;  
								isEmtpyRecord = true;
								// Modified to prepare datamap correctly in case of Dimensions in datanode
								it = dataNode.fields();
								while (it.hasNext()) {
									entry = it.next();
									if (entry.getValue().getNodeType() != JsonNodeType.OBJECT) {
										dataMap.put(entry.getKey().trim(), entry.getValue());
									} else {
										dataMap = parseJsonObjectMap(entry.getKey(), entry.getValue(), dataMap, null);
									}
								}
								//logger.info(String.format("dataMap is %s", dataMap));
							}
						//PE-5691 End.	
						flag = false;
						if ((mappedColumnName != null && dataNode.get(mappedColumnName) != null)
								&& (null != code && mappedColumnName != null && !code.equals(mappedColumnName))) {
							((ObjectNode) (dataNode)).set(code, dataMap.get(mappedColumnName));
							((ObjectNode) (dataNode)).remove(mappedColumnName);
						}
						if (skip != null && (skip.equals("true") || Boolean.parseBoolean(skip) == true)) {
							if (code != null && !code.equals(""))
								((ObjectNode) (dataNode)).remove(code);
							else
								((ObjectNode) (dataNode)).remove(mappedColumnName);
							skip = null;
							continue;
						}
						// Validate the data type object block starts here.
						if (node.get(Constants.DATA_TYPE) != null) {
							logger.info("Check if  dataType is not empty");
							nodeDTObject = node.get(Constants.DATA_TYPE);

							if (dataNode.get(code) != null)
								dnCodeObject = dataNode.get(code);
							else if (dataNode.get(mappedColumnName) != null)
								dnCodeObject = dataNode.get(mappedColumnName);
							validationObjNode = node.get(Constants.VALIDATIONS);

				          // If data node is not empty then do data type validation
							if (!isAggregateRequest && dnCodeObject != null
									&& !StringUtils.isEmpty(dnCodeObject.asText())) {
								boolean isNullNode = NullNode.class.equals(dnCodeObject.getClass());
								dnObjetText = dnCodeObject.asText();
								// Check for integer,boolean,double ,date and string data type object.
								if ( (DataUtility.isNumeric(dnObjetText) || StringUtils.isEmpty(dnObjetText) || isNullNode)
										&& "int".equalsIgnoreCase(nodeDTObject.asText())) {
									flag = true;
								} else if (("true".equalsIgnoreCase(dnObjetText) || "false".equalsIgnoreCase(dnObjetText) || isNullNode 
										|| StringUtils.isEmpty(dnObjetText)) && "boolean".equalsIgnoreCase(nodeDTObject.asText())) {
									flag = true;
								} else if ( (DataUtility.isValidDate(dnObjetText) ||  StringUtils.isEmpty(dnObjetText)  || isNullNode) 
										&& "date".equalsIgnoreCase(nodeDTObject.asText())) {
									flag = true;
								} else if ((DataUtility.isDecimal(dnObjetText) || StringUtils.isEmpty(dnObjetText) || isNullNode)  
										&&  "double".equalsIgnoreCase(nodeDTObject.asText())) {
									flag = true;
								} else if (StringUtils.isEmpty(dnObjetText) || isNullNode || "string".equalsIgnoreCase(nodeDTObject.asText())) {
									flag = true;
								}
							} else {
								flag = true;
							}
						} else { // if data node is empty then skip data type validations.
							flag = true;
						}
						//logger.info(String.format("Flag value is  %s ", flag));
						
						//PE -5216 and PE-5643 Check for drop down options exist in metadata or in database.
						// PE-5643 : Required this functionality at /uploadMulttipleFile also. (Mentioned by Alex)
						if (node.get("questionType") != null
								&& ( node.get("questionType").asText().equals("dropdown") || 
										node.get("questionType").asText().equals("radiobutton") )) {
							//logger.info(String.format("Question type is..%s", node.get("questionType").asText()));
							isOptionType =  FormValidator_CSC.checkAnswerExists(node, dataNode, code); 

							if ( ! isOptionType ) {
								obj = apiExpressionEvaluator.prepareOutputJson(null, null, null, ValidationTypes.dropDown,
									code, null, null,JsonUtils.isNullOrBlankOrNullNode(node.get("displayLabel"))? "":node.get("displayLabel").asText() );
								
							}

							if (obj != null) {
								arr.add(obj);
								obj = null;
							}
						} 
							

 						if (node.get(Constants.VALIDATIONS) != null && node.get(Constants.VALIDATIONS).size() > 0
								&& flag) { // Aggregate
							for (int i = 0; i < node.get(Constants.VALIDATIONS).size(); i++) {

								validationNode = node.get(Constants.VALIDATIONS).get(i);
								if (validationNode.get(Constants.VALIDATION_TYPE) != null
										&& EnumUtils.isValidEnum(ValidationTypes.class,
												validationNode.get(Constants.VALIDATION_TYPE).asText())) {

									switch (ValidationTypes
											.valueOf(validationNode.get(Constants.VALIDATION_TYPE).asText())) {
									case required:
										obj = apiExpressionEvaluator.checkMandatoryFields(dataMap, code,
												node.get(Constants.DATA_TYPE).asText(), mappedColumnName);
										break;
									case oneOf:
										obj = apiExpressionEvaluator.checkValidationTypeOneOf(dataMap, code, node,
												validationNode, mappedColumnName);
										break;
									case range:
										rangeObjList = apiExpressionEvaluator.prepareRangeObject(dataMap, validationNode, node,
												dataNode);
										break;
									case rangeValidationRefTable:
										rangeObjList = apiExpressionEvaluator.prepareRefernceRangeObject(dataMap, validationNode,
												node, dataNode);
										break;
									case eligibility:
										// Last Parameter is weather request of  MDA
										arr.addAll(apiExpressionEvaluator.checkValidity(dataMap, validationNode, node,
												dataNode,false));
										break;
									case phone:
									case email:
									case expression:
										obj = apiExpressionEvaluator.expressionEvaluator(dataMap, validationNode, node,
												dataNode,isAggregateRequest, columnMappingMap,columnNodeList);
										//logger.info(String.format("obj..%s", obj));
										break;
									default:
									}
								}else {
									result = Constants.MANDATORY_VALIDATIONTYPE;
									obj = apiExpressionEvaluator.prepareOutputJson(null, null, result, null, null, null,
											null, null);
								}
								if (obj != null) { 
									arr.add(obj);
									obj = null;
								}
								if(rangeObjList!=null)
								{
									for(int j=0;j<rangeObjList.size();j++)
									{
										arr.add(rangeObjList.get(j));
									}
									rangeObjList=null;
								}
							}
						} else if (!isAggregateRequest && flag == false) {
							logger.info("Check if flag is false");
							result = Constants.INVALID_DATATYPE_FOR_ERROR;
							if (null != nodeDTObject && null != dnCodeObject) {
								obj = apiExpressionEvaluator.prepareOutputJson(validationObjNode, node, result,
										ValidationTypes.dataTypeError, dnCodeObject.asText(), nodeDTObject.asText(),
										nodeDTObject.asText(), null);
								//logger.info(String.format("Value of object is..%s", obj));
							}

							if (obj != null) {
								arr.add(obj);
								obj = null;
							}

						}
 						
 						// PE-6874 : Fixed by shifting this code down here
						//Following code written for Mercer JIRA Story PE-5691
						// If data node code value is not present then set to Blank and Data map also.
						// PE-7071 : Remove CODE if there is no validation OR validation_errorType != "AUTOCORRECT"
						if(isEmtpyRecord && 
								(validationNode==null || 	
								(null!=validationNode && !JsonUtils.isNullOrBlankOrNullNode(validationNode.get(Constants.ERROR_TYPE)) 
									&& !(validationNode.get(Constants.ERROR_TYPE).asText()).equalsIgnoreCase("AUTOCORRECT"))) ) {
							((ObjectNode)dataNode).remove(code); 
						}
						//PE-5691 End.	

						// PE : 2449 to create auto-correct object for missing EEID
 						
						if ((code!=null &&code.equalsIgnoreCase(Constants.EMPLOYEE_EEID)) || 
								(mappedColumnName !=null && mappedColumnName.equalsIgnoreCase(Constants.EMPLOYEE_EEID)))
							obj = prepareEeidAutoCorrectObject(code, dataMap, dataNode,columnMappingMap);

						if (obj != null) {
							arr.add(obj);
							obj = null;
						}

						if (node.get("questionType") != null
								&& node.get("questionType").asText().equals("checkboxes")) {

							buildCheckboxArray(dataNode,code, mappedColumnName);
						}
						
					}
					code = null;
				}

				if ((arr != null && arr.size() > 0)) {
					if (!isAggregateRequest) {
						/*
						 * if(dataNode.get(k).get(code)!=null) ((ObjectNode)
						 * (dataNode.get(k))).set(code, dataNode.get(k).get(code));
						 */
						((ObjectNode) (dataNode)).set(Constants.VALIDATION_ERRORS, arr);
						arr = null;
					} else { // Aggregate
						JsonNode objectNode = mapper.convertValue(arr, JsonNode.class);
						return objectNode;
					}
				}

			}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_VALIDATE));
		return dataNode;
	}

	public ArrayNode findAndMerge(JsonNode dataNode, ArrayNode csvNode, String uniqueIdColumnCode) throws Exception {

		Iterator<String> it = null;
		String key = "";
		boolean isMerged = false;
		if (dataNode != null && dataNode.size() > 0) {
			for (int i = 0; i < dataNode.size(); i++) {
				isMerged = false;
				if (dataNode.get(i).get(uniqueIdColumnCode) != null)
				{
					for (int j = 0; j < csvNode.size(); j++) {
						if (csvNode.get(j).get(uniqueIdColumnCode) != null && csvNode.get(j).get(uniqueIdColumnCode)
								.equals(dataNode.get(i).get(uniqueIdColumnCode))) {
							it = dataNode.get(i).fieldNames();
							while (it != null && it.hasNext()) {
								key = it.next();
								if (csvNode.get(j).get(key) == null || csvNode.get(j).get(key).equals("")) {
									((ObjectNode) csvNode.get(j)).set(key, dataNode.get(i).get(key));
								}
							}
							isMerged = true;
						}
					}
					if (!isMerged) {
						csvNode.add(dataNode.get(i));
					}
				}
			}
		}
		return csvNode;
	}

	public void buildCheckboxArray(JsonNode dataNode,String code, String mappedColumnName) {
		if (code == null || code.equals("")) {
			code = mappedColumnName;
		}

		if (dataNode.get(code) != null)

		{
			if (dataNode.get(code).getNodeType() != JsonNodeType.OBJECT
					&& dataNode.get(code).getNodeType() != JsonNodeType.ARRAY) {
				ArrayNode arrNode = mapper.createArrayNode();
				String arr1[] = dataNode.get(code).asText().split(",");
				if (arr1 != null && arr1.length > 0) {
					for (int i = 0; i < arr1.length; i++) {
						arrNode.add(arr1[i]);
					}
				}
				((ObjectNode) (dataNode)).set(code, arrNode);
			} else {
				Iterator<Map.Entry<String, JsonNode>> it = dataNode.get(code).fields();
				Map.Entry<String, JsonNode> entry = null;
				while (it != null && it.hasNext()) {
					entry = it.next();
					ArrayNode arrNode = mapper.createArrayNode();
					String arr1[] = entry.getValue().asText().split(",");
					if (arr1 != null && arr1.length > 0) {
						for (int i = 0; i < arr1.length; i++) {
							arrNode.add(arr1[i]);
						}
					}
					((ObjectNode) (dataNode.get(code))).set(entry.getKey(), arrNode);
				}
			}
		}
	}

	/**
	 * 
	 * @param header
	 * @param entitiesJson
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, Object> compareFields(List<String> header, String entitiesJson,
			HashMap<String, Object> xlsxMap) throws Exception {
		ArrayNode entitiesNode = mapper.readValue(entitiesJson, ArrayNode.class);
		ObjectNode entityNode = null;
		JsonNode columnNode = null;
		ArrayNode arr = null;
		int columnNodeSize = 0;
		boolean found = false;
		ObjectNode resultNode = null;
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		ArrayNode resultEntities = mapper.createArrayNode();
		String entityCode = null;
		JsonNode skip = null;
		String mappedColName = null;
		boolean sheetMatchingFlag = true;
		String sheetName = "";

		if (entitiesNode != null && xlsxMap != null && xlsxMap.get("xlsxHeaderMap") != null) {
			if (entitiesNode.size() < ((HashMap<String, List<String>>) xlsxMap.get("xlsxHeaderMap")).size()) {
				sheetMatchingFlag = false;
			} else {
				for (int i = 0; i < entitiesNode.size(); i++) {
					sheetName = entitiesNode.get(i).get(Constants.CONTEXTDATAKEY).get("companyName").asText() + "_"
							+ entitiesNode.get(i).get(Constants.CONTEXTDATAKEY).get("ctryCode").asText();
					if (((HashMap<String, List<String>>) xlsxMap.get("xlsxHeaderMap")).get(sheetName) == null) {
						sheetMatchingFlag = false;
					}
					break;
				}

			}
			//logger.info(String.format("nodetype...%s", entitiesNode.size()));
		}

		for (int i = 0; i < entitiesNode.size(); i++) {

			ObjectNode dataNode = mapper.createObjectNode();
			columnNode = entitiesNode.get(i).get(Constants.SECTION_STRUCTURE).get(Constants.COLUMNS);
			if (entitiesNode.get(i).get(Constants.CONTEXTDATAKEY).get("companyName") != null
					&& entitiesNode.get(i).get(Constants.CONTEXTDATAKEY).get("ctryCode") != null) {
				sheetName = entitiesNode.get(i).get(Constants.CONTEXTDATAKEY).get("companyName").asText() + "_"
						+ entitiesNode.get(i).get(Constants.CONTEXTDATAKEY).get("ctryCode").asText();
			}
			columnNodeSize = (columnNode).size();
			if (xlsxMap != null && xlsxMap.get("xlsxHeaderMap") != null) {
				if (!sheetMatchingFlag) {
					header = ((HashMap<String, List<String>>) xlsxMap.get("xlsxHeaderMap"))
							.get(xlsxMap.get("FirstSheetName"));
				} else {
					header = ((HashMap<String, List<String>>) xlsxMap.get("xlsxHeaderMap")).get(sheetName);
				}
			}
			resultNode = mapper.createObjectNode();
			if (header != null) {
				for (int j = 0; j < header.size(); j++) {
					found = false;
					for (int k = 0; k < columnNodeSize; k++) {
						skip = columnNode.get(k).get(Constants.SKIP);
						if (null != columnNode.get(k).get("code")) {
							entityCode = columnNode.get(k).get("code").asText();
						}
						if (null != columnNode.get(k).get("mappedColumnName")) {
							mappedColName = columnNode.get(k).get("mappedColumnName").asText();
						}
						//Code modified to skip mapping in case of skip=true in request PE : 6858
						if(skip!=null && skip.asText().equals("true"))
						{
							if(header.get(j).equals(entityCode) || header.get(j).equals(mappedColName))
							{	
							found = true;
							break;
							}
							else
							{
							continue;
							}
							
						}
						if ((!StringUtils.isEmpty(entityCode)|| !StringUtils.isEmpty(mappedColName)) && skip==null) {
							if (header.get(j).equals(entityCode) || header.get(j).equals(mappedColName)) {
								found = true;
								break;
							}
						}
					}
					if (!found) {
						break;
					}
				}
				if (!found) {
					for (int j = 0; j < header.size(); j++) {
						dataNode.put("" + (j + 1), header.get(j));
					}
					if (arr == null)
						arr = mapper.createArrayNode();
					arr.add(dataNode);
					entityNode = mapper.createObjectNode();
					entityNode.set("data", arr);
					arr = null;
					if (entitiesNode.get(i).get(Constants.CONTEXTDATAKEY) != null)
						entityNode.set("contextData", entitiesNode.get(i).get(Constants.CONTEXTDATAKEY));
					resultEntities.add(entityNode);
				} else {
					resultMap.put("resultNode", null);
					if (sheetMatchingFlag == false) {
						resultMap.put("sheetMatchingFlag",
								null != xlsxMap.get("FirstSheetName") ? xlsxMap.get("FirstSheetName") : "");
					}
					return resultMap;
				}
			}

		}
		resultNode.set("entities", resultEntities);
		resultMap.put("resultNode", resultNode);
		if (sheetMatchingFlag == false) {
			resultMap.put("sheetMatchingFlag",
					null != xlsxMap.get("FirstSheetName") ? xlsxMap.get("FirstSheetName") : "");
		}
		return resultMap;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, Object> convertXlsxToMap(Map<String, Object> workBookMap,String empIdColumn) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_CONVERXLSXTOMAP));
		HashMap<String, Object> map = null;
		List<String> headerList = null;
		List<ArrayList<String>> dataList = null;
		HashMap<String, List<HashMap<String, String>>> xlsxDataMap = null;
		HashMap<String, List<String>> xlsxHeaderMap = null;
		HashMap<String, Object> xlsxMap = null;
		xlsxDataMap = new HashMap<String, List<HashMap<String, String>>>();
		xlsxHeaderMap = new HashMap<String, List<String>>();
		HashSet eeidSet = null;
		boolean eeidFlag = true;
		boolean eeidColFlag = false;
		if (workBookMap != null && workBookMap.size() > 0) {
			xlsxMap = new HashMap<String, Object>();
			for (Entry<String, Object> entry : workBookMap.entrySet()) {
				eeidSet = new HashSet();
				if (entry.getKey().equals("FirstSheetName")) {
					xlsxMap.put("FirstSheetName", entry.getValue());
					continue;
				}
				List<HashMap<String, String>> allRecords = new ArrayList<HashMap<String, String>>();
				map = (HashMap<String, Object>) entry.getValue();
				headerList = (List<String>) map.get("header");
				dataList = (List<ArrayList<String>>) map.get("data");
				for (int i = 0; i < dataList.size(); i++) {

					HashMap<String, String> record = new HashMap<String, String>();
					for (int j = 0; j < headerList.size(); j++) {
						if (headerList.get(j).equalsIgnoreCase(Constants.EMPLOYEE_EEID) || (empIdColumn!=null 
								&& !empIdColumn.equals("") && headerList.get(j).equals(empIdColumn))) {
							eeidColFlag = true;
							try {
								if (dataList.get(i).get(j) != null && !dataList.get(i).get(j).equals(""))
									eeidFlag = eeidSet.add(dataList.get(i).get(j));
							} catch (IndexOutOfBoundsException e) {
								eeidFlag = true;
							}
						}
						if (eeidFlag) {
							try {
								record.put(headerList.get(j),
										(!StringUtils.isEmpty(dataList.get(i).get(j)) ? dataList.get(i).get(j)
												: Constants.BLANK));
							} catch (IndexOutOfBoundsException e) {
								record.put(headerList.get(j), Constants.BLANK);
							}
						} else {
							//logger.info(String.format("Duplicate value...%s", dataList.get(i).get(j)));
							xlsxMap.put("isEeidDuplicate", "true");
							eeidSet = null;
							return xlsxMap;
						}
					}
					/*if (!eeidColFlag) {
						xlsxMap.put("isEeidDuplicate", "false");
						headerList.add(Constants.EMPLOYEE_EEID);
						eeidSet = null;
						//return xlsxMap;
					}*/
					allRecords.add(record);
				}
				xlsxDataMap.put(entry.getKey(), allRecords);
				xlsxHeaderMap.put(entry.getKey(), headerList);
				xlsxMap.put("xlsxHeaderMap", xlsxHeaderMap);
				xlsxMap.put("xlsxDataMap", xlsxDataMap);
			}

		}
		eeidSet = null;
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_CONVERXLSXTOMAP));
		return xlsxMap;
	}

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String, String> prepareJsonString(HashMap<String, Object> xslsMap) throws Exception{

		HashMap<String, Object> map = new HashMap<String, Object>();
		HashMap<String, List<HashMap<String, String>>> xlsxDataMap = null;
		String csvJsonString = "";
		LinkedHashMap<String, String> xslJsonMap = new LinkedHashMap<String, String>();
		if (xslsMap != null && xslsMap.size() > 0) {
			xlsxDataMap = (HashMap<String, List<HashMap<String, String>>>) xslsMap.get("xlsxDataMap");
			for (Entry<String, List<HashMap<String, String>>> entry : xlsxDataMap.entrySet()) {
				map.put("data", entry.getValue());
				ObjectMapper mapper = new ObjectMapper();
					csvJsonString = mapper.writeValueAsString(map);
					xslJsonMap.put(entry.getKey(), csvJsonString);

			}
		}
		return xslJsonMap;
	}

	// PE : 2449 to check duplication of EEID start in case of entity data in
	// request body
	public HashMap<String,String> checkUniqueNessForEEID(String enetityJson) throws Exception{
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_CHECKUNIQUENESSFOREEID));
		@SuppressWarnings("unchecked")
		HashMap<String,String> empIdMap= new HashMap<String,String>();
		empIdMap.put("isUnique", "true");
		JsonNode entityNode = null;
		boolean flag = false;
		String code = "";
		JsonNode sectionStructureNode = null;
		String empIdColumn="";
		ArrayList<JsonNode> columnNodeList = null;
			columnNodeList = new ArrayList<JsonNode>();
			JsonNode entittiesNode = mapper.readValue(enetityJson, JsonNode.class);
			HashSet<String> eeidSet = null;
			if (entittiesNode != null) {
				for (int i = 0; i < entittiesNode.size(); i++) {
					eeidSet = new HashSet<String>();
					entityNode = entittiesNode.get(i);
					sectionStructureNode = entityNode.get(Constants.SECTION_STRUCTURE);
					
					for (int j = 0; j < sectionStructureNode.get(Constants.COLUMNS).size(); j++) {
						columnNodeList.add(sectionStructureNode.get(Constants.COLUMNS).get(j));

					}
					if (columnNodeList != null && columnNodeList.size() > 0) {

						for (JsonNode node : columnNodeList) {
							if (node.get(Constants.CODE) != null)
								code = node.get(Constants.CODE).asText();
							if (code.equals(Constants.EMPLOYEE_EEID))
							{
								flag = true;
								if(node.get(Constants.MAPPED_COLUMN_NAME)!=null && !node.get(Constants.MAPPED_COLUMN_NAME).equals(""))
								{
									empIdColumn = node.get(Constants.MAPPED_COLUMN_NAME).asText();
									empIdMap.put("empIdColumn", empIdColumn);
								}
							}
								
						}
					}

					if (flag) {
						JsonNode dataNode = entittiesNode.get(i).get(Constants.DATA);
						if (dataNode != null) {
							for (int k = 0; k < dataNode.size(); k++) {

								if (dataNode.get(k).get(Constants.EMPLOYEE_EEID) != null
										&& !dataNode.get(k).get(Constants.EMPLOYEE_EEID).asText().equals("")) {
									String eeId = dataNode.get(k).get(Constants.EMPLOYEE_EEID).asText();
									if (!eeidSet.add(eeId)) {
										eeidSet = null;  
										empIdMap.put("isUnique",Constants.DUPLICATE);
										return empIdMap;
									}
								} else {
									 empIdMap.put("isUnique",Constants.MISSING);
									 return empIdMap;
								}
							}
						}
					}
				}
			}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_CHECKUNIQUENESSFOREEID));
		return empIdMap;

	}

	// PE : 2449 to prepare auto-correct validation object in case of missing EEID..
	public ObjectNode prepareEeidAutoCorrectObject(String code, HashMap<String, JsonNode> dataMap, JsonNode dataNode,
			 HashMap<String, String> columnMappingMap) throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_PREPAREEEIDAUTOCORRECTOBJECT));
		ObjectNode obj = null;
		if (dataMap != null && ( !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.EMPLOYEE_EEID))   
				|| !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(columnMappingMap.get(code)))  )) {
			return obj;
		} else {
			String eeId = generateUniqueEeid();
			//logger.info(String.format("eeid generated..%s", eeId));
			APIExpressionEvaluator apiExpressionEvaluator = new APIExpressionEvaluator();
				obj = apiExpressionEvaluator.prepareOutputJson(null, null, eeId, ValidationTypes.eeIdAutoCorrect, code,
						null, null, null);
				((ObjectNode) (dataNode)).put(code, eeId);
		}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_PREPAREEEIDAUTOCORRECTOBJECT));
		return obj;
	}

	public static synchronized String generateUniqueEeid() throws Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_GENERATEUNIQUEEID));
		Date d1 = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.EEID_FORMAT);
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_GENERATEUNIQUEEID));
		return "" + sdf.format(d1);
		

	}

	
	
	public static ArrayNode removeMlNode(JsonNode jsonNode) throws Exception {

		
		 final List<JsonNode> _children;
		 
		JsonNode mlNode = null;
		JsonNode entityNode = null;
		Iterator<Entry<String, JsonNode>> mlItr = null;
		ArrayNode mlJsonDataArray = (ArrayNode) jsonNode.get("entities");
		Map.Entry<String, JsonNode> entry = null;

		for (int i = 0; i < mlJsonDataArray.size(); i++) {

			entityNode = mlJsonDataArray.get(i);
			mlNode = entityNode.get(Constants.DATA);

			for (int k = 0; k < mlNode.size(); k++) {

				mlItr = mlNode.get(k).fields();
				while ( mlItr.hasNext() ) {
					entry = mlItr.next();
					if (entry.getKey().contains("_Replace")  || entry.getKey().contains("_ACflag")) {
				       ((ObjectNode) mlNode.get(k)).remove(entry.getKey());
					}
				}

			}
		}
		
		return mlJsonDataArray;
	}
	public String performL0Validation(String entitiesJson, String csvJsonString, boolean isAggregateRequest,
			HashMap<String, String> xslJsonMap, String FirstSheetName) throws Exception
	{
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_PERFORML0VALIDATION));
			JsonNode sectionStructureNode = null;
			JsonNode contextDataNode = null;
			JsonNode csvNode = null;
			JsonNode entityNode = null;
			long startTime=0;
			long stopTime=0;
			Set<String> xslJsonSheets = null;
			String code = "";
			JsonNode validationNode = null;
			String returnValue="";
			Pattern pattern = Pattern.compile(Constants.EXP_PATTERN);
			boolean columnMissflag = true;
			ArrayNode entitiesNode = mapper.readValue(entitiesJson, ArrayNode.class);
			if (xslJsonMap != null) {
				xslJsonSheets = new HashSet<String>(xslJsonMap.keySet());
			}
			for (int i = 0; i < entitiesNode.size(); i++) {
				entityNode = entitiesNode.get(i);
				if(entitiesNode.get(i).get(Constants.DATA)!=null && entitiesNode.get(i).get(Constants.DATA).size()>0)
				{
				break;	
				}
				sectionStructureNode = entityNode.get(Constants.SECTION_STRUCTURE);
				contextDataNode = entityNode.get(Constants.CONTEXTDATAKEY);
				if (csvJsonString != null && !csvJsonString.equals("")) {
					csvNode = mapper.readValue(csvJsonString, JsonNode.class);
				} else if (xslJsonMap != null && xslJsonMap.size() > 0) {
					String coName_CtryCode = contextDataNode.get("companyName").asText() + "_"
							+ contextDataNode.get("ctryCode").asText();
					if (FirstSheetName != null) {
						csvNode = mapper.readValue(xslJsonMap.get(FirstSheetName), JsonNode.class);
					} else if (xslJsonMap.get(coName_CtryCode) != null) {
						if (xslJsonSheets.contains(coName_CtryCode)) {
							csvNode = mapper.readValue(xslJsonMap.get(coName_CtryCode), JsonNode.class);
						}
					} else if (xslJsonMap.get(coName_CtryCode) == null) {
						/*throw new CustomStatusException(Constants.HTTPSTATUS_400,
								String.format(Constants.ERROR_SHEET_NOT_FOUND, coName_CtryCode));*/
						paramValueList =  new ArrayList<String>();
						paramValueList.add(""+coName_CtryCode);
						throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_035),
								Cache.getPropertyFromError(ErrorCacheConstant.ERR_035+Constants.PARAMS),paramValueList);
					}
				}
				if (csvNode != null) {
					sectionStructureNode = entityNode.get(Constants.SECTION_STRUCTURE);
					ArrayList<JsonNode> columnNodeList = new ArrayList<JsonNode>();
					for (int j = 0; j < sectionStructureNode.get(Constants.COLUMNS).size(); j++) {
						columnNodeList.add(sectionStructureNode.get(Constants.COLUMNS).get(j));
						if (columnNodeList != null && columnNodeList.size() > 0) {

							for (JsonNode node : columnNodeList) {
								if (node.get(Constants.CODE) != null)
									code = node.get(Constants.CODE).asText();
								if (node.get(Constants.VALIDATIONS) != null
										&& node.get(Constants.VALIDATIONS).size() > 0) {
									for (int k = 0; k < node.get(Constants.VALIDATIONS).size(); k++) {
										validationNode = node.get(Constants.VALIDATIONS).get(k);
										columnMissflag = true;
										if (validationNode != null && validationNode.get("category") != null
												&& validationNode.get("category").asText().equalsIgnoreCase("file_acceptance")) {
											
											if (validationNode.get(Constants.EXPRESSION) != null && validationNode
													.get(Constants.EXPRESSION).asText().contains("ISBLANK")) {
												Matcher matcher = pattern.matcher(validationNode.get(Constants.EXPRESSION).asText()
																.replaceAll(Constants.CURLY_BRACES, ""));
												List<String> fields = new ArrayList<String>();
												while (matcher.find()) {
													fields.add(matcher.group().replace("this.", "").trim());
												}
												for (int fieldSize = 0; fieldSize < fields.size(); fieldSize++) {
													if (csvNode.get(Constants.DATA).get(0).get(fields.get(fieldSize)) != null) {
														columnMissflag = false;
														break;
													}
												}
												if (!columnMissflag) {
													 startTime = System.currentTimeMillis();
													columnMissflag = checkforBlank(fields, csvNode);
													 stopTime = System.currentTimeMillis();
													 //logger.info(String.format("Time taken for checkforBlank() in FormValidator..%d", (stopTime-startTime)));
												}
												if(columnMissflag)
												{
												return validationNode.get(Constants.MESSAGE).asText();
												}
											}

										}

									}
								}
							}
						}

					}
				}
			}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_PERFORML0VALIDATION));
		return returnValue;
	}
	public boolean checkforBlank(List<String> fields,JsonNode csvNode)throws Exception
	
	{
		boolean returnValue = true;
		JsonNode dataNode = csvNode.get(Constants.DATA);
		for (String field : fields) {
			if (returnValue == false)
				break;
				for (int i = 0; i < dataNode.size(); i++) {
					if (dataNode.get(i)!=null && dataNode.get(i).get(field)!=null && dataNode.get(i).get(field).asText() != null
							&& !dataNode.get(i).get(field).asText().trim().equals("")) {
						returnValue = false;
						break;
					}
				}
		}
		return returnValue;
	}
	
}
