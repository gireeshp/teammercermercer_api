package in.lnt.udf;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.serde2.objectinspector.ListObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;

public class DateDifference extends UDF {
	
	 public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
	    if (arguments.length != 3) {
	      throw new UDFArgumentLengthException("arrayContainsExample only takes 2 arguments: List<T>, T");
	    }
	    // 1. Check we received the right object types.
	    ObjectInspector date2 = arguments[0];
	    ObjectInspector date1 = arguments[1];
	    ObjectInspector units = arguments[2];
	    
	    if (!(date2 instanceof StringObjectInspector) || !(date1 instanceof StringObjectInspector)) {
	      throw new UDFArgumentException("first argument must be a list / array, second argument must be a string");
	    } 
	    // the return type of our function is a boolean, so we provide the correct object inspector
	    return PrimitiveObjectInspectorFactory.javaBooleanObjectInspector;
	  }
	
	public static String evaluate(String date2,String date1, String units){
		return null; 
	}
	
}
