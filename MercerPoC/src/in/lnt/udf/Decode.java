package in.lnt.udf;
import org.apache.hadoop.hive.ql.exec.UDF;

public class Decode extends UDF {
	public static String evaluate(Boolean bool, String str1, String str2) {
		if (bool)
			return str1;
		else
			return str2;
	}
}