package in.lnt.connector;



import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.Cache;
 
import java.util.HashMap;
import java.util.Map;

public class MongoConnector {
	public static DB db;
	public static DBCollection seqCollection;
	// Collection Name "mercer_poc"

	public static Mongo m;

	private static Map<String, DBCollection> collections = new HashMap<String, DBCollection>();

	public static DB getMongoDBConnection() {
		if (db == null) {
			try {
				connectToDB();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return db;
	}

	public static boolean connectToDB() throws Exception {
		System.out.println("connect");
		String serverIp = Cache.getProperty(CacheConstats.MONGO_IP);
		int port = Integer.parseInt(Cache.getProperty(CacheConstats.MONGO_PORT));
		String dbName = "maxiqdemo";
		if (m == null)
			m = new Mongo(serverIp, port);
		db = m.getDB(dbName);

		return true;
	}
}
