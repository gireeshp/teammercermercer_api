package in.lnt.connector;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitMQConnector {

	private static final Logger logger = LoggerFactory.getLogger(RabbitMQConnector.class);

	private static String host;
	private static Integer port;
	private static String userId;
	private static String password;
	private static String vhost;
 
	public RabbitMQConnector(String host, Integer port, String userId, String password, String vhost) {
		// TODO Auto-generated constructor stub
		this.host = host;
		this.port = port;
		this.userId = userId;
		this.password = password;
		this.vhost = vhost;
		
		logger.info(host);
	}

	 
	public Connection createConnecction() {
		if ((host != null) || (userId != null) || (password != null)) {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(host);
			factory.setPort(port);
			factory.setUsername(userId);
			factory.setPassword(password);
			factory.setVirtualHost(vhost);
			try {
				Connection connection = factory.newConnection();
				return connection;
			} catch (IOException | TimeoutException e) {
				logger.error(e.getMessage() + " Cause is" + e.getCause());
			}
		}
		return null;
	}
}
