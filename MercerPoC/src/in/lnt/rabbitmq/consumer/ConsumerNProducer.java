package in.lnt.rabbitmq.consumer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.EnumUtils;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import in.lnt.connector.RabbitMQConnector;
import in.lnt.enums.QueueListConsumer;

public class ConsumerNProducer implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(ConsumerNProducer.class);
	
	static Channel consumerChannel = null;
	static Channel producerChannel = null;
	static Map<String, ArrayList<String>> queue_exchange_binding_list = null;

	void consumerProducer() {
		RMQChannelSetup channels = new RMQChannelSetup();
		channels.createConsumerAndProducerConnection();
		channels.setting_Queue_Exchange_RoutingKey();

		consumerChannel = channels.getConsumerChannel();
		producerChannel = channels.getProducerChannel();
		queue_exchange_binding_list = channels.getQueue_exchange_binding_list();
	}

	@Override
	public void run() {
		logger.info("==========================================================================");
		final Consumer consumer = new DefaultConsumer(producerChannel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				logger.info("------------");
				Date date = new Date(); // date object
				logger.info(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date.getTime()));
//				logger.info(properties);
				logger.info(consumerTag);
//				logger.info(envelope);
				ArrayList<String> exchange_binding = new ArrayList<String>();
				exchange_binding.add(envelope.getExchange());
				exchange_binding.add(envelope.getRoutingKey());
				Set setkeys = queue_exchange_binding_list.keySet();
				Iterator iter = setkeys.iterator();
				while (iter.hasNext()) {
					String key = (String) iter.next();
					if (queue_exchange_binding_list.get(key).equals(exchange_binding)) {
						logger.info(key);
						// System.out.println(message);
						key = key.replace("PRODUCER_", "");
						key = key.replace("CONSUMER_", "");
						logger.info(key);
						if (EnumUtils.isValidEnum(QueueListConsumer.class, key)
								|| EnumUtils.isValidEnum(QueueListConsumer.class, key)) {

							switch (QueueListConsumer.valueOf(key)) {
							case data_model_creation:
								toJSON(new String(body, StandardCharsets.UTF_8));
								break;
							case validations:
								
								break;
							}
						} else {
							logger.info("Queue name not matching.");
						}
						break;
					}

				}
			}
		};
		boolean autoAck = true; // acknowledgment is covered below
		try {
			producerChannel.basicConsume("validations", autoAck, consumer);
			producerChannel.basicConsume("data_model_creation", autoAck, consumer);
		} catch (IOException e) {
			logger.error(e.getMessage() + " Cause is" + e.getCause());
		}
	}

	private void toJSON(final String message) {
		Thread jsonDumpThread = new Thread(message) {
			public void run() {
				// TODO Auto-generated method stub
				logger.info("message.length() -> " + message.length());
				CsvSchema bootstrap = CsvSchema.emptySchema().withHeader();
				CsvMapper csvMapper = new CsvMapper();
				try {
					MappingIterator<Map<?, ?>> mappingIterator = csvMapper.reader(Map.class).with(bootstrap)
							.readValues(message);

					JSONArray array = new JSONArray();
					ServiceAsThread serviceAsThread = new ServiceAsThread();
					while (mappingIterator.hasNext()) {
						serviceAsThread.data.put(new JSONObject(mappingIterator.next()));
					}
					Thread thread = new Thread(serviceAsThread);
					thread.start();

				} catch (IOException e) {
					logger.error(e.getMessage() + " Cause is" + e.getCause());
				}
			}
		};
		jsonDumpThread.start();
	}

	public static void main(String[] args) {
		Date date = new Date(); // date object
		logger.info(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date.getTime()));
		ConsumerNProducer c = new ConsumerNProducer();
		c.consumerProducer();
		Thread thread = new Thread(c);
		thread.start();
	}
}
