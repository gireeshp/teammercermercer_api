package in.lnt.rabbitmq.consumer;
 
 

import java.util.Date;

import org.joda.time.LocalDateTime;
import org.json.JSONArray;

import in.lnt.mongo.MongoLoader;

public class ServiceAsThread implements Runnable {
	
	//key = true -> JSONDUMP into MONGO for  MODEL DATA
	//key = false -> ACtual DATA Validation
	boolean key;
	JSONArray data = new JSONArray(); 
	
	public void run() {
		MongoLoader mongoLoader = new MongoLoader();
		mongoLoader.dumpDataToMongo(data);
		 Date date = new Date(); // date object
	        System.out.println(date.getTime());  
	}
	
}
