package in.lnt.rabbitmq.consumer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import in.lnt.connector.RabbitMQConnector;
import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.Cache;

public class RMQChannelSetup {
	
	private static final Logger logger = LoggerFactory.getLogger(RMQChannelSetup.class);

	Connection producerConnector = null;
	Connection consumerConnector = null;
	static Channel consumerChannel = null;
	static Channel producerChannel = null;
	static Map<String, ArrayList<String>> queue_exchange_binding_list = new HashMap<String, ArrayList<String>>();

	public static Channel getConsumerChannel() {
		return consumerChannel;
	}

	public static void setConsumerChannel(Channel consumerChannel) {
		RMQChannelSetup.consumerChannel = consumerChannel;
	}

	public Map<String, ArrayList<String>> getQueue_exchange_binding_list() {
		return queue_exchange_binding_list;
	}

	public  void setQueue_exchange_binding_list(Map<String, ArrayList<String>> queue_exchange_binding_list) {
		RMQChannelSetup.queue_exchange_binding_list = queue_exchange_binding_list;
	}

	public static Channel getProducerChannel() {
		return producerChannel;
	}

	public static void setProducerChannel(Channel producerChannel) {
		RMQChannelSetup.producerChannel = producerChannel;
	}

	void createConsumerAndProducerConnection() {

		producerConnector = new RabbitMQConnector(Cache.getProperty(CacheConstats.RABBITMQ_PRODUCER_IP),
				Integer.valueOf(Cache.getProperty(CacheConstats.RABBITMQ_PRODUCER_PORT)),
				Cache.getProperty(CacheConstats.RABBITMQ_PRODUCER_USERID),
				Cache.getProperty(CacheConstats.RABBITMQ_PRODUCER_PASSWORD),
				Cache.getProperty(CacheConstats.RABBITMQ_PRODUCER_VHOST_NAME)).createConnecction();

		consumerConnector = new RabbitMQConnector(Cache.getProperty(CacheConstats.RABBITMQ_CONSUMER_IP),
				Integer.valueOf(Cache.getProperty(CacheConstats.RABBITMQ_CONSUMER_PORT)),
				Cache.getProperty(CacheConstats.RABBITMQ_CONSUMER_USERID),
				Cache.getProperty(CacheConstats.RABBITMQ_CONSUMER_PASSWORD),
				Cache.getProperty(CacheConstats.RABBITMQ_CONSUMER_VHOST_NAME)).createConnecction();
		return;
	}

	void setting_Queue_Exchange_RoutingKey() {
		try {

			producerChannel = producerConnector.createChannel();
			String[] producerQueues = Cache.getProperty(CacheConstats.RABBITMQ_PRODUCER_QUEUE_NAME).split(",");
			String[] producerBindings = Cache.getProperty(CacheConstats.RABBITMQ_PRODUCER_BINDING_KEY).split(",");
			int i = 0;
			for (String queue : producerQueues) {
				ArrayList<String> list_exchange_binding = new ArrayList<String>();
				producerChannel.queueBind(queue, Cache.getProperty(CacheConstats.RABBITMQ_PRODUCER_EXCHANGE_NAME),
						producerBindings[i]);
				list_exchange_binding.add(Cache.getProperty(CacheConstats.RABBITMQ_PRODUCER_EXCHANGE_NAME));
				list_exchange_binding.add(producerBindings[i++]);
				queue_exchange_binding_list.put("PRODUCER_" + queue, list_exchange_binding);
			}

			consumerChannel = consumerConnector.createChannel();
			String[] consumerQueues = Cache.getProperty(CacheConstats.RABBITMQ_CONSUMER_QUEUE_NAME).split(",");
			String[] consumerBindings = Cache.getProperty(CacheConstats.RABBITMQ_CONSUMER_BINDING_KEY).split(",");
			i = 0;
			for (String queue : consumerQueues) {
				ArrayList<String> list_exchange_binding = new ArrayList<String>();
				consumerChannel.queueBind(queue, Cache.getProperty(CacheConstats.RABBITMQ_CONSUMER_EXCHANGE_NAME),
						consumerBindings[i]);
				list_exchange_binding.add(Cache.getProperty(CacheConstats.RABBITMQ_CONSUMER_EXCHANGE_NAME));
				list_exchange_binding.add(consumerBindings[i++]);
				queue_exchange_binding_list.put("CONSUMER_" + queue, list_exchange_binding);
			}
		} catch (IOException e) {
			logger.error(e.getMessage() + " Cause is" + e.getCause());
		}
		return;
	}
}
