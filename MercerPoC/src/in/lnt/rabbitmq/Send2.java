package in.lnt.rabbitmq;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

	public class Send2 {
	
		private final static String QUEUE_NAME = "validations";
	
		public static void main(String[] argv) throws Exception {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("172.20.99.12");
			factory.setPort(5672);
			factory.setVirtualHost("validations");
			factory.setUsername("root");
			factory.setPassword("Newuser123");
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			channel.queueBind(QUEUE_NAME, "validations", "validations");
			channel.queueBind("data_model_creation", "validations", "model_creation");
			// channel.queueDeclare(QUEUE_NAME, true, false, false, null);
			
			
			
			Map<String, Object> headers = new HashMap<>();
			headers.put("key", "abcd"); 
            BasicProperties.Builder propertiesBuilder = new BasicProperties.Builder();
            propertiesBuilder.headers(headers);
            BasicProperties props = propertiesBuilder.build();
            
            
			Path path = Paths.get("D:\\PROJECT\\MERCER\\Mercer Data\\test1.csv");
			String data = new String(Files.readAllBytes(path));
			channel.basicPublish("validations", "validations", props, data.getBytes());
			System.out.println("Sent - " + data.getBytes().length);
			 
		}
}