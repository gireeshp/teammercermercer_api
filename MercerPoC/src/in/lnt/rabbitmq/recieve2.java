package in.lnt.rabbitmq;

import java.io.IOException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class recieve2 {

	private final static String QUEUE_NAME = "validations";
	static Channel channel = null;

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("172.20.99.12");
		factory.setPort(5672);
		factory.setVirtualHost("validations");
		factory.setUsername("root");
		factory.setPassword("Newuser123");
		Connection connection = factory.newConnection();
		channel = connection.createChannel();
		System.out.println(channel.getChannelNumber());
		System.out.println(channel.queueDeclare());
		channel.queueDeclare(QUEUE_NAME, true, false, false, null);
		channel.queueBind(QUEUE_NAME, "validations", "validations");
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		final Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println(" [x] Received '" + message + "'"); 
				System.out.println(envelope.toString());
				try {
					doWork(message);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		boolean autoAck = true; // acknowledgment is covered below
		channel.basicConsume(QUEUE_NAME, autoAck, consumer);
	}

	private static void doWork(String task) throws InterruptedException {
		/*
		 * 
		 * Thread.sleep(5000);
		 * 
		 * try { channel.basicPublish("", "hello", null, "i have nothing in "
		 * .getBytes()); } catch (IOException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); }
		 * 
		 */}
}