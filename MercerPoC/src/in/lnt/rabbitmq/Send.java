package in.lnt.rabbitmq;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Send {

	private final static String QUEUE_NAME = "hello";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("172.20.99.12");
		factory.setUsername("root");
		factory.setPassword("Newuser123");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(QUEUE_NAME, true, false, false, null);
		Path path = Paths.get("D:\\workspace\\MercerPoC\\resources\\config.properties");
		String data = new String(Files.readAllBytes(path));
		String message = "a";
		channel.basicPublish("", QUEUE_NAME, null, data.getBytes("UTF-8"));
		System.out.println(" [x] Sent '" + data + "'");
		/*Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println(" [x] Received '" + message + "'");
			}
		};
		channel.basicConsume(QUEUE_NAME, true, consumer);*/
	}
}