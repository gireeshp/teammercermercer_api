package in.lnt.hiveQueryPerformanceTest;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.hive.HiveContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class Test {
	public static void main(String[] args) {
		// Create SparkContext
		SparkConf conf = new SparkConf().setAppName("MercerPoC").setMaster("local[*]");
		JavaSparkContext spark = new JavaSparkContext(conf);
		SparkContext sc = spark.sc();

		HiveContext hiveContext = new org.apache.spark.sql.hive.HiveContext(sc);
		SQLContext sqlContext = new org.apache.spark.sql.SQLContext(sc);
		DataFrame data = hiveContext.parquetFile(
				"hdfs://172.20.99.11:8020/maxiq/ds/stage2/58585/1781939/OUT/part-r-00000-70953a7c-de5f-4e93-9029-5912e5f17a25.snappy.parquet");
		data.save("MyTestTable",SaveMode.Overwrite);
		System.out.println("f-----");
		DataFrame sql = hiveContext.sql("select count(ASSET_FUEL_TYPE) from MyTestTable");

		//System.out.println(sql.first().toString() + " " + data.count());

	}

	public static void registerDualDataFrameWithDummyData(SQLContext SQLContext, JavaSparkContext spark) {

		List<String> stringAsList = new ArrayList<String>();
		stringAsList.add("TEST");

		JavaRDD<Row> rowRDD = spark.parallelize(stringAsList).map(new Function<String, Row>() {
			@Override
			public Row call(String row) throws Exception {
				return RowFactory.create(row);
			}
		});

		StructType schema = DataTypes.createStructType(
				new StructField[] { DataTypes.createStructField("DUAL", DataTypes.StringType, false) });

		DataFrame df = SQLContext.createDataFrame(rowRDD, schema);
		df.registerTempTable(StringUtils.upperCase("DUAL"));
	}

	public void queries() {
		List<String> queriesAsList = new ArrayList<String>();
		queriesAsList.add("");
	}
}
