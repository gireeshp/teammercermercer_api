package in.lnt.customcomponent;


import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MongoInsert implements Function<Row, String>, Serializable {

    private static final long serialVersionUID = 1L;
    private String[] fieldNames = null;
    private Map<String, String> params_ = new HashMap<String, String>();
    private String collection;


    public MongoInsert(String[] fieldNames, String coll,
                       Map<String, String> params) {
        this.fieldNames = fieldNames;
        this.collection = coll;
        this.params_ = params;
    }

    public String call(Row v1) throws Exception {

        Map<String, String> values = new HashMap<String, String>();
        for (int i = 0; i < fieldNames.length; i++) {
            values.put(fieldNames[i], String.valueOf(v1.get(i)));
            System.out.println(v1.get(i));
        }
        DBObject dbObject = new BasicDBObject(values);
     //   MercerLoader.init(params_);
       // MercerLoader.db.getCollection(collection).insert(dbObject);
        return v1.toString();
    }

}
