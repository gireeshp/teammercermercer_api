package in.lnt.customcomponent;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import in.lnt.validations.ValidationGenerator;

public class Validator implements Function<Row, String>, Serializable {

	Map<String, JSONObject> mapping = null;
	static int i = 0;
	String[] fields = null;
	static ObjectMapper mapper = new ObjectMapper();
	static String metadataString = "";
	static int count = 0;
	static ValidationGenerator generator = new ValidationGenerator(0);

	public Validator(String metadataString) {
		// TODO Auto-generated constructor stub
		this.metadataString = metadataString;
	}

	@Override
	public String call(Row v1) throws Exception {
		// TODO Auto-generated method stub
		fields = v1.schema().fieldNames();

		String data = v1.mkString().replace("~!@#", "\n");
		data = data.substring(1, data.length() - 1); 
		InputStream csvStream = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
		InputStream jsonStream = new ByteArrayInputStream(metadataString.getBytes(StandardCharsets.UTF_8));  
		//generator.ValidationDriver(jsonStream, csvStream);
		return null;
	}

}
