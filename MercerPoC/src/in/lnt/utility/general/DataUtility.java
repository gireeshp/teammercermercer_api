package in.lnt.utility.general;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;


/**
 * 
 * @author Sarnag Gandhi
 * @version 0.1
 * @since  28 Aug 2017.
 *
 */
public class DataUtility {
	
	public static boolean isStringNotNullAndBlank(String str) {
		if(str!=null && !str.equals("")) {
			return true;
		}
		return false;
	}
	  
   /**
    * 
    * @param inDate
    * @return boolean
    */
	public static boolean isValidDate(String inDate) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    dateFormat.setLenient(false);
	    try {
	      dateFormat.parse(inDate.trim());
	    } catch (ParseException pe) {
	      return false;
	    }
	    return true;
	  }
	
	/**
	 * 
	 * @return timestampfor storing in DB
	 */
	public static Timestamp currentTimestamp() {
		Date today = new java.util.Date();				// TODO Create Util Function for this 
		return new java.sql.Timestamp(today.getTime());
	}
	
	/**
	 * 
	 * @param string
	 * @return boolean
	 */
	public static boolean isDecimal(String string) {
	       if (!StringUtils.isEmpty(string)) {
	    	   return string.matches("(\\+|-)?([0-9]*+(\\.[0-9]+)?)");  //For 2 decimals ("^\\d+\\.\\d{2}$")
	    	   										   // For loosely typed use regex:^[\d.]+$	
	       }else {
	    	   return false;
	       }
	  }

	
	/**
	 * 
	 * @param string
	 * @return boolean
	 */
	public static boolean isNumeric(String string) {
	       if (!StringUtils.isEmpty(string)) {
	    	   return string.matches("(\\+|-)?([0-9]+)");  
	    	   										   	
	       }else {
	    	   return false;
	       }
	  }
}
