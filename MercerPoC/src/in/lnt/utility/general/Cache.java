package in.lnt.utility.general;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.lnt.utility.constants.CacheConstats;


public class Cache implements Serializable {
	
	private static final Logger logger = LoggerFactory.getLogger(Cache.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Properties config = new Properties();
	private static Properties error = new Properties();
	private static volatile Cache cache;

	static {
		 String base = System.getenv().get(CacheConstats.MAXIQ_HOME);
		FileInputStream fis=null;
		FileInputStream fisForError=null;
		//String base = "D:/workspace/MercerPoC";
		
		logger.info("BasePath    =====  " + base);
		if (StringUtils.isBlank(base)) {
			logger.info("Base Path $MAXIQ_HOME {} not configured");
		}
		if(null == base){
			base = "/softwares/maxiq/web/wars_for_mercer_poc/mercer_conf";
		}

		String basePath = base + "/conf/config.properties";
		String errorPath = base + "/conf/mercer_error_message.properties";
		logger.info(basePath);
		try {
			
			fis = FileUtils.openInputStream(new File(basePath));
			fisForError = FileUtils.openInputStream(new File(errorPath));
				config.load(fis);
				error.load(fisForError);
		
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		finally
		{
		
			try {
				fisForError.close();
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static Cache getInstance() {
		if (null == cache) {
			synchronized (Cache.class) {
				if (null == cache) {
					cache = new Cache();
				}
			}
		}

		return cache;
	}

	/***
	 * Gets the cache value for the key
	 *
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		return config.getProperty(key);
	}
	public static String getPropertyFromError(String key) {
		return error.getProperty(key);
	}
}
