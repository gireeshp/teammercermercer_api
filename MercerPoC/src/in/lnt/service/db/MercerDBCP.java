package in.lnt.service.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;

import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.Cache;

public class MercerDBCP {

	private static String driver = Cache.getProperty(CacheConstats.MERCER_DB_DRIVER);
	private static String dbAddress = Cache.getProperty(CacheConstats.MERCER_DB_ADDRESS);
	private static String dbName = Cache.getProperty(CacheConstats.MERCER_DB_NAME);
	private static String userName = Cache.getProperty(CacheConstats.MERCER_DB_USERNAME);
	private static String password = Cache.getProperty(CacheConstats.MERCER_DB_PASSWORD);

	private static MercerDBCP datasource;
    private BasicDataSource basicDs;

	private MercerDBCP() throws IOException, SQLException  {
		basicDs = new BasicDataSource();
		basicDs.setDriverClassName(driver);
		basicDs.setUrl(dbAddress+dbName);
		basicDs.setUsername(userName);
		basicDs.setPassword(password);
		
		// the settings below are optional -- dbcp can work with defaults
		basicDs.setMinIdle(5);
		basicDs.setMaxIdle(20);
		basicDs.setMaxOpenPreparedStatements(180);

	}

	 public static MercerDBCP getDataSource() throws IOException, SQLException  {
	        if (datasource == null) {
	            datasource = new MercerDBCP();
	            return datasource;
	        } else {
	            return datasource;
	        }
	    }
	 
	 public Connection getConnection() throws SQLException {
	        return this.basicDs.getConnection();
	    }


}
