package in.lnt.datageneration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.jfairy.Fairy;
import org.jfairy.producer.company.Company;
import org.jfairy.producer.net.Network;
import org.jfairy.producer.payment.CreditCardProducer;
import org.jfairy.producer.person.Person;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.CDL;

public class DataGeneartion {
	private static final int TOTAL_RECORDS = 1000;

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		Fairy fairy = Fairy.create();
		Company company = fairy.company();
		CreditCardProducer creditCard = fairy.creditCard();
		Network network = fairy.network();
		Person person = fairy.person();
		
		Random random = new Random();

		Map<String,JSONObject> mapping = new HashMap<String,JSONObject>();
		
		List<String> personMethods  =new ArrayList<String>();
		List<String> networkMethods =new ArrayList<String>();
		List<String> companyMethods =new ArrayList<String>();
		
		//String[] numberedfields = 
		
		String[] classesList = {"Person" ,"Network", "Company"};
		for (String individulaClass : classesList) {
			Method[] methods = null;
			switch(individulaClass){
			case "Person" : 
				 methods = Person.class.getDeclaredMethods();
				for (Method method : methods) {
					personMethods.add(method.getName());
				}
				break;
			case "Network" : 
				 methods = Network.class.getDeclaredMethods();
				for (Method method : methods) {
					networkMethods.add(method.getName());
				}
				break;
			case "Company" : 
				 methods = Company.class.getDeclaredMethods();
				for (Method method : methods) {
					companyMethods.add(method.getName());
				}
				break;
			} 
		}
		System.out.println(personMethods.size() + " "+ networkMethods.size()+ " "+companyMethods.size() );
		personMethods.remove("isMale");
		personMethods.remove("setSex");
		personMethods.remove("isFemale");
		personMethods.remove("setAge");
		personMethods.remove("setCompany");
		personMethods.remove("telephoneNumberFormat");
		personMethods.remove("randomBoolean");
		personMethods.remove("generateUsername");
		personMethods.remove("generateEmail");
		personMethods.remove("generate");
		
		JSONArray data = new JSONArray();
		
		int compVar = 0;
		int perVar = 0;
		int netVar = 0;

		int fields = Math.abs(random.nextInt()) % 1 + 2;
		System.out.println(fields);
		for (int j = 0; j < TOTAL_RECORDS; j++) {

			JSONObject dataObj = new JSONObject();
			for (int i = 0; i < fields; i++) {
				JSONObject mappingObj = new JSONObject();
				if (i >= 13) {
					//System.out.println("compVar - "+ i + " "+ compVar);
					Method m = company.getClass().getMethod(companyMethods.get(compVar));
					// System.out.println(m.invoke(company) + " - "+
					// m.getReturnType().getSimpleName() +" - "+
					// companyMethods.get(compVar));
					dataObj.put(companyMethods.get(compVar), m.invoke(company));
					mappingObj.put("code", companyMethods.get(compVar));
					mappingObj.put("displayLabel", companyMethods.get(compVar));
					mappingObj.put("dataType", m.getReturnType().getSimpleName());
					mappingObj.put("validations",
							getValidation(companyMethods.get(compVar), m.getReturnType().getSimpleName()));
					mapping.put(companyMethods.get(compVar), mappingObj);
					compVar++;
					//System.out.println(mappingObj); 
				}
				else if (i == 11) {
					//System.out.println("netVar - "+ i + " "+ netVar);
					Method m = network.getClass().getMethod(networkMethods.get(netVar));
					// System.out.println(m.invoke(company) + " - "+
					// m.getReturnType().getSimpleName() +" - "+
					// companyMethods.get(compVar));
					dataObj.put(networkMethods.get(netVar), m.invoke(network));
					mappingObj.put("code", networkMethods.get(netVar));
					mappingObj.put("displayLabel", networkMethods.get(netVar));
					mappingObj.put("dataType", m.getReturnType().getSimpleName());
					mappingObj.put("validations",
							getValidation(networkMethods.get(netVar), m.getReturnType().getSimpleName()));
					mapping.put(networkMethods.get(netVar), mappingObj);
					netVar++;
				}
				else if (i <= 10) {
					//System.out.println("perVar - "+ i + " "+ perVar);
					System.out.println(personMethods.get(perVar));
					Method m = person.getClass().getMethod(personMethods.get(perVar));
					dataObj.put(personMethods.get(perVar), m.invoke(person));
					mappingObj.put("code", personMethods.get(perVar));
					mappingObj.put("displayLabel", personMethods.get(perVar));
					mappingObj.put("dataType", m.getReturnType().getSimpleName());
					mappingObj.put("validations",
							getValidation(personMethods.get(perVar), m.getReturnType().getSimpleName()));
					mapping.put(personMethods.get(perVar), mappingObj);
					perVar++;
				}
			}
			compVar = 0;
			perVar = 0;
			netVar = 0;
			data.put(dataObj);
		}
		System.out.println("MAPPING " + mapping.size());
		//System.out.println("DATA " + data.toString());
		
		JSONArray mappingArray = new JSONArray();
		Set<String> keys = mapping.keySet();
		for (String key : keys) {
			mappingArray.put(mapping.get(key));
		}
		JSONObject mappingObject = new JSONObject();
		mappingObject.put("columns", mappingArray);
		 
		
		File fileCsv=new File("D:/PROJECT/MERCER/project Backup/30-3/new/MercerPoC/resources/data.csv");
	 
        String csv = CDL.toString(data);
        try {
			FileUtils.writeStringToFile(fileCsv, csv);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try (FileWriter file = new FileWriter("D:/PROJECT/MERCER/project Backup/30-3/new/MercerPoC/resources/mapping.json")) {
			file.write(mappingObject.toString());
			System.out.println("Successfully Copied JSON Object to File...");
			System.out.println("\nJSON Object: " + mappingObject.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}

	private static Object getValidation(String name, String dataType) {
		// TODO Auto-generated method stub
		JSONArray obj = new JSONArray();
		return obj;
	}
}
