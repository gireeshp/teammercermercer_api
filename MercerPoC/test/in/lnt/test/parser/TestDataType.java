package in.lnt.test.parser;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.augmentiq.maxiq.ExpressionEvaluator.ExpressionEvaluator;
import com.augmentiq.maxiq.ExpressionEvaluator.exceptions.ExpressionEvaluatorException;
import com.augmentiq.maxiq.ExpressionEvaluator.exceptions.FieldNotValidException;

import junit.framework.TestCase;

public class TestDataType extends TestCase{
	ExpressionEvaluator ExpressionEvaluator = new ExpressionEvaluator();
	static Pattern DOUBLE_PATTERN = Pattern.compile("\\d+\\.\\d+");
	public void testDouble() throws ExpressionEvaluatorException, FieldNotValidException{Map<String, String> paramValues = new HashMap<String, String>();
	paramValues.put("age", "20");

	System.out
			.println("Addition : " + ExpressionEvaluator.expressionEvaluator(paramValues, "(15 + 10) >= 1.5", false));

	paramValues.put("this.sti_actual", "1629.67");
	paramValues.put("this.sti_target", "23025.60");
	

/*	System.out.println("Range : "
			+ ExpressionEvaluator.expressionEvaluator(paramValues, "1629.67/23025.60", false));
																	*/
	System.out
			.println("Addition : " + ExpressionEvaluator.expressionEvaluator(paramValues, "length(target)", false));

	System.out.println("Addition : " + ExpressionEvaluator.expressionEvaluator(paramValues,
			"age >10 && age < 29  && (length(no) > 1)", false));}

}
