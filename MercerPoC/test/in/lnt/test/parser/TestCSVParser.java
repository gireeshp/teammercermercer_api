package in.lnt.test.parser;

import java.util.HashMap;
import java.util.Map;

import com.augmentiq.maxiq.ExpressionEvaluator.ExpressionEvaluator;
import com.augmentiq.maxiq.ExpressionEvaluator.exceptions.ExpressionEvaluatorException;
import com.augmentiq.maxiq.ExpressionEvaluator.exceptions.FieldNotValidException;

import in.lnt.parser.CSVParser;
import junit.framework.TestCase;

public class TestCSVParser extends TestCase{
   public void testCSVParser() throws ExpressionEvaluatorException, FieldNotValidException{
	   CSVParser csvParser =new CSVParser();
	   //System.out.println(csvParser.parseFile("./resources/sample_data.csv"));
		ExpressionEvaluator ExpressionEvaluator = new ExpressionEvaluator();

	   
	   Map<String, String> paramValues = new HashMap<String, String>();
		paramValues.put("{input}", "Mumbai(W)");

		String expresion = "upper({input})";
		System.out.println("Upper : " + ExpressionEvaluator.expressionEvaluator(paramValues, expresion, false));

   }
}
