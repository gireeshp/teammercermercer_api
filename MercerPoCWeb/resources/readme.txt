Properties in config.properties file:
1. HDFS_BYPASS_PASSWORD will not set password for the SCP and SSh connection.
2. HDFS_STRICT_HOST_KEY_CHECKING = yes OR no
3. HDFS_STORE_PATH = Final path where files need to be stored.
4. CLUSTER_DESTINATION_INTERIM_PATH = Path where files will be stored on HDFS system.
5. CLUSTER_DESTINATION_INTERIM_PATH_TODELETE = Accepts boolean value if user wants to discard files 
	stored on path defined in CLUSTER_DESTINATION_INTERIM_PATH.  
