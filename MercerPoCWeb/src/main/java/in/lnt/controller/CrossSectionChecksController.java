package in.lnt.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import in.lnt.exceptions.CustomStatusException;
import in.lnt.constants.Constants;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.JsonUtils;
import in.lnt.validations.FormValidator_CSC;

/**
 * Handles requests for the Level 3B
 */
@Controller
public class CrossSectionChecksController {

	private static final Logger logger = LoggerFactory.getLogger(CrossSectionChecksController.class);

//	// upload settings
//	private static final int MEMORY_THRESHOLD = 1024 * 1024 * 3; // 3MB
//	private static final int MAX_FILE_SIZE = 1024 * 1024 * 2000; // 2GB
//	private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 2500; // 2.5GB

	
	/**
	 * Upload multiple file using Spring Controller
	 * 
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	// ,consumes = "application/json",produces = "application/json"
	@RequestMapping(value = "/crossSectionChecks", method = RequestMethod.POST, consumes = "application/json",produces="application/json")
	public @ResponseBody void crossSectionChecksHandler(@RequestBody String validationData, HttpServletRequest request,HttpServletResponse response) 
			{
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_CROSSSECTIONCHECK));
		String entitiesJson = "";
		String key = "";
		String entitiesKey=null;
		JSONObject wholeDataObject=null;
		Iterator<String> it=null;
		String validatedSyntax="false";
		String isUniqueEeid="true";
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode errNode = null;
		try{
			errNode = mapper.createObjectNode();
		validatedSyntax = JsonUtils.isJSONValid(validationData); // Json Syntax checker..
		if(validatedSyntax.equalsIgnoreCase("true")) {
			wholeDataObject = new JSONObject(validationData);
			it = wholeDataObject.keys();
		} else {
			throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_004));
		}
		
			while (it != null && it.hasNext()) {
				key = it.next();
				if(key.equalsIgnoreCase("entities")){
					entitiesKey=key;
				}
			}

			if(entitiesKey!=null && wholeDataObject.get(entitiesKey) != null){
				//logger.info(String.format("StartTime For process : %s", wholeDataObject.get(entitiesKey) ));
				entitiesJson=wholeDataObject.get(entitiesKey).toString();
				/*isUniqueEeid = new FormValidator_CSC().checkUniqueNessForEEID(entitiesJson);
				if(isUniqueEeid.equalsIgnoreCase("Duplicate"))
					throw new CustomStatusException(Constants.HTTPSTATUS_400,Constants.DUPLICATE_EEID);
				else if(isUniqueEeid.equalsIgnoreCase("Missing"))
					throw new CustomStatusException(Constants.HTTPSTATUS_400,Constants.MISSING_EEID);*/
			} else {	//errorResponse.put("RESPONSE", "ERR 3: Invalid input JSON ");
				throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_004));
			}
			
			// TODO Send JsonObject
			boolean isMDARequest = false; 
			isMDARequest = Boolean.valueOf(String.valueOf(request.getAttribute(Constants.ISMDAREQUEST)));
			request.setAttribute(Constants.ISMDAREQUEST, isMDARequest);
			validateForm(entitiesJson,request, response);

		} catch (CustomStatusException cse) {
			errNode = JsonUtils.updateErrorNode(cse,errNode);
			downLoadData(response, errNode.toString().getBytes(), cse.getHttpStatus());
		} catch (Exception e) {
			//logger.error(String.format("Exception is %s Reason is %d", e.getMessage(),e.getCause()));
			//errNode.put("Message Key", e.getMessage());
			e.printStackTrace();
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			downLoadData(response, errNode.toString().getBytes(), Constants.HTTPSTATUS_500);
		}
	}	
	
	
	/**
	 * @author Jwala Pradhan
	 * @param validationData
	 * @param request
	 * @param response
	 * @throws Exception
	 * @story PE:5414
	 */
	@RequestMapping(value = "/performDefaultActions", method = RequestMethod.POST, consumes = "application/json",produces="application/json")
	public @ResponseBody void mercerDefinedActionsHandler(@RequestBody String validationData, HttpServletRequest request,HttpServletResponse response) 
		 {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_METHOD_PERFORMDEFAULTACTIONS));
		request.setAttribute(Constants.ISMDAREQUEST, true);
		crossSectionChecksHandler(validationData,  request, response);
	}	
	
	void downLoadData(HttpServletResponse response, byte[] bs, int status) {
		InputStream is;
		ObjectNode errNode = null;
		try {
			//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_METHOD_DOWNLOAD));
			is = new ByteArrayInputStream(bs);
			response.setStatus(status);
			// MIME type of the file
			response.setContentType("application/json");
			// Response header
			response.setHeader("Content-Disposition", "attachment; filename=\"output.json\"");

			// response.setHeader("Content-Disposition", "inline;
			// filename=\"output.json\"");
			// Read from the file and write into the response
			OutputStream os = response.getOutputStream();
			byte[] buffer = new byte[1024];
			int len;
			while ((len = is.read(buffer)) != -1) {
				os.write(buffer, 0, len);
			}
			os.flush();
			os.close();
			is.close();
			//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_METHOD_DOWNLOAD));
		} catch (Exception e) {
			logger.error("Exception occured in downloadData..,"+e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			downLoadData(response, errNode.toString().getBytes(), Constants.HTTPSTATUS_500);
			
		}
	}
	
	/*
	 *  TODO Move to FormValidator.java
	 */
	public @ResponseBody void validateForm(String entitiesJson,HttpServletRequest request,HttpServletResponse response) 
			throws CustomStatusException,Exception {
		//logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_METHOD_VALIDATEFORM));
		JsonNode data = null;
		boolean isMDARequest = false;
			FormValidator_CSC formValidator = new FormValidator_CSC();
			//logger.info(request.getAttribute(Constants.ISMDAREQUEST).toString());
			data = formValidator.parseAndValidate_CrossSectionCheck(entitiesJson,(Boolean) request.getAttribute(Constants.ISMDAREQUEST));
			if (data != null) {
				downLoadData(response, data.toString().getBytes(), 200);
				//logger.debug("response sent successfully..");
			}
		//logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_METHOD_VALIDATEFORM));
	}
}
