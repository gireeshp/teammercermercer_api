package in.lnt.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import in.lnt.exception.CustomAPIException;

/**
 * REST exception handlers defined at a global level for the application
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	private static final Logger log = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    /**
     * Catch all for any other exceptions...
     */
    @ExceptionHandler({ Exception.class })
    public ResponseEntity<?> handleAnyException(Exception e) {
        return errorResponse(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  
    

    @ExceptionHandler({ JSONException.class })
    public ResponseEntity<?> handleAnyException(JSONException e) {
        return errorResponse(e, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler({ CustomAPIException.class })
    public ResponseEntity<?> handleAnyException(CustomAPIException e) {
        return errorResponse(e, HttpStatus.BAD_REQUEST);
    }
    
    /**
     * Handle failures commonly thrown from code
     */
    @ExceptionHandler({ InvocationTargetException.class, IllegalArgumentException.class, ClassCastException.class,
            ConversionFailedException.class})
    public ResponseEntity handleMiscFailures(Throwable t) {
        return errorResponse(t, HttpStatus.BAD_REQUEST);
    }

    /**
     * Send a 409 Conflict in case of concurrent modification
     */
    @ExceptionHandler({  OptimisticLockingFailureException.class,
            DataIntegrityViolationException.class })
    public ResponseEntity handleConflict(Exception ex) {
        return errorResponse(ex, HttpStatus.CONFLICT);
    }

    
    /**
     * Send a 409 Conflict in case of concurrent modification
     */
    @ExceptionHandler({  IOException.class,   FileNotFoundException.class })
    public ResponseEntity handleConflict(IOException ex) {
        return errorResponse(ex, HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(value = { HttpClientErrorException.class })
    protected ResponseEntity<Object> handleConflict(final RuntimeException ex, final WebRequest request) {
        if (ex instanceof HttpClientErrorException)
        {
            final HttpClientErrorException restEx = (HttpClientErrorException) ex;
            return handleException(restEx, request);
        }
        return handleExceptionInternal(ex, "Internal server error",
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
    
    protected ResponseEntity<ErrorMessage> errorResponse(Throwable throwable,HttpStatus status) {
        if (null != throwable) {
            log.error("error caught: " + throwable.getMessage(), throwable);
           // return response(new CustomAPIException(status.value(),throwable,throwable.getMessage()), status);
            return response(new CustomAPIException(status.value(),throwable.getMessage()), status);
        } else {
            log.error("unknown error caught in RESTController, {}", status);
            return response(null, status);
        }
    }

    protected <T> ResponseEntity<T> response(CustomAPIException body, HttpStatus status) {
        log.debug("Responding with a status of {}", status);
        ObjectMapper mapperObj = new ObjectMapper();
        ObjectWriter writer= null;
        String jsonStr = "";
        JsonNode dataNode=null;
        try {
        	mapperObj.configure(SerializationFeature.WRAP_ROOT_VALUE,true);
			writer = mapperObj.writer().withRootName("errors");
			//body.setStackTrace(null) ;
			
			jsonStr = mapperObj.writeValueAsString(body);
			
			//jsonStr.substring(0, jsonStr.indexOf("stackTrace")+1);
			
		//	jsonStr.replace(target, replacement)
			/*try {
				dataNode = mapperObj.readValue(jsonStr, JsonNode.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonStr = ((ObjectNode) (dataNode.get(0))).remove("stackTrace").asText();*/
		} catch (JsonProcessingException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			//throw new JsonProcessingException(ex.getMessage());
		}
        return new ResponseEntity(jsonStr.substring(0, jsonStr.indexOf("stackTrace")-1)+jsonStr.substring(jsonStr.indexOf("\"message\"")),status);
    }
    
    
	public synchronized Throwable  fillInStackTrace()
	{
	return new Throwable();
	}
    
	
	private HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.valueOf(statusCode);
    }
}