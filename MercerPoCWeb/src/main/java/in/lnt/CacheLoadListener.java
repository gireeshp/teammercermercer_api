package in.lnt;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import in.lnt.service.db.DBUtils;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class CacheLoadListener  implements ServletContextListener {
	
	CacheManager cacheMgr;
	
	Cache cache ;

    public void contextInitialized(ServletContextEvent arg0) {
    	
        cacheMgr = CacheManager.getInstance();
       
        //2. Create a cache called "cacheStore"
        cacheMgr.addCache("cacheStore");
      
        // 3. Get a cache called "cache1"
        cache = cacheMgr.getCache("cacheStore");
        
        // 4. Put few elements in cache
        try {
			cache.put(new Element("dbCacheKey", DBUtils.loadDataBaseInMemory()));
			cache.put(new Element("allTableList", DBUtils.getAllTables()));
			//cache.put(new Element("dbCacheKey", DBUtils.loadDataBaseInMemory()));
		} catch (IllegalArgumentException e) {
		} catch (IllegalStateException e) {
		} catch (CacheException e) {
		} catch (SQLException e) {
		} catch (IOException e) {
		}
        
        // 4. Put few elements in cache
       // cache.put(new Element("whatever", "yourObject"));

    }

    public void contextDestroyed(ServletContextEvent arg0) {
        // 8. shut down the cache manager
        cacheMgr.shutdown();
    }

}
