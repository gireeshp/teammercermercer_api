package in.lnt.service.storage;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import in.lnt.constants.MercerQueryConstants;
import in.lnt.secureShell.SCPManager;
import in.lnt.secureShell.SSHManager;
import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.DataUtility;

/**
 * 
 * @author Sarang Gandhi
 * @version 0.1
 */

public class MLIntegration {

	private static final Logger logger = LoggerFactory.getLogger(MLIntegration.class);

	public static File copyToMLPath(List<String> fileNames, List<InputStream> inputStreams) {

		logger.info("Entering Inside the method copyToMLPath");

		// SSH related
		SSHManager instanceSsh = null;
		String command;
		String result;
		String sshErrorMessage = null;

		// SCP related
		SCPManager instanceScp = null;
		String knownHostFilepath = "";
		String destFullOldFilepath = null;
		String scpErrorMessage = null;

		// Remote credentials
		String remoteHost = CacheConstats.ML_REMOTE_ADD;
		String remoteUname = CacheConstats.ML_REMOTE_UNAME;
		String remotePwd = CacheConstats.ML_REMOTE_PWD;
		String mlRemoteInputLoc = CacheConstats.ML_INPUTLOC;
		logger.debug("remote credentials are remoteHost: " + remoteHost + "remoteUname: " + remoteUname);

		String fileNameArr[] = null;
		String newJsonFileName = null;
		String newDataFileName = null;
		Timestamp currTstamp = null;

		File mlFile = null;
		try {
			command = "";

			instanceScp = new SCPManager(remoteUname, remotePwd, remoteHost, knownHostFilepath);
			logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "SCP connection acquired.");

			instanceSsh = new SSHManager(remoteUname, remotePwd, remoteHost, knownHostFilepath);
			logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "SSH instance has been opened.");

			sshErrorMessage = instanceSsh.connect();

			scpErrorMessage = instanceScp.connect();
			if (sshErrorMessage != null || scpErrorMessage != null) {
				logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + sshErrorMessage + " SCP Error:" + scpErrorMessage);
				return mlFile;
			} else {

				for (int i = 0; i < fileNames.size(); i++) {

					destFullOldFilepath = mlRemoteInputLoc + "/" + fileNames.get(i);
					instanceScp.put(mlRemoteInputLoc, inputStreams.get(i), fileNames.get(i));

					currTstamp = DataUtility.currentTimestamp();
					fileNameArr = fileNames.get(i).split("\\.(?=[^\\.]+$)");

					// Rename file logic using mv command.
					if (i == 0) {
						newJsonFileName = fileNameArr[0] + "_" + currTstamp.getTime() + "." + fileNameArr[1];
						command = "mv " + destFullOldFilepath + " " + mlRemoteInputLoc + "/" + newJsonFileName;
						// Call sendCommand for each command and the output (without prompts) is
						// returned
						result = instanceSsh.sendCommand(command);
						logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "Temporary file '" + destFullOldFilepath
								+ "' renamed as " + mlRemoteInputLoc + "/" + newJsonFileName + ".Result : " + result);
					}
					if (i == 1) {
						newDataFileName = fileNameArr[0] + "_" + currTstamp.getTime() + "." + fileNameArr[1];
						command = "mv " + destFullOldFilepath + " " + mlRemoteInputLoc + "/" + newDataFileName;
						// Call sendCommand for each command and the output (without prompts) is
						// returned
						result = instanceSsh.sendCommand(command);
						logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "Temporary file '" + destFullOldFilepath
								+ "' renamed as " + mlRemoteInputLoc + "/" + newDataFileName + ".Result : " + result);
					}
				}
				mlFile = executeRemoteScript(newJsonFileName, newDataFileName, instanceScp);
			}

		} catch (SftpException ex) {
			logger.error("SFTP error has occured :" + ex.getMessage());
//			ex.printStackTrace();
		} catch (Exception ex) {
			logger.error("Exception has occured :" + ex.getMessage());
//			ex.printStackTrace();
		} finally {
			if (null != instanceScp)
				instanceScp.close();
			if (null != instanceSsh)
				instanceSsh.close();
		}

		logger.info("Exiting from the method copyToMLPath");

		logger.info("calling before  File fetched to apache tomcat server ..... mlFile" + mlFile.getAbsolutePath());
		return mlFile;
	}

	/**
	 * 
	 * @param jsonFile
	 * @param dataFile
	 */

	public static File executeRemoteScript(String jsonFile, String dataFile, SCPManager instanceScp) {

		logger.info(" Entering into ExecuteRemoteScript");

		String knownHostFilepath = "";
		String errorMessage = null;

		// SCP Details
		String remoteHost = CacheConstats.ML_REMOTE_ADD;
		String remoteUser = CacheConstats.ML_REMOTE_UNAME;
		String remotePwd = CacheConstats.ML_REMOTE_PWD;
		String remotePort = CacheConstats.ML_REMOTE_PORT;

		// determine the number of processes running on the current Unix system
		StringBuffer command = new StringBuffer();
		BufferedReader reader = null;
		InputStream inputStreamObj = null;
		OutputStream outputStream = null;
		String line = "";

		JSch jsch = null;
		Session session = null;
		File targetFile = null;
		Channel channel = null;
		InputStream input = null;
		try {

			command.append(CacheConstats.ML_REMOTE_SCRIPT);
			command.append(" ");
			command.append(CacheConstats.ML_INPUTLOC + "/" + jsonFile);
			command.append(" ");
			command.append(CacheConstats.ML_INPUTLOC + "/" + dataFile);

			if (null == instanceScp) {
				instanceScp = new SCPManager(remoteUser, remotePwd, remoteHost, knownHostFilepath);
				errorMessage = instanceScp.connect();
			}

			logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "SCP connection acquired.");
			logger.info(" temp instanceScp" + instanceScp);

			jsch = new JSch();

			session = jsch.getSession(remoteUser, remoteHost, Integer.parseInt(remotePort));

			session.setConfig("StrictHostKeyChecking", "no");

			session.setPassword(remotePwd);

			session.connect();

			channel = session.openChannel("exec");

			((ChannelExec) channel).setCommand(command.toString());

			logger.info("Channel Connected to machine " + remoteHost + " server with command: " + command);

			((ChannelExec) channel).setErrStream(System.err);

			input = channel.getInputStream();

			channel.connect();

			logger.info(" Line not 216   connecting channel");

			targetFile = new File(dataFile);

			logger.info("Line no 212 " + targetFile.getName());

			try {
				InputStreamReader inputReader = new InputStreamReader(input);
				BufferedReader bufferedReader = new BufferedReader(inputReader);
				while ((line = bufferedReader.readLine()) != null) {

					logger.info("line :" + line);

					if (line.equalsIgnoreCase(CacheConstats.ML_SUCCESS_RESPONSE)) {
						if (null == errorMessage) {
							logger.info("Error message is null inside inner if block before scp");
							FileUtils.copyInputStreamToFile(instanceScp.get(CacheConstats.ML_OUTPUTLOC, dataFile),
									targetFile);
							logger.info("calling FileUtils.copyInputStreamToFile File fetched to apache tomcat server "
									+ targetFile.getAbsolutePath());
							logger.info("calling FileUtils.copyInputStreamToFile File fetched to apache tomcat server "
									+ targetFile.getName() + targetFile.getAbsolutePath());

							try {
								Thread.sleep(CacheConstats.ML_RESPONSE_TIMEOUT);
							} catch (Exception ex) {
								logger.error(ex.getMessage() + " Cause is" + ex.getCause());
							}

							logger.info("SCP DONE :");
						} else {
							logger.info("errorMessage :" + errorMessage);
						}

					} else {
						targetFile = null;
					}
				}

			} catch (IOException ex) {
				logger.error(ex.getMessage() + ex.getCause());
//				ex.printStackTrace();
			} catch (Exception ex) {
				logger.error(ex.getMessage() + ex.getCause());
//				ex.printStackTrace();
			} finally {
				if (null != inputStreamObj)
					try {
						inputStreamObj.close();
					} catch (IOException ex) {
						logger.error("IOException from exception block inputStreamObj" + ex.getMessage());
					}
				if (null != outputStream)
					try {
						outputStream.close();
					} catch (IOException ex) {
						logger.error("IOException from exception block outputStream" + ex.getMessage());
					}
				if (reader != null)
					try {
						reader.close();
					} catch (IOException ex) {
						logger.error("IOException from exception block reader" + ex.getMessage());
					}

			}
		} catch (Exception ex) {
			logger.error(ex.getMessage() + ex.getCause());
//			ex.printStackTrace();
		} finally {
			channel.disconnect();
			session.disconnect();
		}
		logger.info(" Exiting from ExecuteRemoteScript");
		if (null != targetFile) {
			logger.info("calling before  File fetched to apache tomcat server ....." + targetFile.getAbsolutePath());
		}

		return targetFile;

	}
}
