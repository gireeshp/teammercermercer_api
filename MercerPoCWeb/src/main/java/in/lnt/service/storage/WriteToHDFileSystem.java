package in.lnt.service.storage;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.augmentiq.maxiq.Cache.CacheConstants;
import com.jcraft.jsch.SftpException;

import in.lnt.constants.MercerQueryConstants;
import in.lnt.database.JDBCSingleton;
import in.lnt.pojo.FileStorageInfo;
import in.lnt.secureShell.SCPManager;
import in.lnt.secureShell.SSHManager;
import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.DataUtility;

/**
 * @author Nikhil Kshirsagar
 * @since 25-09-2017
 * @version 1.0 
 */
public class WriteToHDFileSystem {
	
	private static final Logger logger = LoggerFactory.getLogger(WriteToHDFileSystem.class);
	
	public static void writeFilesToHdfs(FileStorageInfo fsi, List<String> fileNames , List<InputStream> inputStreams) {
		logger.debug(MercerQueryConstants.LOG_MERCERPOCWEB+" : >> WriteFilesToHdfs.writeFilesToHdfs()");
		
		JDBCSingleton jdbcInstance = null;
		Connection mysqlConn = null;
		Statement stmt = null;
		String query = null;
		Object[] args = null;
		Timestamp currTstamp = null;
		InputStream iStream = null;
		
		String filenameOld = null;
		String filenameNew =null;
		String[] filenameArr = null;
		int i = 0;
		try{
			jdbcInstance = JDBCSingleton.getInstance();
			
			for(int ctr = 0; ctr<fileNames.size(); ctr++){
				
				currTstamp = DataUtility.currentTimestamp();
				fsi.setTimeStamp(currTstamp);
				
				filenameOld = fileNames.get(ctr);
				
				filenameArr = filenameOld.split("\\.(?=[^\\.]+$)");
				filenameNew = filenameArr[0] + "_" + currTstamp.getTime() + "."+ filenameArr[1];
				fsi.setFileName(filenameNew);
				
				iStream = inputStreams.get(ctr);
				
				logger.debug(MercerQueryConstants.LOG_MERCERPOCWEB+" : >> WriteFilesToHdfs.writeFilesToHdfs()");
				copyToRemoteFS(fsi, filenameOld, filenameNew, iStream);
				logger.debug(MercerQueryConstants.LOG_MERCERPOCWEB+" : << WriteFilesToHdfs.writeFilesToHdfs()");
				
				if(mysqlConn == null){								// Open MySQL connection if file saved successfully
					mysqlConn = jdbcInstance.getConnection();
				}
				
				try {
					
					query = "insert into fileStorageInfo(srNo, requestId, time_stamp, fileName, requestLocale, requestAgent, requestIp) "
							+ "values(?,?,?,?,?,?,?)";
					args = new Object[]{UUID.randomUUID().toString(), fsi.getRequestId(), fsi.getTimeStamp(), fsi.getFileName(),
							fsi.getRequestLocale(), fsi.getRequestAgent(),  fsi.getRequestIp()};
					
					i = jdbcInstance.insert(query, args);

					logger.info(MercerQueryConstants.LOG_MERCERPOCWEB +i+" record has been updated in fileStorageInfo.");
					
				} catch (Exception e) {
					logger.warn(MercerQueryConstants.LOG_MERCERPOCWEB + e.getMessage());
				}
			}
		}catch(Exception e){
			logger.warn(MercerQueryConstants.LOG_MERCERPOCWEB + e.getMessage());
		} finally {
			try{
				if(null!=stmt){
					stmt.close();
					logger.warn(MercerQueryConstants.LOG_MERCERPOCWEB + "Statement is closed.");
				}
				if(mysqlConn!=null){
					mysqlConn.close();
					logger.warn(MercerQueryConstants.LOG_MERCERPOCWEB + "MySql Connection is closed.");
				}
			}catch(SQLException e){
				logger.warn(MercerQueryConstants.LOG_MERCERPOCWEB + e.getMessage());				
			}catch(Exception e){
				logger.warn(MercerQueryConstants.LOG_MERCERPOCWEB + e.getMessage());				
			}
			logger.debug(MercerQueryConstants.LOG_MERCERPOCWEB+" : << WriteFilesToHdfs.writeFilesToHdfs()");
		}
	}
	
	public static void copyToRemoteFS(FileStorageInfo fsi, String fileNameOld, String fileNameNew, InputStream iStream) 
						throws SftpException, Exception {
		SCPManager instanceScp = null;
		logger.debug(MercerQueryConstants.LOG_MERCERPOCWEB+" : >> WriteFilesToHdfs.copyToRemoteFS()");
		
		// Remote credentials
	    String user = Cache.getProperty(CacheConstats.HDFS_USERNAME);		// 10643821
	    String password = Cache.getProperty(CacheConstats.HDFS_PASSWORD);	// 
	    String ip = Cache.getProperty(CacheConstats.HDFS_IP);				// "172.20.99.11";

	    // SCP related
	    String destFilepath = Cache.getProperty(CacheConstats.CLUSTER_DESTINATION_INTERIM_PATH);	// "/home/10643821/one";
	    String hdfsStorePath = Cache.getProperty(CacheConstats.HDFS_STORE_PATH);	// 
	    String knownHostFilepath = "";
	    
    	String destFullOldFilepath = destFilepath+"/"+fileNameOld;
    	String errorMessage = null;

    	// SSH related
		SSHManager instanceSsh = null;
		String command;
		String result;

		try{
			instanceScp = new SCPManager(user, password, ip, knownHostFilepath);
			logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "SCP instance has been opened.");
			
			errorMessage = instanceScp.connect();
			if(errorMessage != null){
				logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "SCP Connect Error : "+ errorMessage);
			}else{
				remoteFileCopy(fileNameOld, iStream, instanceScp, destFilepath);
			}
			/* 
			 * Copy to HDFS 
			 */
			long startTime = System.currentTimeMillis();

			// Execute hadoop fs -copyFromLocal  HERE
			while(instanceScp.isPresent(destFullOldFilepath)){
				logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + " Temporary File '"+destFullOldFilepath+ "' is copied to system.");
				
				try{
					instanceSsh = new SSHManager(user, password, ip, knownHostFilepath);	// 172.20.99.11
					logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "SSH instance has been opened.");
					
					errorMessage = instanceSsh.connect();
					if(errorMessage != null){
						logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + errorMessage);
					
					}else{
						// Rename file to New Filename as in DB record
						command = "mv "+ destFullOldFilepath + " " + destFilepath+"/"+fileNameNew;
						
						// Call sendCommand for each command and the output (without prompts) is returned
						result = instanceSsh.sendCommand(command);										
						logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "Temporary file '"+destFullOldFilepath
								+"' renamed as "+destFilepath+"/"+fileNameNew + "Result : "+result);
						
						// hadoop fs -copyFromLocal <src> <dest>
						command = "hadoop fs -copyFromLocal "+destFilepath+"/"+fileNameNew+" "+hdfsStorePath;	
						result = instanceSsh.sendCommand(command);
						logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "File '"+hdfsStorePath
								+ "' is written to HDFS. Result : "+result);
					}
				}finally{
					if(instanceSsh!=null)
						instanceSsh.close();
					logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "SSH instance has been closed.");
				}
				
				if(Cache.getProperty(CacheConstats.CLUSTER_DESTINATION_INTERIM_PATH_TODELETE)!=null 
						&& Boolean.parseBoolean(Cache.getProperty(CacheConstats.CLUSTER_DESTINATION_INTERIM_PATH_TODELETE))==true){
					try{
						// TODO Remove only those files that have same size at both locations
						instanceScp.removeFile(destFilepath+"/"+fileNameNew);
						logger.warn(MercerQueryConstants.LOG_MERCERPOCWEB + "Temporary file '"
								+destFilepath+"/"+fileNameNew+ "' is deleted from system.");
					}catch(Exception e){
						logger.warn(MercerQueryConstants.LOG_MERCERPOCWEB + "Error in deleting temporary file '"
								+destFilepath+"/"+fileNameNew+ "' from system. "+ e.getMessage());
					}
				}					
				break;
			}

			long estimatedTime = System.currentTimeMillis() - startTime;
			logger.info("Total time took to write file to HDFS: " +  estimatedTime + " ms");
								
		} finally {
			if(instanceScp!=null){
				instanceScp.close();
			}
			logger.info(MercerQueryConstants.LOG_MERCERPOCWEB + "SCP instance has been closed.");
		}
		logger.debug(MercerQueryConstants.LOG_MERCERPOCWEB+" : << WriteFilesToHdfs.copyToRemoteFS()");
	}

	/**
	 * @param fileNameOld
	 * @param iStream
	 * @param instanceScp
	 * @param destFilepath
	 */
	private static void remoteFileCopy(String fileNameOld, InputStream iStream, SCPManager instanceScp,
			String destFilepath) {
		try {
			// File is copied to remote file system
			instanceScp.put(destFilepath, iStream, fileNameOld);
		} catch (SftpException e) {
			logger.info(e.getMessage());
		}
	}
}
