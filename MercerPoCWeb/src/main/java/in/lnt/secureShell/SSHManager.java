package in.lnt.secureShell;

import java.io.File;

/* 
* SSHManager
* 
* @author Sarang Gandhi
* @version 1.0
*/

import java.io.IOException;
import java.io.InputStream;

//import org.apache.log4j.Level;
import org.apache.log4j.Logger;
//import org.apache.log4j.Priority;
import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.Cache;

/**
 * @author Nikhil Kshirsagar
 * @since 25-09-2017
 * @version 1.0 
 */
public class SSHManager {
	private static final Logger logger = Logger.getLogger(SSHManager.class.getName());
	private JSch jschSSHChannel;
	private String strUserName;
	private String strConnectionIP;
	private int intConnectionPort;
	private String strPassword;
	private Session sesConnection;
	private int intTimeOut;

	private static final int  TIMEOUT =  300000; 

	// Constructor (userName, password, connectionIP, knownHostsFileName)
	public SSHManager(String userName, String password, String connectionIP, String knownHostsFileName) {
		doCommonConstructorActions(userName, password, connectionIP, knownHostsFileName);
		intConnectionPort = 22;
		intTimeOut = TIMEOUT;
	}

	// Constructor (userName, password, connectionIP, knownHostsFileName,connectionPort)
	public SSHManager(String userName, String password, String connectionIP, String knownHostsFileName,
			int connectionPort) {
		doCommonConstructorActions(userName, password, connectionIP, knownHostsFileName);
		intConnectionPort = connectionPort;
		intTimeOut = TIMEOUT;
	}

	// Constructor (userName, password, connectionIP, knownHostsFileName, connectionPort, timeOutMilliseconds)
	public SSHManager(String userName, String password, String connectionIP, String knownHostsFileName,
			int connectionPort, int timeOutMilliseconds) {
		doCommonConstructorActions(userName, password, connectionIP, knownHostsFileName);
		intConnectionPort = connectionPort;
		intTimeOut = TIMEOUT;
	}

	private void doCommonConstructorActions(String userName, String password, String connectionIP, String knownHostsFileName) {
		jschSSHChannel = new JSch();
		try {
			jschSSHChannel.setKnownHosts(knownHostsFileName);
		} catch (JSchException jschX) {
			logError(jschX.getMessage());
		}
		
		strUserName = userName;
		strPassword = password;
		strConnectionIP = connectionIP;
	}
		

	public String connect() {
		String errorMessage = null;

		try {
			sesConnection = jschSSHChannel.getSession(strUserName, strConnectionIP, intConnectionPort);
			
			// If HDFS_BYPASS_PASSWORD=true, then don't use password
			if(Cache.getProperty(CacheConstats.HDFS_BYPASS_PASSWORD)==null ||
					(Cache.getProperty(CacheConstats.HDFS_BYPASS_PASSWORD)!=null && Boolean.parseBoolean(Cache.getProperty(CacheConstats.HDFS_BYPASS_PASSWORD))==false)){
				sesConnection.setPassword(strPassword);
			}
			
			// UNCOMMENT THIS FOR TESTING PURPOSES, BUT DO NOT USE IN PRODUCTION
			if(Cache.getProperty(CacheConstats.HDFS_STRICT_HOST_KEY_CHECKING)!=null && 
					(Cache.getProperty(CacheConstats.HDFS_STRICT_HOST_KEY_CHECKING).equals("no") || Cache.getProperty(CacheConstats.HDFS_STRICT_HOST_KEY_CHECKING).equals("yes")) ){
				sesConnection.setConfig("StrictHostKeyChecking", Cache.getProperty(CacheConstats.HDFS_STRICT_HOST_KEY_CHECKING));
			}

			sesConnection.connect(intTimeOut);
		} catch (JSchException jschX) {
			errorMessage = jschX.getMessage();
		}

		return errorMessage;
	}

	private String logError(String errorMessage) {
		if (errorMessage != null) {
			/*logger.log(Priority.FATAL, "{0}:{1} - {2}",
							new Object[] { strConnectionIP, intConnectionPort, errorMessage });*/
		}
		return errorMessage;
	}

	private String logWarning(String warnMessage) {
		if (warnMessage != null) {
			/*logger.log(Level.FATAL, "{0}:{1} - {2}",
							new Object[] { strConnectionIP, intConnectionPort, warnMessage });*/
		}
		return warnMessage;
	}
		
	public String sendCommand(String command) {
		StringBuilder outputBuffer = new StringBuilder();
		try {
			Channel channel = sesConnection.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			InputStream commandOutput = channel.getInputStream();
			channel.connect();
			int readByte = commandOutput.read();

			while (readByte != 0xffffffff) {
				outputBuffer.append((char) readByte);
				readByte = commandOutput.read();
			}

			channel.disconnect();
		} catch (IOException ioX) {
			logWarning(ioX.getMessage());
			return null;
		} catch (JSchException jschX) {
			logWarning(jschX.getMessage());
			return null;
		}

		return outputBuffer.toString();
	}

	public void close() {
		sesConnection.disconnect();
	}


//	public InputStream getCSVFromMLAlgo(File csvFile, File jsonFile ) {
//		return null;
//	}

			/*public static void main(String[] args) {
				     logger.info("sendCommand");
		
				     *//**
				      * YOU MUST CHANGE THE FOLLOWING
				      * FILE_NAME: A FILE IN THE DIRECTORY
				      * USER: LOGIN USER NAME
				      * PASSWORD: PASSWORD FOR THAT USER
				      * HOST: IP ADDRESS OF THE SSH SERVER
				     **//*
				     String command = "spark-submit AC_20Aug_V1.py";
				     String userName = "maxiq";
				     String password = "MercerDoc1";
				     String connectionIP = "hn0-Mercer";
				     SSHManager instance = new SSHManager(userName, password, connectionIP, "");
				     String errorMessage = instance.connect();
		
				     if(errorMessage != null)
				     {
				        logger.info(errorMessage);
				        fail();
				     }
		
				     String expResult = "FILE_NAME\n";
				     // call sendCommand for each command and the output 
				     //(without prompts) is returned
				     String result = instance.sendCommand(command);
				     // close only after all commands are sent
				     logger.info("Result of the ML code is"+ result);
				     instance.close();
				      assertEquals(expResult, result);
				  }
			
			*/
	/**
	 * Test of sendCommand method, of class SSHManager.
	 */
	@Test
	public void testSendCommand() {
		logger.info("sendCommand");

		/**
		 * YOU MUST CHANGE THE FOLLOWING
		 * FILE_NAME: A FILE IN THE DIRECTORY
		 * USER: LOGIN USER NAME
		 * PASSWORD: PASSWORD FOR THAT USER
		 * HOST: IP ADDRESS OF THE SSH SERVER
		 **/
		String command = "spark-submit AC_20Aug_V1.py";
		String userName = "maxiq";
		String password = "MercerDoc1";
		String connectionIP = "hn0-Mercer";
		SSHManager instance = new SSHManager(userName, password, connectionIP, "");
		String errorMessage = instance.connect();

		if(errorMessage != null)
		{
			logger.info(errorMessage);
			fail();
		}

		String expResult = "FILE_NAME\n";
		// call sendCommand for each command and the output 
		//(without prompts) is returned
		String result = instance.sendCommand(command);
		// close only after all commands are sent
		instance.close();
		assertEquals(expResult, result);
	}
		  

	/**
	 * JSch Example Tutorial
	 * Java SSH Connection Program
	 */
	public static void main(String[] args) {
		String host = "";
		int port = 0;
		String user="";
		String password="";
		String command1="";
		
//		host = "hn0-Mercer";
//		user="maxiq";
//		password="MercerDoc1";
		// command1="sh /home/maxiq/Autocorrect/scripts/SimpleTest.sh";
//		command1="mkdir /home/maxiq/Autocorrect/scripts/NewFolderFromCode";
		
		host = "172.20.99.11";
		port = 22;
		user="10643821";
		password="10643821";
//		command1 = "mkdir /home/10643821/tempp/JavaCodedDir";
//		command1 = "hadoop fs -ls /";
		command1 = "hadoop fs -copyFromLocal /";
		
//		host = "13.85.28.223";
//		port = 22;
//		user="maxiq";
//		password="Uns@vedD0cument1";
////		command1 = "mkdir /softwares/JavaCodedDir";
//		command1 = "mkdir /softwares/JavaCodedDir";	
		
		try{
			java.util.Properties config = new java.util.Properties(); 
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session=jsch.getSession(user, host, port);
			session.setPassword(password);
			session.setConfig(config);
			session.connect();
			logger.info("Connected");

			Channel channel=session.openChannel("exec");
			((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec)channel).setErrStream(System.err);

			InputStream in=channel.getInputStream();
	        channel.connect();
	        byte[] tmp=new byte[1024];
	        while(true){
	          while(in.available()>0){
	            int i=in.read(tmp, 0, 1024);
	            if(i<0)break;
	            logger.info(new String(tmp, 0, i));
	          }
	          if(channel.isClosed()){
	        	  logger.info("exit-status: "+channel.getExitStatus());
	            break;
	          }
	          try{Thread.sleep(1000);}catch(Exception ee){}
	        }
			channel.disconnect();
			session.disconnect();
			logger.info("DONE");
		}catch(Exception e){
			logger.info(e.getMessage());
//			e.printStackTrace();
		}
	}


}